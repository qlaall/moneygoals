//
//  MGCommonAccessor.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCommonAccessor.h"

static char * const MGCommonAccessorQueue = "MGCommonAccessorQueue.concurrent.queue";

@interface MGCommonAccessor ()
@property (nonatomic, strong) dispatch_semaphore_t semaphore;
@property (nonatomic, strong) dispatch_queue_t queue;

@end

@implementation MGCommonAccessor

+ (MGCommonAccessor *)shared
{
    static MGCommonAccessor *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[MGCommonAccessor alloc] init];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    return self;
}

- (void)clearAllLocalData:(YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock
{
    self.semaphore = dispatch_semaphore_create(0);
    self.queue = dispatch_queue_create(MGCommonAccessorQueue, DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(self.queue, ^{
        [MGRecordAccessor.shared clearAllLocalDataWithCompleteBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGAccountAccessor.shared clearAllLocalDataWithCompleteBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGCategoryAccessor.shared clearAllLocalDataWithCompleteBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        dispatch_async_on_main_queue(^{
            if (completeBlock) {
                completeBlock();
            }
        });
    });
}

- (void)saveToLocalWithRecordModels:(NSArray *)recordModels accountModels:(NSArray *)accountModels categoryModels:(NSArray *)categoryModels completeBlock:(YYBTapedActionBlock)completeBlock
{
    self.semaphore = dispatch_semaphore_create(0);
    self.queue = dispatch_queue_create(MGCommonAccessorQueue, DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(self.queue, ^{
        [MGRecordAccessor.shared saveToLocalWithDataModels:recordModels completeBlock:^{
            [MGRecordAccessor.shared getLocalDataModels];
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGAccountAccessor.shared saveToLocalWithDataModels:recordModels completeBlock:^{
            [MGAccountAccessor.shared getLocalDataModels];
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGCategoryAccessor.shared saveToLocalWithDataModels:recordModels completeBlock:^{
            [MGCategoryAccessor.shared getLocalDataModels];
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        dispatch_async_on_main_queue(^{
            if (completeBlock) {
                completeBlock();
            }
        });
    });
}

- (void)getAndSaveAllOssDataWithCompleteBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock
{
    self.semaphore = dispatch_semaphore_create(0);
    self.queue = dispatch_queue_create(MGCommonAccessorQueue, DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(self.queue, ^{
        [MGRecordAccessor.shared getOssAndSaveDataModelsWithCompleteBlock:^{
            [MGRecordAccessor.shared getLocalDataModels];
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGAccountAccessor.shared getOssAndSaveDataModelsWithCompleteBlock:^{
            [MGAccountAccessor.shared getLocalDataModels];
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGCategoryAccessor.shared getOssAndSaveDataModelsWithCompleteBlock:^{
            [MGCategoryAccessor.shared getLocalDataModels];
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        dispatch_async_on_main_queue(^{
            if (completeBlock) {
                completeBlock();
            }
        });
    });
}

- (void)saveAllDataToOssWithCompleteBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock
{
    self.semaphore = dispatch_semaphore_create(0);
    self.queue = dispatch_queue_create(MGCommonAccessorQueue, DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(self.queue, ^{
        [MGRecordAccessor.shared saveAllToOssWithCompleteBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGAccountAccessor.shared saveAllToOssWithCompleteBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGCategoryAccessor.shared saveAllToOssWithCompleteBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        dispatch_async_on_main_queue(^{
            if (completeBlock) {
                completeBlock();
            }
        });
    });
}

- (void)saveUnsyncDataToOssWithCompleteBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock
{
//    // 只有没有用户id的账本才能被上传
//    NSArray *recordModels = [MGRecordAccessor.shared.dataModels takeWhile:^BOOL(MGRecordModel *obj) {
//        return !obj.create_userId.isExist;
//    }];
    
    NSArray *accountModels = [MGAccountAccessor.shared.dataModels takeWhile:^BOOL(MGAccountModel *obj) {
        return !obj.create_userId.isExist;
    }];
    
    NSArray *categoryModels = [MGCategoryAccessor.shared.dataModels takeWhile:^BOOL(MGCategoryModel *obj) {
        return !obj.create_userId.isExist;
    }];
    
    self.semaphore = dispatch_semaphore_create(0);
    self.queue = dispatch_queue_create(MGCommonAccessorQueue, DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(self.queue, ^{
//        [MGRecordAccessor.shared saveToOssWithDataModels:recordModels completeBlock:^{
//            dispatch_semaphore_signal(self.semaphore);
//        } errorBlock:^(NSError *error) {
//            dispatch_async_on_main_queue(^{
//                [YYBAlertView showAlertViewWithError:error];
//                if (errorBlock) {
//                    errorBlock(error);
//                }
//            });
//            dispatch_semaphore_signal(self.semaphore);
//        }];
        
//        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGAccountAccessor.shared saveToOssWithDataModels:accountModels completeBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        [MGCategoryAccessor.shared saveToOssWithDataModels:categoryModels completeBlock:^{
            dispatch_semaphore_signal(self.semaphore);
        } errorBlock:^(NSError *error) {
            dispatch_async_on_main_queue(^{
                [YYBAlertView showAlertViewWithError:error];
                if (errorBlock) {
                    errorBlock(error);
                }
            });
            dispatch_semaphore_signal(self.semaphore);
        }];
        
        dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
        dispatch_async_on_main_queue(^{
            if (completeBlock) {
                completeBlock();
            }
        });
    });
}

@end
