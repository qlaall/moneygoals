//
//  MGRecordAccessor.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseAccessor.h"
#import "MGRecordModel.h"
#import "MGReceiptDateModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordAccessor : MGBaseAccessor

+ (MGRecordAccessor *)shared;

- (MGRecordModel *)getSelectedRecordModel;
- (void)selectRecordModel:(MGRecordModel *)recordModel;

// ==============================================================
- (void)createReceiptModel:(MGReceiptModel *)receiptModel recordModel:(MGRecordModel *)recordModel completeBlock:(YYBTapedActionBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;
- (void)deleteReceiptModel:(MGReceiptModel *)receiptModel recordModel:(MGRecordModel *)recordModel completeBlock:(YYBTapedActionBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;
- (void)updateReceiptModel:(MGReceiptModel *)receiptModel shouldUpdateModel:(MGReceiptModel *)shouldUpdateModel recordModel:(MGRecordModel *)recordModel completeBlock:(YYBTapedActionBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;

// ==============================================================
- (NSDecimalNumber *)sumMoneyOfReceiptModels:(NSArray *)receiptModels;

- (void)beginInitialize;
- (MGRecordModel *)getDefaultRecordModel;
- (void)mergeDefaultRecordModelsWithCompleteBlock:(YYBTapedActionBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;

@end

NS_ASSUME_NONNULL_END
