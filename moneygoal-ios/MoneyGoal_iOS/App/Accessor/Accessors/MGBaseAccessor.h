//
//  MGBaseAccessor.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGAccessorModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseAccessor : NSObject

+ (MGBaseAccessor *)shared;
@property (nonatomic, strong) NSMutableArray *dataModels;

// ============================== 默认配置 ========================  需要必填
- (Class)getAccessorClass;
- (NSString *)getAccessorDirPath;
- (NSString *)getAccessorDefaultPath;
// 是否需要初始化数据，如果需要，则在该方法下设置一个值，使下次不需要被初始化
- (BOOL)shouldInitialize; // 非必填
- (NSString *)getOssPrefix;

// =================================================================
- (void)getOssAndSaveDataModelsWithCompleteBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
// 保存到阿里云上，保存成功后，会保存到本地，如果没有开启保存oss的功能，也会直接保存到本地
- (void)saveToOssWithDataModels:(NSArray *)dataModels completeBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
- (void)saveToOssAndLocalWithDataModels:(NSArray *)dataModels completeBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
- (void)saveAllToOssWithCompleteBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

- (void)deleteOssDataModel:(MGAccessorModel *)dataModel completeBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

// =================================================================
// 合并网上下载的数据到本地
- (void)getLocalDataModels;
- (void)saveToLocalWithDataModels:(NSArray *)dataModels completeBlock:(nullable YYBTapedActionBlock)completeBlock;
- (void)createDataModel:(MGAccessorModel *)dataModel completeBlock:(nullable YYBTapedActionBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
- (void)updateDataModel:(MGAccessorModel *)dataModel shouldUpdateModel:(nonnull MGAccessorModel *)shouldUpdateModel completeBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
- (void)deleteDataModel:(MGAccessorModel *)dataModel completeBlock:(nullable YYBTapedActionBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
- (void)deleteLocalDataModel:(MGAccessorModel *)dataModel completeBlock:(nullable YYBTapedActionBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
// 清空本地数据，例如用户退出使用
- (void)clearAllLocalDataWithCompleteBlock:(nullable YYBTapedActionBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

@end

NS_ASSUME_NONNULL_END
