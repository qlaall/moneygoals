//
//  MGCommonAccessor.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGCommonAccessor : NSObject

+ (MGCommonAccessor *)shared;

- (void)clearAllLocalData:(YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;
- (void)saveToLocalWithRecordModels:(NSArray *)recordModels accountModels:(NSArray *)accountModels categoryModels:(NSArray *)categoryModels completeBlock:(YYBTapedActionBlock)completeBlock;

// 从oss拉取所有的数据
- (void)getAndSaveAllOssDataWithCompleteBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;
// 将所有的本地数据，上传到oss
- (void)saveAllDataToOssWithCompleteBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;
// 所有本地未上传过的数据，上传到oss
- (void)saveUnsyncDataToOssWithCompleteBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

@end

NS_ASSUME_NONNULL_END
