//
//  MGAccessorModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGAccessorModel : NSObject

@property (nonatomic) NSUInteger uniqueId;
@property (nonatomic) NSUInteger pathUniqueId;

@property (nonatomic, copy) NSString *create_userId;

- (NSString *)getOssKey;

- (BOOL)shouldUpload;
// 是否是默认的，如果是默认的不能删除
// 只有类别和账户才有默认的
- (BOOL)isStatic;

@end

NS_ASSUME_NONNULL_END
