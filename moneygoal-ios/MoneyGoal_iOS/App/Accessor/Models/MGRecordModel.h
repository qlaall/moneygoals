//
//  MGReceiptModel.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/13.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccessorModel.h"
#import "MGAlertModel.h"
#import "MGReceiptModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordModel : MGAccessorModel

@property (nonatomic) MGRecordType recordType;
@property (nonatomic, strong) NSMutableArray *receiptModels;
@property (nonatomic) NSTimeInterval selected_at;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSDate *startDate; // 起始日期
@property (nonatomic) CGFloat gapMoney; // 倍率金额
//@property (nonatomic) CGFloat startMoney; // 起始金额
@property (nonatomic) NSUInteger lastDays; // 持续时间, 365存钱法为365天

// ======================================
@property (nonatomic, strong) MGAlertModel *alertModel;

- (NSArray *)getReceiptModelsWithBeginDate:(NSDate *)beginDate endDate:(NSDate *)endDate
                          categoryUniqueId:(NSUInteger)categoryUniqueId accountUniqueId:(NSUInteger)accountUniqueId;

// 获取本周的收入或者支出
- (NSDecimalNumber *)getWeeklyMoneyWithDate:(NSDate *)date isIncome:(BOOL)isIncome accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;
- (NSDecimalNumber *)getMonthlyMoneyWithDate:(NSDate *)date isIncome:(BOOL)isIncome accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;
// 获取本日的盈余
- (NSDecimalNumber *)getDayMoneyWithDate:(NSDate *)date accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;

// 获取往日的、打卡计划，30天之内
- (NSArray *)getRecentReceiptModels:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;
// 将recentReceiptModels按日期序列化
- (NSMutableArray *)getSortedRecentReceiptModels:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;

// ====================== 365存钱
// 创建存钱计划
- (void)createRecordProject;
- (CGFloat)getShouldSaveMoney;
- (CGFloat)getStoredMoney;

@end

NS_ASSUME_NONNULL_END
