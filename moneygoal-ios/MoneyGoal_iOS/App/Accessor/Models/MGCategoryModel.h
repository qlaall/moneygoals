//
//  MGCategoryModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccessorModel.h"

typedef NS_ENUM(NSInteger, MGCategoryType)
{
    MGCategoryTypeBuy,
    MGCategoryTypeDaily,
    MGCategoryTypeCar,
    MGCategoryTypeFunny,
    MGCategoryTypeLive,
    MGCategoryTypeMedical,
    MGCategoryTypeStudy,
    MGCategoryTypeGift,
    MGCategoryTypeDecorate,
    MGCategoryTypeFinical,
    MGCategoryTypeTravel,
    MGCategoryTypeOther,
    MGCategoryTypeHardware
};

typedef NS_ENUM(NSInteger, MGIncomeCategoryType)
{
    MGIncomeCategoryTypeLottery = 1001,
    MGIncomeCategoryTypeManage,
    MGIncomeCategoryTypeGift,
    MGIncomeCategoryTypeLend,
    MGIncomeCategoryTypeBonus,
    MGIncomeCategoryTypeParttime,
    MGIncomeCategoryTypeSalary,
    MGIncomeCategoryTypeIdle,
    MGIncomeCategoryTypeSubsidy,
    MGIncomeCategoryTypeExpense,
    MGIncomeCategoryTypeOther
};

NS_ASSUME_NONNULL_BEGIN

@interface MGCategoryModel : MGAccessorModel

@property (nonatomic) NSUInteger filterType; // 区分支出和收入等的分类

@property (nonatomic) MGCategoryType type;
@property (nonatomic, copy) NSString *name, *icon;
@property (nonatomic) NSInteger referenceCount;

@end

NS_ASSUME_NONNULL_END
