//
//  MGAccountModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccessorModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MGAccountType) {
    MGAccountTypeCash,
    MGAccountTypeTrust,
    MGAccountTypeOnline,
    MGAccountTypeCharge,
    MGAccountTypeInvest,
    MGAccountTypeStatic,
    MGAccountTypeShouldCharge,
    MGAccountTypeDebet,
    MGAccountTypeOther
};

@interface MGAccountModel : MGAccessorModel

@property (nonatomic) MGAccountType type;
@property (nonatomic, copy) NSString *name, *cardId, *icon, *remark;

@property (nonatomic) BOOL enableSum; // 是否计入总资产
@property (nonatomic, strong) NSDecimalNumber *initialMoney;
@property (nonatomic) NSInteger referenceCount;

// ======================================================
- (NSString *)getOssKey;

@end

NS_ASSUME_NONNULL_END
