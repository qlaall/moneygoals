//
//  MKDateMonthDayCell.h
//  365money-iOS
//
//  Created by alchemy on 2021/4/4.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "YYBCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MKDateMonthDayCell : YYBCollectionViewCell

@end

NS_ASSUME_NONNULL_END
