//
//  MKDateMonthDayCell.m
//  365money-iOS
//
//  Created by alchemy on 2021/4/4.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "MKDateMonthDayCell.h"

@implementation MKDateMonthDayCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_MEDIUM(15) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView.mas_centerY).offset(-2);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_MEDIUM(11) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.contentView.mas_centerY).offset(3);
    } configureBlock:nil];
    
    return self;
}

@end
