//
//  MKDatePickerView.h
//  365money-iOS
//
//  Created by alchemy on 2021/3/25.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MKDatePickerView : YYBBaseView

@property (nonatomic, strong) UICollectionView *collectionView, *weekendCollectionView;

@property (nonatomic, copy) void (^ dateSelectedBlock)(NSDate *date);
@property (nonatomic, copy) BOOL (^ dateSelectStatusBlock)(NSDate *date);

@property (nonatomic, copy) YYBTapedActionBlock accountTapedBlock, categoryTapedBlock;
- (void)configViewWithAccountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;

- (void)scrollToCurrentDate;
- (void)scrollToDate:(NSDate *)date;

@end

NS_ASSUME_NONNULL_END
