//
//  MKDateMonthCell.h
//  365money-iOS
//
//  Created by alchemy on 2021/3/30.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "YYBCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MKDateMonthCell : YYBCollectionViewCell <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *monthCollectionView;
@property (nonatomic, strong) NSDate *date;

@property (nonatomic, copy) void (^ dateSelectedBlock)(NSDate *date);
@property (nonatomic, copy) BOOL (^ dateSelectStatusBlock)(NSDate *date);

@end

NS_ASSUME_NONNULL_END
