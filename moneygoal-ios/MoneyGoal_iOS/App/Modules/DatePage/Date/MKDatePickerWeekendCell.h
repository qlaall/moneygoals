//
//  MKDatePickerWeekendCell.h
//  365money-iOS
//
//  Created by alchemy on 2021/3/25.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "YYBCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MKDatePickerWeekendCell : YYBCollectionViewCell

@end

NS_ASSUME_NONNULL_END
