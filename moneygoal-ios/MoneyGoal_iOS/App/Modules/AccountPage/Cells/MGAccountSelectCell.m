//
//  MGAccountSelectCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountSelectCell.h"

@implementation MGAccountSelectCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.contentView backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    } configureBlock:^(UIView *view) {
        [view cornerRadius:20];
    }];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(10);
//        make.top.equalTo(self.firstIconView);
        make.centerY.equalTo(self.firstIconView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(14);
        view.textColor = TEXT_COLOR_SECOND;
    }];
    
    self.secondIconView = [UIImageView createAtSuperView:self.contentView iconName:@"ico_common_select" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-20);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    } configureBlock:nil];
    
    [UIView createBottomSeparateViewAtSuperView:self color:APP_COLOR_LIGHT_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    return self;
}

- (void)configViewWithDataModel:(MGAccountModel *)dataModel status:(BOOL)status
{
    self.firstIconView.image = [UIImage imageNamed:dataModel.icon];
    self.firstStringLabel.text = dataModel.name;
    self.secondIconView.hidden = !status;
}

@end
