//
//  MGBaseIconCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseBorderDetailCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseIconCell : MGBaseBorderDetailCell

@end

NS_ASSUME_NONNULL_END
