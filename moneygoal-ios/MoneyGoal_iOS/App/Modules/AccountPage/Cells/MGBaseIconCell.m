//
//  MGBaseIconCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseIconCell.h"

@implementation MGBaseIconCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createAtSuperView:self.containerView iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(45);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
    }];
    
    self.secondIconView = [UIImageView createAtSuperView:self.containerView iconName:@"next" constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6, 11));
        make.centerY.equalTo(self.containerView);
        make.right.equalTo(self.containerView).offset(-20);
    } configureBlock:nil];
    
    self.thirdIconView = [UIImageView createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.secondIconView.mas_left).offset(-10);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UIImageView *view) {
        [view cornerRadius:15];
        view.backgroundColor = APP_COLOR_SAPERATE;
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(nonnull NSString *)detailValue iconName:(nonnull NSString *)iconName {
    self.firstStringLabel.text = titleValue;
    self.thirdIconView.image = [UIImage imageNamed:detailValue];
    self.firstIconView.image = [UIImage imageNamed:iconName];
}

@end
