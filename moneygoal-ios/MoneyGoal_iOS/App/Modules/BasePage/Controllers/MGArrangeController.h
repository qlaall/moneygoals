//
//  MGArrangeController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCollectionController.h"
#import "MGBaseFlowLayout.h"
#import "MGBaseActionsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGArrangeController : MGCollectionController

@property (nonatomic, strong) MGBaseActionsView *actionsView;

- (void)handleArrangeComplete;
- (void)handleCreate;

- (NSString *)emptyModelsRemindStr;
- (void)configFlowLayout:(MGBaseFlowLayout *)layout;

- (NSMutableArray *)getArrangeDataModels;

@end

NS_ASSUME_NONNULL_END
