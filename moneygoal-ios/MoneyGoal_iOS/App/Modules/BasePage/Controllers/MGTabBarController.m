//
//  MGTabBarController.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/7.
//

#import "MGTabBarController.h"
#import "MGRecordsController.h"
#import "MGReceiptController.h"
#import "MGSettingController.h"

@interface MGTabBarController ()
@property (nonatomic, strong) MGRecordModel *recordModel;
@property (nonatomic, strong) YYBNavigationBarButton *centerTabBar;

@end

@implementation MGTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.plainTitleColor = [UIColor colorWithHexValue:0x959595];
    self.selectedTitleColor = [UIColor colorWithHexValue:0x000000];
    self.titleFont = YYBFONT_NORMAL(10);
    
    @weakify(self);
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:MGNOTI_RECORDCHANGE object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self);
        MGRecordModel *selectModel = [MGRecordAccessor.shared getSelectedRecordModel];
        self.recordModel = selectModel;
        
        if (selectModel.recordType == MGRecordType365Money) {
            [self.centerTabBar.button setBackgroundImage:[UIImage new] forState:0];
        } else {
            [self.centerTabBar.button setBackgroundImage:[UIImage imageNamed:@"tab_add"] forState:0];
        }
    }];
}

- (NSArray *)customViewControllerNames {
    return @[@"MGRecordsController", @"MGSettingController"];
}

- (NSMutableArray<YYBNavigationBarContainer *> *)customTabBarContainerList {
    YYBNavigationBarControl *tab1 = [self commonContainerWithPlainStateIcon:@"tabs_1_n" selectedStateIcon:@"tabs_1_s" title:@"账本"];
    tab1.imageSize = CGSizeMake(25, 25);
    tab1.iconEdgeInsets = UIEdgeInsetsMake(6, 0, 0, 0);
    tab1.labelEdgeInsets = UIEdgeInsetsMake(3, 0, 0, 0);
    
    self.centerTabBar = [YYBNavigationBarButton buttonWithConfigureBlock:^(YYBNavigationBarButton *container, UIButton *view) {
        container.contentSize = CGSizeMake(60, 60);
        [view setBackgroundImage:[UIImage imageNamed:@"tab_add"] forState:0];
        [view cornerRadius:30];
    } tapedActionBlock:nil];
    
    YYBNavigationBarControl *tab3 = [self commonContainerWithPlainStateIcon:@"tabs_3_n" selectedStateIcon:@"tabs_3_s" title:@"其他"];
    tab3.imageSize = CGSizeMake(25, 25);
    tab3.iconEdgeInsets = UIEdgeInsetsMake(6, 0, 0, 0);
    tab3.labelEdgeInsets = UIEdgeInsetsMake(3, 0, 0, 0);
    
    return [@[tab1, self.centerTabBar, tab3] mutableCopy];
}

- (BOOL)shouldHandleHitTestWithTabBar:(YYBTabBar *)tabBar {
    return NO;
}

- (CGSize)containerSizeWithComponentIndex:(NSInteger)componentIndex {
    if (componentIndex == 1) {
        return CGSizeMake(60, 60);
    }
    return CGSizeMake((kScreenWidth - 60) / 2, 49);
}

- (BOOL)containerCouldSelectedWithTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex {
    if (componentIndex == 1) {
        if (self.recordModel.recordType != MGRecordType365Money) {
            [[NSNotificationCenter defaultCenter] postNotificationName:MGNOTI_CREATERECEIPT object:nil];
        }
        return NO;
    }
    return YES;
}

- (void)didSelectedContainerInTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex {
    [[[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight] impactOccurred];
    
    if (componentIndex == 0) {
        self.selectedIndex = 0;
    } else if (componentIndex == 2) {
        self.selectedIndex = 1;
    }
}

@end
