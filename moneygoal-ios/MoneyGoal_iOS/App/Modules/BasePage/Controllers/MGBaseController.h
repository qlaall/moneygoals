//
//  MGBaseController.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/7.
//

#import "YYBViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseController : YYBViewController

@property (nonatomic) BOOL enableNavigationShadows;

@end

NS_ASSUME_NONNULL_END
