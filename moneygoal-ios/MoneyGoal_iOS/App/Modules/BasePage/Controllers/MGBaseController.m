//
//  MGBaseController.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/7.
//

#import "MGBaseController.h"

@interface MGBaseController ()

@end

@implementation MGBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar {
    [super configCustomNavigationBar:navigationBar];
    
    navigationBar.backgroundColor = APP_COLOR_BACKGROUND;
    
    if (self.enableNavigationShadows) {
        navigationBar.shadowView.backgroundColor = [UIColor whiteColor];
        [navigationBar.shadowView setLayerShadow:[UIColor colorWithHexValue:0xE9E7E7 alpha:0.5] offset:CGSizeMake(0, 2) radius:10];
    }
}

- (void)configCustomReturnButton:(YYBNavigationBarControl *)backButton {
    [super configCustomReturnButton:backButton];
    
    [backButton setBarButtonTitle:@"" controlState:0];
    [backButton setBarButtonImage:[UIImage imageNamed:@"back"] controlState:0];
    
    backButton.imageSize = CGSizeMake(30, 44);
    backButton.iconEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
}

- (void)configCustomPageTitleView:(YYBNavigationBarLabel *)titleView {
    [super configCustomPageTitleView:titleView];
    
    titleView.label.font = FONT_MEDIUM(17);
}

@end
