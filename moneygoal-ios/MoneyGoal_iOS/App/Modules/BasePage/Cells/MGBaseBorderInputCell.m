//
//  MGBaseBorderInputCell.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseBorderInputCell.h"

@implementation MGBaseBorderInputCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createAtSuperView:self.containerView iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(45);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
    }];
        
    self.inputStringView = [UITextField createAtSuperView:self.containerView leftViewOffset:0 cornerRadius:0 fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-20);
        make.left.equalTo(self.containerView.mas_centerX);
        make.top.bottom.equalTo(self.containerView);
    } configureBlock:^(UITextField *view) {
        view.clearButtonMode = UITextFieldViewModeWhileEditing;
        view.textAlignment = NSTextAlignmentRight;
    }];
    
    @weakify(self);
    [self.inputStringView.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        if (self.stringChangedBlock) {
            self.stringChangedBlock(x);
        }
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(nonnull NSString *)detailValue iconName:(nonnull NSString *)iconName {
    self.firstStringLabel.text = titleValue;
    
    self.inputStringView.placeholder = [NSString stringWithFormat:@"输入%@", titleValue];
    self.inputStringView.text = detailValue;
    self.firstIconView.image = [UIImage imageNamed:iconName];
}

@end
