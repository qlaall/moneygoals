//
//  MGTableViewCell.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MGCornerType) {
    MGCornerTypeTop,
    MGCornerTypeCenter,
    MGCornerTypeBottom,
    MGCornerTypeAll
};

@interface MGTableViewCell : YYBTableViewCell

- (void)configViewWithCornerType:(MGCornerType)cornerType;

@end

NS_ASSUME_NONNULL_END
