//
//  MGTableViewCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableViewCell.h"

@implementation MGTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.containerView = [UIView createViewAtSuperView:self.contentView backgroundColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 10, 0, 10));
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithCornerType:(MGCornerType)cornerType {
    if (cornerType == MGCornerTypeTop) {
        self.containerView.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner;
        self.containerView.layer.cornerRadius = 8;
    } else if (cornerType == MGCornerTypeCenter) {
        self.containerView.layer.masksToBounds = NO;
        self.containerView.layer.cornerRadius = 0;
    } else if (cornerType == MGCornerTypeBottom) {
        self.containerView.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
        self.containerView.layer.cornerRadius = 8;
    } else if (cornerType == MGCornerTypeAll) {
        self.containerView.layer.masksToBounds = YES;
        self.containerView.layer.cornerRadius = 8;
    }
}

@end
