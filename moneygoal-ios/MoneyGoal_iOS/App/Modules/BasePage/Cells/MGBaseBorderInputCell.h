//
//  MGBaseBorderInputCell.h
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseBorderInputCell : MGBaseCornerCell

@end

NS_ASSUME_NONNULL_END
