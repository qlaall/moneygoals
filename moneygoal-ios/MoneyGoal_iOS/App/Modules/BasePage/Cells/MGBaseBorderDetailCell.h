//
//  MGBaseBorderDetailCell.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseBorderDetailCell : MGBaseCornerCell

@end

NS_ASSUME_NONNULL_END
