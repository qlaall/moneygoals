//
//  MGAlertView.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/15.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertView : YYBAlertView

@end

NS_ASSUME_NONNULL_END
