//
//  MGBaseActionsView.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseActionsView.h"

@implementation MGBaseActionsView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = APP_COLOR_BACKGROUND;
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.width.mas_equalTo(160);
        make.bottom.equalTo(self).offset(-10 - [UIDevice safeAreaInsetsBottom]);
        make.top.equalTo(self).offset(10);
    } configureBlock:^(UIButton *view) {
        [view setTitle:@"添加" forState:0];
        view.titleLabel.font = FONT_REGULAR(15);
        view.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [view setBackgroundImage:[APP_COLOR_MAIN colorWithAlphaComponent:1].colorToUIImage forState:0];
        [view setImage:[UIImage imageNamed:@"action_add"] forState:0];
        [view setImage:[UIImage imageNamed:@"action_add"] forState:UIControlStateHighlighted];
        [view cornerRadius:22.5];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.createBlock) {
            self.createBlock();
        }
    }];
    
    self.secondActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.width.mas_equalTo(120);
        make.bottom.equalTo(self).offset(-10 - [UIDevice safeAreaInsetsBottom]);
        make.top.equalTo(self).offset(10);
    } configureBlock:^(UIButton *view) {
        view.backgroundColor = [UIColor whiteColor];
        [view setTitle:@"删除" forState:0];
        [view setTitleColor:TEXT_COLOR_SECOND forState:0];
        view.titleLabel.font = FONT_REGULAR(15);
        [view setImage:[UIImage imageNamed:@"action_delete"] forState:0];
        [view setImage:[UIImage imageNamed:@"action_delete"] forState:UIControlStateHighlighted];
        view.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        [view cornerRadius:22.5];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.deleteBlock) {
            self.deleteBlock();
        }
    }];
    
    self.thirdActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.secondActionButton.mas_right).offset(10);
        make.right.equalTo(self).offset(-10);
        make.bottom.equalTo(self).offset(-10 - [UIDevice safeAreaInsetsBottom]);
        make.top.equalTo(self).offset(10);
    } configureBlock:^(UIButton *view) {
        [view setTitle:@"保存修改" forState:0];
        view.titleLabel.font = FONT_REGULAR(15);
        view.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
//        [view setTitleColor:APP_COLOR_MAIN forState:0];
        [view setBackgroundImage:[APP_COLOR_MAIN colorWithAlphaComponent:1].colorToUIImage forState:0];
        [view setImage:[UIImage imageNamed:@"action_check"] forState:0];
        [view setImage:[UIImage imageNamed:@"action_check"] forState:UIControlStateHighlighted];
        [view cornerRadius:22.5];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.updateBlock) {
            self.updateBlock();
        }
    }];
    
    return self;
}

- (void)changeCreate {
    self.firstActionButton.hidden = NO;
    self.secondActionButton.hidden = YES;
    self.thirdActionButton.hidden = YES;
    
    [self.firstActionButton setTitle:@"添加" forState:0];
    [self.firstActionButton setImage:[UIImage imageNamed:@"action_add"] forState:0];
    [self.firstActionButton setImage:[UIImage imageNamed:@"action_add"] forState:UIControlStateHighlighted];
}

- (void)changeUpdate {
    self.firstActionButton.hidden = YES;
    self.secondActionButton.hidden = NO;
    self.thirdActionButton.hidden = NO;
}

- (void)changeCompleteCreate {
    self.firstActionButton.hidden = NO;
    self.secondActionButton.hidden = YES;
    self.thirdActionButton.hidden = YES;
    
    [self.firstActionButton setTitle:@"完成" forState:0];
    [self.firstActionButton setImage:[UIImage imageNamed:@"action_check"] forState:0];
    [self.firstActionButton setImage:[UIImage imageNamed:@"action_check"] forState:UIControlStateHighlighted];
}

@end
