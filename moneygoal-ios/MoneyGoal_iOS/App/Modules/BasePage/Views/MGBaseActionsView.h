//
//  MGBaseActionsView.h
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseActionsView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionBlock createBlock;
@property (nonatomic, copy) YYBTapedActionBlock updateBlock;
@property (nonatomic, copy) YYBTapedActionBlock deleteBlock;

- (void)changeCompleteCreate;
- (void)changeUpdate;
- (void)changeCreate;

@end

NS_ASSUME_NONNULL_END
