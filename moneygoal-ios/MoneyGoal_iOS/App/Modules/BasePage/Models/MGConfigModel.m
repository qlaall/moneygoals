//
//  MGConfigModel.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGConfigModel.h"

@implementation MGConfigModel

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _enableOSS = YES;
    _enableTimeMemory = NO;
    
    return self;
}

@end
