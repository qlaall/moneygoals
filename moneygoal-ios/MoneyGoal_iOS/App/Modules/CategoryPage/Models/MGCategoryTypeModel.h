//
//  MGCategoryTypeModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/27.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGCategoryTypeModel : NSObject

@property (nonatomic) MGCategoryType type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic) BOOL enableOpen;

@end

NS_ASSUME_NONNULL_END
