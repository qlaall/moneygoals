//
//  MGCategoryIconController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoryIconController.h"

#import "MGIconCell.h"
#import "MGIconSectionCell.h"

@interface MGCategoryIconController ()
@property (nonatomic, strong) UISegmentedControl *segmentedView;

@property (nonatomic) NSInteger selectActionIndex;

@end

@implementation MGCategoryIconController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView registerClass:[MGIconSectionCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MGIconSectionCell"];
    [self.collectionView reloadData];
}

- (UIView *)customNavigationBarTitleView {
    self.segmentedView = [[UISegmentedControl alloc] initWithItems:@[@"支出图标", @"收入图标"]];
    self.segmentedView.selectedSegmentIndex = 0;
    [self.segmentedView addTarget:self action:@selector(handleSegmentAction) forControlEvents:UIControlEventValueChanged];
    return self.segmentedView;
}

- (void)handleSegmentAction {
    self.selectActionIndex = self.segmentedView.selectedSegmentIndex;
    [self.collectionView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.selectActionIndex == 0) {
        return 11;
    } else if (self.selectActionIndex == 1) {
        return 8;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kScreenWidth, 10);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    MGIconSectionCell *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MGIconSectionCell" forIndexPath:indexPath];
    return view;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.selectActionIndex == 0) {
        if (section == 0) return 8;
        if (section == 1) return 4;
        if (section == 2) return 6;
        if (section == 3) return 7;
        if (section == 4) return 7;
        if (section == 5) return 3;
        if (section == 6) return 6;
        if (section == 7) return 5;
        if (section == 8) return 4;
        if (section == 9) return 4;
        if (section == 10) return 4;
    } else if (self.selectActionIndex == 1) {
        if (section == 0) return 3;
        if (section == 1) return 4;
        if (section == 2) return 3;
        if (section == 3) return 1;
        if (section == 4) return 2;
        if (section == 5) return 2;
        if (section == 6) return 2;
        if (section == 7) return 1;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemSize = (kScreenWidth - 90) / 6;
    return CGSizeMake(itemSize, itemSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGIconCell"];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MGIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGIconCell" forIndexPath:indexPath];
    
    NSInteger startIndex = (indexPath.section * 100 + indexPath.row) + 100;
    
    NSString *prefix = self.selectActionIndex == 0 ? @"cate_type_" : @"cate_income_type_";
    NSString *iconName = [NSString stringWithFormat:@"%@%d",prefix, (int)startIndex];
    [cell configViewWithDataModel:iconName];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger startIndex = (indexPath.section * 100 + indexPath.row) + 100;
    
    NSString *prefix = self.selectActionIndex == 0 ? @"cate_type_" : @"cate_income_type_";
    NSString *iconName = [NSString stringWithFormat:@"%@%d",prefix, (int)startIndex];
    
    if (self.selectIconBlock) {
        self.selectIconBlock(iconName);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)customPageTitle {
    return @"选择图标";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.left = 20;
    insets.right = 20;
    insets.top += 20;
    return insets;
}

@end
