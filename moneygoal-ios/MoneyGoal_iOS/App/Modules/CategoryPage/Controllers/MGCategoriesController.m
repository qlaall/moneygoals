//
//  MGCategoriesController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoriesController.h"
#import "MGCategoryController.h"

#import "MGCategoryTypeView.h"
#import "MGBaseActionsView.h"

#import "MGCategoryTypeModel.h"

#import "MGCategoryCell.h"

@interface MGCategoriesController ()
@property (nonatomic, strong) MGBaseActionsView *actionsView;
@property (nonatomic, strong) UISegmentedControl *segmentedView;

@property (nonatomic) NSInteger selectActionIndex;

@end

@implementation MGCategoriesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    NSArray *categoryTypeModels = [MGCategoryAccessor getStandardCategoryTypes];
    self.dataList = [NSArray yy_modelArrayWithClass:[MGCategoryTypeModel class] json:categoryTypeModels].mutableCopy;
    [self.tableView reloadData];
}

- (void)afterConfigViewAction {
    [super afterConfigViewAction];
    
    @weakify(self);
    self.actionsView = [MGBaseActionsView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.mas_equalTo(65 + [UIDevice safeAreaInsetsBottom]);
    } configureBlock:^(MGBaseActionsView *view) {
        view.createBlock = ^{
            @strongify(self);
            [self createAction];
        };
    }];
    
    [self.actionsView changeCreate];
}

- (UIView *)customNavigationBarTitleView {
    self.segmentedView = [[UISegmentedControl alloc] initWithItems:@[@"支出", @"收入"]];
    self.segmentedView.selectedSegmentIndex = 0;
    [self.segmentedView addTarget:self action:@selector(handleSegmentAction) forControlEvents:UIControlEventValueChanged];
    return self.segmentedView;
}

- (void)handleSegmentAction {
    self.selectActionIndex = self.segmentedView.selectedSegmentIndex;
    
    if (self.selectActionIndex == 0) {
        NSArray *categoryTypeModels = [MGCategoryAccessor getStandardCategoryTypes];
        self.dataList = [NSArray yy_modelArrayWithClass:[MGCategoryTypeModel class] json:categoryTypeModels].mutableCopy;
    } else if (self.selectActionIndex == 1) {
        NSArray *categoryTypeModels = [MGCategoryAccessor getStandardIncomeCategoryTypes];
        self.dataList = [NSArray yy_modelArrayWithClass:[MGCategoryTypeModel class] json:categoryTypeModels].mutableCopy;
    }
    [self.tableView reloadData];
}

- (void)createActionWithCategoryType:(MGCategoryType)type section:(NSInteger)section {
    @weakify(self);
    [YYBViewRouter openOnClass:[MGCategoryController class] configureBlock:^(MGCategoryController *obj) {
        @strongify(self);
        obj.createCategoryType = type;
        obj.createModelBlock = ^(NSUInteger uniqueId) {
            [self.tableView reloadSection:section withRowAnimation:UITableViewRowAnimationNone];
            if (self.createModelBlock) {
                self.createModelBlock(uniqueId);
            }
        };
    }];
}

- (void)createAction {
    @weakify(self);
    [YYBViewRouter openOnClass:[MGCategoryController class] configureBlock:^(MGCategoryController *obj) {
        @strongify(self);
        obj.createModelBlock = ^(NSUInteger uniqueId) {
            [self.tableView reloadData];
            if (self.createModelBlock) {
                self.createModelBlock(uniqueId);
            }
        };
    }];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGCategoryCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    MGCategoryTypeModel *categoryModel = self.dataList[section];
    NSArray *dataModels = [MGCategoryAccessor.shared getCategoryModelsWithCategoryType:categoryModel.type];
    return dataModels.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MGCategoryTypeView *view = [[MGCategoryTypeView alloc] init];
    
    MGCategoryTypeModel *categoryModel = self.dataList[section];
    @weakify(self);
    view.tapedActionBlock = ^{
        @strongify(self);
        [self createActionWithCategoryType:categoryModel.type section:section];
    };
    [view configViewWithTitleValue:categoryModel.name];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MGCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGCategoryCell"];
    
    MGCategoryTypeModel *categoryModel = self.dataList[indexPath.section];
    NSArray *dataModels = [MGCategoryAccessor.shared getCategoryModelsWithCategoryType:categoryModel.type];
    MGCategoryModel *dataModel = dataModels[indexPath.row];
    
    [cell configViewWithDataModel:dataModel];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MGCategoryTypeModel *categoryModel = self.dataList[indexPath.section];
    NSArray *dataModels = [MGCategoryAccessor.shared getCategoryModelsWithCategoryType:categoryModel.type];
    MGCategoryModel *dataModel = dataModels[indexPath.row];
    
    if (!dataModel.isStatic) {
        @weakify(self);
        [YYBViewRouter openOnClass:[MGCategoryController class] configureBlock:^(MGCategoryController *obj) {
            @strongify(self);
            obj.updateModel = dataModel;
            obj.deleteModelBlock = ^(NSUInteger uniqueId) {
                [self.tableView reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];
                if (self.deleteModelBlock) {
                    self.deleteModelBlock(uniqueId);
                }
            };
            obj.updateModelBlock = ^(NSUInteger uniqueId) {
                [self.tableView reloadData];
                if (self.updateModelBlock) {
                    self.updateModelBlock(uniqueId);
                }
            };
        }];
    }
}

- (NSString *)customPageTitle {
    return @"分类列表";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.bottom += 65;
    return insets;
}

@end

