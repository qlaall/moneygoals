//
//  MGCategoryTypeCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCategoryTypeCell : YYBTableViewCell

@end

NS_ASSUME_NONNULL_END
