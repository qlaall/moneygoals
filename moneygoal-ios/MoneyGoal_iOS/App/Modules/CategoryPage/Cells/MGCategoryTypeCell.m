//
//  MGCategoryTypeCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoryTypeCell.h"

@implementation MGCategoryTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(16) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.centerY.equalTo(self.contentView);
    } configureBlock:nil];
    
    self.secondIconView = [UIImageView createAtSuperView:self.contentView iconName:@"category_select" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-20);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    } configureBlock:nil];
    
    [UIView createBottomSeparateViewAtSuperView:self color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status
{
    self.firstStringLabel.text = titleValue;
    self.secondIconView.hidden = !status;
}

@end
