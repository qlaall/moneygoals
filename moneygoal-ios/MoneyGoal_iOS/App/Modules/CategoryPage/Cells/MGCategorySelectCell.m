//
//  MGCategorySelectCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategorySelectCell.h"

@implementation MGCategorySelectCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.contentView backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(8);
        make.centerX.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    } configureBlock:^(UIView *view) {
        [view cornerRadius:20];
    }];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.top.equalTo(self.firstIconView.mas_bottom).offset(5);
        make.centerX.equalTo(self.firstIconView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(12);
        view.textColor = TEXT_COLOR_SECOND;
    }];
    
    self.secondIconView = [UIImageView createAtSuperView:self.contentView iconName:@"ico_common_select" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.firstIconView).offset(1);
        make.bottom.equalTo(self.firstIconView).offset(1);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithDataModel:(MGCategoryModel *)dataModel status:(BOOL)status
{
    self.firstStringLabel.text = dataModel.name;
    self.secondIconView.hidden = !status;
    self.firstIconView.image = [UIImage imageNamed:dataModel.icon];
    
//    if (status) {
////        [self.firstIconView cornerRadius:20 width:2 color:APP_COLOR_MAIN];
//        self.firstIconView.backgroundColor = APP_COLOR_MAIN_LIGHT;
//    } else {
////        [self.firstIconView cornerRadius:20 width:0 color:[UIColor clearColor]];
//        self.firstIconView.backgroundColor = APP_COLOR_SAPERATE;
//    }
}

@end
