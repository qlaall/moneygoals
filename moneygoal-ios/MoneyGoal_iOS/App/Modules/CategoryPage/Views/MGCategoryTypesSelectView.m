//
//  MGCategoryTypesSelectView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoryTypesSelectView.h"

#import "MGCategoryTypeCell.h"

@interface MGCategoryTypesSelectView ()
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation MGCategoryTypesSelectView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self);
        make.height.mas_equalTo(65);
    } configureBlock:^(UILabel *view) {
        view.text = @"选择一个主分类";
    }];
    
    self.tableView = [UITableView createAtSuperView:self delagateBlock:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(400);
    } dequeueCellIdentifiers:@[@"MGCategoryTypeCell"] configureBlock:nil];
    
    [UIView createTopSeparateViewAtSuperView:self color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(65, 0, 0, 0)];
    
    return self;
}

- (void)setSelectType:(MGCategoryType)selectType {
    _selectType = selectType;
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [MGCategoryAccessor getStandardCategoryTypes].count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MGCategoryTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGCategoryTypeCell"];
    
    NSDictionary *dict = [MGCategoryAccessor getStandardCategoryTypes][indexPath.row];
    MGCategoryType type = [dict[@"type"] integerValue];
    BOOL isSelect = type == self.selectType;
    
    [cell configViewWithTitleValue:dict[@"name"] status:isSelect];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [MGCategoryAccessor getStandardCategoryTypes][indexPath.row];
    if (self.selectTypeBlock) {
        MGCategoryType type = [dict[@"type"] integerValue];
        self.selectTypeBlock(type);
    }
}

@end
