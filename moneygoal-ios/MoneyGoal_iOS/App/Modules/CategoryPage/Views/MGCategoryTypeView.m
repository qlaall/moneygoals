//
//  MGCategoryTypeView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/27.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoryTypeView.h"

@implementation MGCategoryTypeView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } configureBlock:^(UIButton *view) {
        view.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
        view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        view.titleLabel.font = FONT_REGULAR(14);
        [view setTitleColor:TEXT_COLOR_FIRST forState:0];
        [view setBackgroundImage:[YYBCOLOR(0xFAFAFA) colorToUIImage] forState:0];
        [view setBackgroundImage:[APP_COLOR_MEDIUM_SAPERATE colorToUIImage] forState:UIControlStateHighlighted];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    [UIView createBottomSeparateViewAtSuperView:self color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsZero];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    [self.firstActionButton setTitle:titleValue forState:0];
}

@end
