//
//  MGCategoryTypesSelectView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCategoryTypesSelectView : YYBBaseView

@property (nonatomic, copy) void (^ selectTypeBlock)(MGCategoryType type);
@property (nonatomic) MGCategoryType selectType;

@end

NS_ASSUME_NONNULL_END
