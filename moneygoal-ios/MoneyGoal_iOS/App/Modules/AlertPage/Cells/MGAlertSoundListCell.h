//
//  MGAlertSoundListCell.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertSoundListCell : YYBTableViewCell

@property (nonatomic, copy) BOOL (^ soundSelectedStatusBlock)(NSInteger soundIndex);
@property (nonatomic, copy) void (^ selectedSoundBlock)(MGAlertSoundModel *dataModel);

@end

NS_ASSUME_NONNULL_END
