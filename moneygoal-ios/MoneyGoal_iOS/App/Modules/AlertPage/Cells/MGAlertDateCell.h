//
//  MGAlertDateCell.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/13.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertDateCell : MGTableViewCell

- (void)configViewWithDate:(NSDate *)date;

@end

NS_ASSUME_NONNULL_END
