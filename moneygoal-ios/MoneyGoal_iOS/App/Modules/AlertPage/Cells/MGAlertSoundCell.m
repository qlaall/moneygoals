//
//  MGAlertSoundCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertSoundCell.h"

@implementation MGAlertSoundCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textAlignment = NSTextAlignmentCenter;
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status {
    self.firstStringLabel.text = titleValue;
    
    self.firstStringLabel.textColor = status ? [UIColor whiteColor] : TEXT_COLOR_FIRST;
    self.firstStringLabel.backgroundColor = status ? APP_COLOR_MAIN : [UIColor whiteColor];
    [self.firstStringLabel cornerRadius:16 width:1 color:status ? APP_COLOR_MAIN : APP_COLOR_SAPERATE];
}

@end
