//
//  MGAlertSoundModel.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertSoundModel : NSObject

@property (nonatomic) NSInteger soundMode;
@property (nonatomic, copy) NSString *iconName;
@property (nonatomic, copy) NSString *soundFileName;
@property (nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
