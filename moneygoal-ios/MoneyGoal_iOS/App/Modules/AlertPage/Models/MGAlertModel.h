//
//  MGAlertModel.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGAlertSoundModel.h"
#import "MGAlertSoundModel+MGAdd.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MGAlertType) {
    MGAlertTypeNone,
    MGAlertTypeDay
};

@interface MGAlertModel : NSObject

@property (nonatomic) MGAlertType type;
@property (nonatomic, copy) NSString *content;
@property (nonatomic) NSDate *startDate;

@property (nonatomic, strong) MGAlertSoundModel *soundModel;

- (NSString *)getAlertType;

@end

NS_ASSUME_NONNULL_END
