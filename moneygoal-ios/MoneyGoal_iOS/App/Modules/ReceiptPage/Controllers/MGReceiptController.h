//
//  MGReceiptController.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/27.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptController : MGBaseController

@property (nonatomic, strong) MGRecordModel *recordModel;
@property (nonatomic, strong) MGReceiptModel *dataModel;
// 传入的、修改的model
@property (nonatomic, strong) MGReceiptModel *updateModel;

// 日期页面传入的日期
@property (nonatomic, strong) NSDate *selectedDate;

@property (nonatomic, copy) YYBTapedActionBlock updateSuccBlock;
@property (nonatomic, copy) YYBTapedActionBlock deleteSuccBlock;

@end

NS_ASSUME_NONNULL_END
