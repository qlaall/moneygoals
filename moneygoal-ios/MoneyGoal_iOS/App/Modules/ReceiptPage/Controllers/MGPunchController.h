//
//  MGPunchController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/29.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGPunchController : MGTableController

@property (nonatomic, strong) MGReceiptModel *dataModel;
// 传入的、修改的model
@property (nonatomic, strong) MGReceiptModel *updateModel;

@property (nonatomic, copy) YYBTapedActionBlock updateSuccBlock;

@end

NS_ASSUME_NONNULL_END
