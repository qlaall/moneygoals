//
//  MGReceiptEmptyCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/29.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptEmptyCell.h"

@implementation MGReceiptEmptyCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.firstIconView = [UIImageView createAtSuperView:self.contentView iconName:@"empty_state" constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(184, 84));
        make.top.equalTo(self.contentView).offset(80);
    } configureBlock:^(UIImageView *view) {
        view.contentMode = UIViewContentModeScaleAspectFit;
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_MEDIUM(16) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.firstIconView.mas_bottom).offset(40);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(5);
    } configureBlock:nil];
    
    self.firstStringLabel.text = @"没有账单";
    self.secondStringLabel.text = @"现在开始，立刻记一笔吧";
    
    return self;
}

@end
