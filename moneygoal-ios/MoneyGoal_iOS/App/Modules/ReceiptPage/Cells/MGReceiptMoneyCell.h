//
//  MGReceiptMoneyCell.h
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/21.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptMoneyCell : YYBTableViewCell

- (void)configViewWithTitleValue:(NSString *)titleValue receiptType:(MGReceiptType)receiptType isPunched:(BOOL)isPunched;

@end

NS_ASSUME_NONNULL_END
