//
//  MGReceiptMoneyCell.m
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/21.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "MGReceiptMoneyCell.h"
#import "NSString+ReceiptAdd.h"

@implementation MGReceiptMoneyCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_MEDIUM(48) textColor:[UIColor blackColor] constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.left.equalTo(self.contentView).offset(15);
        make.top.equalTo(self.contentView).offset(10);
    } configureBlock:^(UILabel *view) {
        view.textAlignment = NSTextAlignmentRight;
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:YYBFONT_NORMAL(16) textColor:[UIColor colorWithHexValue:0x8A8A8D] constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-20);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(-5);
    } configureBlock:^(UILabel *view) {
        view.textAlignment = NSTextAlignmentRight;
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue receiptType:(MGReceiptType)receiptType isPunched:(BOOL)isPunched {
    if (titleValue == nil) {
        self.firstStringLabel.text = @"0.00";
        self.firstStringLabel.textColor = TEXT_COLOR_SECOND;
        self.secondStringLabel.text = nil;
    } else {
        self.firstStringLabel.text = [NSString stringWithFormat:@"%.2f", [titleValue calculateData]];
        
        if (receiptType == MGReceiptTypePaid) {
            self.firstStringLabel.textColor = TEXT_COLOR_FIRST;
        } else if (receiptType == MGReceiptTypeIncome) {
            self.firstStringLabel.textColor = TEXT_COLOR_INCOME;
        } else {
            if (isPunched) {
                self.firstStringLabel.textColor = TEXT_COLOR_INCOME;
            } else {
                self.firstStringLabel.textColor = TEXT_COLOR_FIRST;
            }
        }
        
        if ([titleValue containsSymbol]) {
            self.secondStringLabel.text = titleValue;
        } else {
            self.secondStringLabel.text = nil;
        }
    }
}

@end
