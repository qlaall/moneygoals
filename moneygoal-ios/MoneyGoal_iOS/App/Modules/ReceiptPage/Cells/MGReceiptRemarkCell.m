//
//  MGReceiptRemarkCell.m
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/12.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "MGReceiptRemarkCell.h"

@interface MGReceiptRemarkCell () <UITextFieldDelegate>

@end

@implementation MGReceiptRemarkCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-30);
        make.height.mas_equalTo(50);
    }];
    
    self.firstIconView = [UIImageView createAtSuperView:self.containerView iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UIImageView *view) {
        view.image = [UIImage imageNamed:@"ico_common_remark"];
    }];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(45);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
        view.text = @"备注";
    }];
    
    self.inputStringView = [UITextField createAtSuperView:self.containerView leftViewOffset:0 cornerRadius:0 fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-20);
        make.left.equalTo(self.containerView).offset(100);
        make.top.bottom.equalTo(self.containerView);
    } configureBlock:^(UITextField *view) {
        view.clearButtonMode = UITextFieldViewModeWhileEditing;
        view.textAlignment = NSTextAlignmentRight;
        view.placeholder = @"输入备注";
    }];
    
    self.inputStringView.delegate = self;
    [self.inputStringView addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-5);
        make.bottom.equalTo(self.contentView);
        make.top.equalTo(self.containerView.mas_bottom);
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(5);
        make.centerY.equalTo(self.secondStringLabel);
        make.size.mas_equalTo(CGSizeMake(55, 20));
    } configureBlock:^(UIButton *view) {
        view.titleLabel.font = FONT_REGULAR(12);
        [view setTitle:@"清空备注" forState:0];
        [view setTitleColor:TEXT_COLOR_SECOND forState:0];
//        [view cornerRadius:10 width:0.5 color:TEXT_COLOR_THIRD];
        
//        [view setBackgroundImage:[UIColor whiteColor].colorToUIImage forState:0];
//        [view setBackgroundImage:[[UIColor whiteColor] colorWithAlphaComponent:0.3].colorToUIImage forState:UIControlStateHighlighted];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        self.inputStringView.text = nil;
        [self textDidChange];
    }];
    
    return self;
}

- (void)textDidChange
{
    NSString *text = self.inputStringView.text;
    self.secondStringLabel.text = [NSString stringWithFormat:@"%d / 20",(int)text.length];
    self.firstActionButton.hidden = text.length == 0;
    
    if (self.stringChangedBlock) {
        self.stringChangedBlock(text);
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // 删除符号
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    NSString *text_str = textField.text;
    // 不能输入换行符
    if ([string isEqualToString:@"\n"]) {
        return NO;
    }
    
    if (text_str.length >= 20) {
        return NO;
    }
    
    return YES;
}

- (void)configViewWithTitleValue:(NSString *)titleValue {
    self.inputStringView.text = titleValue;
    self.firstActionButton.hidden = !titleValue || titleValue.length == 0;
    
    NSString *lengthStr = @(titleValue.length).stringValue;
    NSString *attrStr = [NSString stringWithFormat:@"%@ / 20 字符", lengthStr];
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:attrStr];
    if (titleValue.length > 20) {
        [attr addAttribute:NSFontAttributeName value:FONT_MEDIUM(12) range:NSMakeRange(0, lengthStr.length)];
        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexValue:0xF1625C] range:NSMakeRange(0, lengthStr.length)];
    } else {
        [attr addAttribute:NSFontAttributeName value:FONT_REGULAR(12) range:NSMakeRange(0, lengthStr.length)];
        [attr addAttribute:NSForegroundColorAttributeName value:TEXT_COLOR_FIRST range:NSMakeRange(0, lengthStr.length)];
    }
    self.secondStringLabel.attributedText = attr;
}

@end
