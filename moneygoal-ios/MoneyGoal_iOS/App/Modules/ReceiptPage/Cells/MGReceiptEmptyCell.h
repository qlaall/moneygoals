//
//  MGReceiptEmptyCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/29.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptEmptyCell : YYBTableViewCell

@end

NS_ASSUME_NONNULL_END
