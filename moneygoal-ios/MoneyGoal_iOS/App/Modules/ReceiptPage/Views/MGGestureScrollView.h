//
//  MGGestureScrollView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGGestureScrollView : UIScrollView

@end

NS_ASSUME_NONNULL_END
