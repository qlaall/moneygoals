//
//  MGReceiptSwitchView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/24.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptSwitchView : YYBBaseView

@property (nonatomic, copy) MGReceiptType (^ getSelectTypeBlock)(void);
@property (nonatomic, copy) YYBTapedActionIndexBlock receiptTypeSelectBlock;

- (void)handleChangeReceiptType;

@end

NS_ASSUME_NONNULL_END
