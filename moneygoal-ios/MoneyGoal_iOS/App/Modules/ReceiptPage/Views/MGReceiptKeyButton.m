//
//  MGReceiptKeyButton.m
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/5/15.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "MGReceiptKeyButton.h"

@interface MGReceiptKeyButton ()
@property (nonatomic, strong) UILabel *firstStringLabel;

@end

@implementation MGReceiptKeyButton

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    [self cornerRadius:6];
    [self setBackgroundImage:APP_COLOR_MAIN.colorToUIImage forState:0];
    [self setBackgroundImage:[APP_COLOR_MAIN colorWithAlphaComponent:0.6].colorToUIImage forState:UIControlStateHighlighted];
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:YYBFONT_BOLD(16) textColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    } configureBlock:^(UILabel *view) {
        view.numberOfLines = 2;
        view.text = @"完\n成";
    }];
    
    return self;
}

@end
