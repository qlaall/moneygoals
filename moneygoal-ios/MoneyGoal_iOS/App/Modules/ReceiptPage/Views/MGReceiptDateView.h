//
//  MGReceiptDateView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/28.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptDateView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionIndexBlock selectTypeBlock;
@property (nonatomic, copy) void (^ dateChangeBlock)(NSDate *date, BOOL dateOrTime);
@property (nonatomic) BOOL dateOrTime; // 0时间 1日期
@property (nonatomic, strong) NSDate *selectDate;

@end

NS_ASSUME_NONNULL_END
