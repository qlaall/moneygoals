//
//  MGReceiptDateView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/28.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptDateView.h"

@interface MGReceiptDateView ()
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UISegmentedControl *segmentedView;

@end

@implementation MGReceiptDateView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.wrapperView = [UIView createViewAtSuperView:self backgroundColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(65);
    } configureBlock:nil];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(16) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.center.equalTo(self.wrapperView);
    } configureBlock:^(UILabel *view) {
        view.text = @"选择日期";
    }];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_dismiss"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeCancel);
        }
    }];
    
    self.segmentedView = [[UISegmentedControl alloc] initWithItems:@[@"时间", @"日期"]];
    [self.segmentedView addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    [self.wrapperView addSubview:self.segmentedView];
    [self.segmentedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.wrapperView).offset(20);
        make.centerY.equalTo(self.wrapperView);
    }];
    
    self.datePicker = [UIDatePicker createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.wrapperView.mas_bottom);
        make.bottom.equalTo(self).offset(-55);
    } configureBlock:^(UIDatePicker *view) {
        view.maximumDate = [NSDate date];
        view.datePickerMode = UIDatePickerModeDateAndTime;
        view.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
        if (@available(iOS 13.4, *)) {
            view.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
    }];
    
    self.secondActionButton = [UIButton createAtSuperView:self cornerRadius:22.5 fontValue:FONT_MEDIUM(16) title:@"完成" titleColor:APP_COLOR_MAIN constraintBlock:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-5);
        make.centerX.equalTo(self);
        make.width.mas_equalTo(160);
        make.height.mas_equalTo(45);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[APP_COLOR_MAIN colorWithAlphaComponent:0.1].colorToUIImage forState:0];
    } tapedBlock:^(UIButton *view) {
        if (self.dateChangeBlock) {
            self.dateChangeBlock(self.datePicker.date, self.dateOrTime);
        }
    }];
    
    [UIView createTopSeparateViewAtSuperView:self color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(65, 0, 0, 0)];
    
    return self;
}

- (void)setDateOrTime:(BOOL)dateOrTime {
    _dateOrTime = dateOrTime;
    if (_dateOrTime) {
        _datePicker.datePickerMode = UIDatePickerModeDate;
    } else {
        _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
    _segmentedView.selectedSegmentIndex = dateOrTime ? 1 : 0;
}

- (void)setSelectDate:(NSDate *)selectDate {
    _selectDate = selectDate;
    _datePicker.date = selectDate;
}

- (void)handleSegmentAction:(UISegmentedControl *)segmentedControl {
    _dateOrTime = segmentedControl.selectedSegmentIndex == 1;
    if (_dateOrTime) {
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    } else {
        self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
}

@end
