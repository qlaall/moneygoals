//
//  MGReceiptDateModel.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/29.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptDateModel.h"

@implementation MGReceiptDateModel

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.dataModels = [[NSMutableArray alloc] init];
    self.paidMoney = [[NSDecimalNumber alloc] initWithString:@"0"];
    self.incomeMoney = [[NSDecimalNumber alloc] initWithString:@"0"];
    
    return self;
}

- (NSDictionary *)getMoneyValuesWithRecordModel:(MGRecordModel *)recordModel accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId {
    if (self.paidMoney.floatValue == 0 && self.incomeMoney.floatValue == 0) {
        NSArray *receiptModels = [recordModel getReceiptModelsWithBeginDate:self.startOfDay endDate:[self.startOfDay endOfDay] categoryUniqueId:categoryUniqueId accountUniqueId:accountUniqueId];
        NSDecimalNumber *paidMoney = [[NSDecimalNumber alloc] initWithString:@"0"];
        NSDecimalNumber *incomeMoney = [[NSDecimalNumber alloc] initWithString:@"0"];
        
        if (receiptModels.count > 0) {
            // 只舍不如
            NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundUp scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES];
            
            for (NSInteger index = 0; index < receiptModels.count; index ++) {
                MGReceiptModel *dataModel = receiptModels[index];
                if (dataModel.receiptType == MGReceiptTypePaid) {
                    paidMoney = [paidMoney decimalNumberByAdding:dataModel.savedMoney withBehavior:roundUp];
                } else if (dataModel.receiptType == MGReceiptTypeIncome) {
                    incomeMoney = [incomeMoney decimalNumberByAdding:dataModel.savedMoney withBehavior:roundUp];
                }
            }
        }
        
        self.paidMoney = paidMoney;
        self.incomeMoney = incomeMoney;
    }
    
    return @{ @"incomeMoney": self.incomeMoney, @"paidMoney": self.paidMoney };
}

@end
