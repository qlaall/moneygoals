//
//  MGCreateDateCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/11.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCreateDateCell.h"

@implementation MGCreateDateCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createAtSuperView:self.containerView iconName:@"ico_common_date" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(45);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
        view.text = @"记账开始时间";
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-20);
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithDate:(NSDate *)date {
    NSString *dateStr = [date toStringWithFormatter:@"yyyy年M月d日"];
    self.secondStringLabel.text = dateStr;
}

@end
