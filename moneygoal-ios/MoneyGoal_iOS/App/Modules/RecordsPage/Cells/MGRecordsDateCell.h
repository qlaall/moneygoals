//
//  MGRecordsDateCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsDateCell : YYBTableViewCell

- (void)configViewWithDataModel:(MGReceiptDateModel *)dataModel incomeMoney:(NSDecimalNumber *)incomeMoney paidMoney:(NSDecimalNumber *)paidMoney;

@end

NS_ASSUME_NONNULL_END
