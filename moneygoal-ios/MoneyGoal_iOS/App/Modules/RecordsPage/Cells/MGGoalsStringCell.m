//
//  MGRecordsStringCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsStringCell.h"

@implementation MGRecordsStringCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.firstWrapperView = [UIView createViewAtSuperView:self.contentView backgroundColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 15, 0, 15));
    } configureBlock:^(UIView *view) {
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 8;
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.firstWrapperView fontValue:YYBFONT_NORMAL(14) textColor:TEXT_COLOR_THIRD constraintBlock:^(MASConstraintMaker *make) {
        make.center.equalTo(self.firstWrapperView);
//        make.bottom.equalTo(self.firstWrapperView).offset(-15);
    } configureBlock:nil];
    
    return self;
}

@end
