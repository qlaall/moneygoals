//
//  MGRecordsTypeCell.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsTypeCell.h"

@implementation MGRecordsTypeCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.wrapperView = [UIView createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } configureBlock:^(UIView *view) {
        [view cornerRadius:4 width:1 color:APP_COLOR_SAPERATE];
    }];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.wrapperView).offset(15);
        make.centerY.equalTo(self.wrapperView);
        make.size.mas_equalTo(CGSizeMake(40, 55));
    } configureBlock:nil];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(10);
        make.top.equalTo(self.firstIconView);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(5);
        make.right.equalTo(self.wrapperView).offset(-15);
    } configureBlock:^(UILabel *view) {
        view.numberOfLines = 0;
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue iconName:(NSString *)iconName {
    self.firstStringLabel.text = titleValue;
    self.secondStringLabel.attributedText = [NSMutableAttributedString createLineSpacingString:detailValue spacingValue:2 alignment:NSTextAlignmentLeft];
    self.firstIconView.image = [UIImage imageNamed:iconName];
}

@end
