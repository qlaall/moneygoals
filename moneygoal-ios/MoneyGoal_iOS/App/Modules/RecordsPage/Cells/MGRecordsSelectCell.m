//
//  MGRecordsSelectCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsSelectCell.h"

@implementation MGRecordsSelectCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.wrapperView = [UIView createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 10, 0, 10));
    } configureBlock:^(UIView *view) {
        view.backgroundColor = [UIColor whiteColor];
        [view cornerRadius:6];
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.wrapperView).offset(15);
        make.bottom.equalTo(self.wrapperView.mas_centerY).offset(-1);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_THIRD constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.wrapperView.mas_centerY).offset(1);
    } configureBlock:nil];
    
    self.firstIconView = [UIImageView createAtSuperView:self.wrapperView iconName:@"record_next" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel.mas_right).offset(2);
        make.centerY.equalTo(self.firstStringLabel);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.wrapperView);
        make.centerY.equalTo(self.wrapperView);
        make.size.mas_equalTo(CGSizeMake(55, 65));
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"grid_select_normal"] forState:0];
        [view setBackgroundImage:[UIImage imageNamed:@"grid_select_select"] forState:UIControlStateSelected];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    return self;
}

- (void)configViewWithDataModel:(MGRecordModel *)dataModel status:(BOOL)status {
    self.firstStringLabel.text = dataModel.name;
    self.firstActionButton.selected = status;
    
    if (dataModel.recordType == MGRecordType365Money) {
        self.secondStringLabel.text = @"365存钱账本";
    } else {
        self.secondStringLabel.text = @"普通账本";
    }
}

@end
