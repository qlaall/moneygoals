//
//  MGRecordsPreviousDayCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsPreviousDayCell.h"

@implementation MGRecordsPreviousDayCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:YYBFONT_NORMAL(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.top.equalTo(self.contentView).offset(15);
        make.height.mas_equalTo(17);
    } configureBlock:nil];
    
    return self;
}

- (void)viewBeginConfigData {
    self.firstStringLabel.text = @"往期记录";
}

@end
