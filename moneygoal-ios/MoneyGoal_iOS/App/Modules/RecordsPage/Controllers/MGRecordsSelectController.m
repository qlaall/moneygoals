//
//  MGRecordsSelectController.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsSelectController.h"
#import "MGRecordController.h"
#import "MGLoginController.h"

#import "MGRecordsSelectLayout.h"
#import "MGBaseActionsView.h"

#import "MGRecordsSelectCell.h"

@interface MGRecordsSelectController ()
@property (nonatomic, strong) MGRecordsSelectLayout *layout;
@property (nonatomic, strong) MGBaseActionsView *actionsView;

@end

@implementation MGRecordsSelectController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = APP_COLOR_BACKGROUND;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.layout = [[MGRecordsSelectLayout alloc] init];
    self.layout.dataModels = MGRecordAccessor.shared.dataModels;
    
    @weakify(self);
    self.layout.updateBlock = ^{
        [MGRecordAccessor.shared saveToLocalWithDataModels:MGRecordAccessor.shared.dataModels completeBlock:nil];
    };
    self.collectionView.collectionViewLayout = self.layout;
    
    // 底部的操作按钮
    self.actionsView = [MGBaseActionsView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.mas_equalTo(65 + [UIDevice safeAreaInsetsBottom]);
    } configureBlock:^(MGBaseActionsView *view) {
        view.createBlock = ^{
            @strongify(self);
            [self createRecordModel];
        };
    }];
    
    [self.actionsView changeCreate];
}

- (void)beginQueryData {
    [super beginQueryData];
    [self refreshData];
}

- (void)refreshData {
    if (self.refreshAlertView) {
        [self.refreshAlertView closeAlertView];
    }
    
    if (MGRecordAccessor.shared.dataModels.count == 0) {
        UIEdgeInsets insets = UIEdgeInsetsMake([self customPageHeaderHeight], 0, 65 + [UIDevice safeAreaInsetsBottom], 0);
        self.refreshAlertView = [MGAlertView showOnlyTextAlertViewInView:self.view insets:insets title:@"请先添加账本"];
    }

    [self.collectionView reloadData];
}

- (void)createRecordModel {
    @weakify(self);
    if ([MGUserModel isUserLogin]) {
        [MGAlertView showRecordsTypeAlertViewWithTapedBlock:^(MGRecordType type) {
            @strongify(self);
            [self handleCreateRecordWithType:type];
        }];
    } else {
        [YYBViewRouter openOnClass:[MGLoginController class]];
    }
}

- (void)handleCreateRecordWithType:(MGRecordType)recordType {
    @weakify(self);
    [YYBViewRouter openOnClass:[MGRecordController class] configureBlock:^(MGRecordController *obj) {
        obj.recordType = recordType;
        obj.createRecordModelBlock = ^{
            @strongify(self);
            [self refreshData];
            self.updateRecordsBlock();
        };
    }];
}

// 更新账本信息
- (void)handleUpdateRecordModel:(MGRecordModel *)recordModel {
    @weakify(self);
    [YYBViewRouter openOnClass:[MGRecordController class] configureBlock:^(MGRecordController *obj) {
        obj.updateModel = recordModel;
        obj.updateRecordModelBlock = ^{
            @strongify(self);
            [self refreshData];
            self.updateRecordsBlock();
        };
        obj.deleteRecordModelBlock = ^{
            @strongify(self);
            [self refreshData];
            self.updateRecordsBlock();
        };
        obj.createRecordModelBlock = ^{
            @strongify(self);
            [self refreshData];
            self.updateRecordsBlock();
        };
    }];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGRecordsSelectCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return MGRecordAccessor.shared.dataModels.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MGRecordsSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGRecordsSelectCell" forIndexPath:indexPath];
    
    MGRecordModel *dataModel = MGRecordAccessor.shared.dataModels[indexPath.row];
    @weakify(self);
    cell.tapedActionBlock = ^{
        @strongify(self);
        if (dataModel.uniqueId != MGRecordAccessor.shared.getSelectedRecordModel.uniqueId) {
            [MGRecordAccessor.shared selectRecordModel:dataModel];
            [self.collectionView reloadData];
            [YYBAlertView showAlertViewWithStatusString:@"切换成功"];
            if (self.changeRecordBlock) {
                self.changeRecordBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    };
    
    BOOL isSelect = dataModel.uniqueId == MGRecordAccessor.shared.getSelectedRecordModel.uniqueId;
    [cell configViewWithDataModel:dataModel status:isSelect];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MGRecordModel *dataModel = MGRecordAccessor.shared.dataModels[indexPath.row];
    [self handleUpdateRecordModel:dataModel];
}

- (NSString *)customPageTitle {
    return @"账本列表";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.top += 10;
    return insets;
}

@end
