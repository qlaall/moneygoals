//
//  MG365RecordsView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/10.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MG365RecordsView : YYBBaseView

@property (nonatomic, copy) void (^ handleReceiptBlock)(MGReceiptModel *receiptModel);

@end

NS_ASSUME_NONNULL_END
