//
//  MGRecordsTypeView.h
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsTypeView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionBlock closeTapedBlock;
@property (nonatomic, copy) void (^ selectRecordTypeBlock)(MGRecordType type);

@end

NS_ASSUME_NONNULL_END
