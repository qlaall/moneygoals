//
//  MG365RecordsView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/10.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MG365RecordsView.h"
#import "MG365RecordsGridView.h"
#import "MG365RecordsHeaderView.h"

@interface MG365RecordsView () <UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *contentView;
@property (nonatomic, strong) MG365RecordsGridView *heartGridView;
@property (nonatomic, strong) MG365RecordsHeaderView *headerView;

@end

@implementation MG365RecordsView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.backgroundColor = APP_COLOR_MAIN_LIGHT;
    
    self.contentView = [[UIScrollView alloc] init];
    self.contentView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.contentView.delegate = self;
    self.contentView.contentSize = frame.size;
    
    self.contentView.minimumZoomScale = 1;
    self.contentView.maximumZoomScale = 3;
    [self addSubview:self.contentView];
    
    self.heartGridView = [[MG365RecordsGridView alloc] init];
    self.heartGridView.frame = CGRectMake(0, 220, frame.size.width, frame.size.height - 220);
    
    @weakify(self);
    self.heartGridView.updateMoneyBlock = ^{
        @strongify(self);
        
    };
    self.heartGridView.handleReceiptBlock = ^(MGReceiptModel * _Nonnull receiptModel) {
        @strongify(self);
        if (self.handleReceiptBlock) {
            self.handleReceiptBlock(receiptModel);
        }
    };
    [self.contentView addSubview:self.heartGridView];
    
    self.headerView = [[MG365RecordsHeaderView alloc] init];
    self.headerView.frame = CGRectMake(0, 0, frame.size.width, 200);
    [self addSubview:self.headerView];
    
    return self;
}

- (void)configViewWithDataModel:(MGRecordModel *)dataModel
{
    self.heartGridView.dataModel = dataModel;
    [self.headerView configViewWithDataModel:dataModel];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.heartGridView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    NSLog(@"%@", NSStringFromCGPoint(scrollView.contentOffset));
}

@end
