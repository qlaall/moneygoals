//
//  MG365RecordsHeaderView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/11.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MG365RecordsHeaderView : YYBBaseView

@property (nonatomic, strong) MGRecordModel *dataModel;

@end

NS_ASSUME_NONNULL_END
