//
//  MGRecordsHeaderView.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/20.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsHeaderView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionBlock editBlock;

@property (nonatomic, strong) MGRecordModel *recordModel;
@property (nonatomic) NSUInteger accountUniqueId;
@property (nonatomic) NSUInteger categoryUniqueId;

- (void)configViewWithRecordModel:(MGRecordModel *)recordModel accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;

@property (nonatomic, copy) YYBTapedActionBlock accountTapedBlock, categoryTapedBlock;

@end

NS_ASSUME_NONNULL_END
