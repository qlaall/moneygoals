//
//  MG365RecordsHeaderView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/11.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MG365RecordsHeaderView.h"
#import <YYBBaseViews/YYBShadowView.h>

@interface MG365RecordsHeaderView ()
@property (nonatomic, strong) YYBShadowView *shadowView;
@property (nonatomic, strong) UIView *scheduleView;
@property (nonatomic, strong) UIView *endMoneyView, *currMoneyView;
@property (nonatomic, strong) UILabel *endMoneyLabel, *currMoneyLabel;

@end

@implementation MG365RecordsHeaderView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.shadowView = [YYBShadowView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(15, 15, 15, 15));
    } configureBlock:^(YYBShadowView *view) {
        [view.shadowView setLayerShadow:[YYBCOLOR(0xDBDBDB) colorWithAlphaComponent:0.5] offset:CGSizeMake(0, 2) radius:4];
        [view.contentView cornerRadius:12];
    }];
    
    self.wrapperView = [UIView createViewAtSuperView:self.shadowView.contentView cornerRadius:12 backgroundColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.shadowView.contentView);
    } configureBlock:^(UIView *view) {
        
    }];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.wrapperView).offset(15);
        make.top.equalTo(self.wrapperView).offset(15);
    } configureBlock:^(UILabel *view) {
        view.font = [UIFont systemFontOfSize:10];
        view.numberOfLines = 0;
        view.textAlignment = NSTextAlignmentLeft;
    }];
    
    self.secondStringLabel = [UILabel createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.wrapperView).offset(-15);
        make.top.equalTo(self.wrapperView).offset(15);
    } configureBlock:^(UILabel *view) {
        view.font = [UIFont systemFontOfSize:10];
        view.numberOfLines = 0;
        view.textAlignment = NSTextAlignmentRight;
    }];
    
    self.scheduleView = [UIView createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.wrapperView).offset(-32);
        make.left.equalTo(self.wrapperView).offset(15);
        make.right.equalTo(self.wrapperView).offset(-15);
        make.height.mas_equalTo(4);
    } configureBlock:^(UIView *view) {
        [view cornerRadius:2];
    }];
    
    self.endMoneyView = [UIView createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.scheduleView);
        make.size.mas_equalTo(CGSizeMake(12, 12));
        make.centerY.equalTo(self.scheduleView);
    } configureBlock:^(UIView *view) {
        view.layer.cornerRadius = 6;
        view.layer.borderWidth = 3;
        view.layer.masksToBounds = YES;
        view.backgroundColor = [UIColor whiteColor];
    }];
    
    self.endMoneyLabel = [UILabel createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.scheduleView);
        make.bottom.equalTo(self.wrapperView).offset(-10);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(10);
    }];
    
    self.currMoneyView = [UIView createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(12, 12));
        make.centerY.equalTo(self.scheduleView);
        make.left.equalTo(self.scheduleView);
    } configureBlock:^(UIView *view) {
        view.layer.cornerRadius = 6;
        view.layer.borderWidth = 3;
        view.layer.masksToBounds = YES;
        view.backgroundColor = [UIColor whiteColor];
    }];
    
    self.currMoneyLabel = [UILabel createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.currMoneyView).offset(10);
        make.bottom.equalTo(self.currMoneyView.mas_top).offset(-2);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(10);
        view.text = @"0.00";
    }];
    
    return self;
}

- (void)configViewWithDataModel:(MGRecordModel *)dataModel
{
    self.dataModel = dataModel;
    
    self.scheduleView.backgroundColor = APP_COLOR_MAIN_LIGHT;
    self.endMoneyView.layer.borderColor = APP_COLOR_MAIN_LIGHT.CGColor;
    self.currMoneyView.layer.borderColor = APP_COLOR_MAIN_LIGHT.CGColor;
    
    CGFloat shouldSaveMoney = [dataModel getShouldSaveMoney];
    self.endMoneyLabel.text = [NSString stringWithFormat:@"目标 %.2f", shouldSaveMoney];
    
    // 现在存的钱
    CGFloat currMoney = [dataModel getStoredMoney];
    if (currMoney >= 0 && shouldSaveMoney > 0) {
        CGFloat transx = currMoney / shouldSaveMoney * (kScreenWidth - 30 - 30 - 12);
        [self.currMoneyView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.scheduleView).offset(transx);
        }];
        
        CGFloat rateValue = currMoney / shouldSaveMoney * 100;
        self.currMoneyLabel.text = [NSString stringWithFormat:@"%.1f (%.1f%%)", currMoney, rateValue];
    }
    
    // 设置开始时间
    if (dataModel.startDate) {
        NSString *strBegin = [dataModel.startDate toStringWithFormatter:@"开始\nYYYY年M月d日"];
        NSMutableAttributedString *attrBegin = [[NSMutableAttributedString alloc] initWithString:strBegin];
        [attrBegin addAttribute:NSFontAttributeName value:FONT_MEDIUM(18) range:NSMakeRange(0, 2)];
        [attrBegin addAttribute:NSFontAttributeName value:FONT_REGULAR(12) range:NSMakeRange(3, strBegin.length - 3)];
        
        NSMutableParagraphStyle *para = [[NSMutableParagraphStyle alloc] init];
        para.lineSpacing = 5;
        [attrBegin addAttribute:NSParagraphStyleAttributeName value:para range:NSMakeRange(0, strBegin.length)];
        
        self.firstStringLabel.attributedText = attrBegin;
        
        // 设置结束时间
        NSString *strEnd = [[dataModel.startDate dateByAddingDays:364] toStringWithFormatter:@"结束\nYYYY年M月d日"];
        NSMutableAttributedString *attrEnd = [[NSMutableAttributedString alloc] initWithString:strEnd];
        [attrEnd addAttribute:NSFontAttributeName value:FONT_MEDIUM(18) range:NSMakeRange(0, 2)];
        [attrEnd addAttribute:NSFontAttributeName value:FONT_REGULAR(12) range:NSMakeRange(3, strEnd.length - 3)];
        
        NSMutableParagraphStyle *paraEnd = [[NSMutableParagraphStyle alloc] init];
        paraEnd.alignment = NSTextAlignmentRight;
        paraEnd.lineSpacing = 5;
        [attrEnd addAttribute:NSParagraphStyleAttributeName value:paraEnd range:NSMakeRange(0, strEnd.length)];
        
        self.secondStringLabel.attributedText = attrEnd;
    }
}


@end
