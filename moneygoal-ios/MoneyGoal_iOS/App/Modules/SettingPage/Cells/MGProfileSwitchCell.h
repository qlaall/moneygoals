//
//  MGProfileSwitchCell.h
//  Decorate-iOS
//
//  Created by yyb on 2022/8/14.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGProfileSwitchCell : MGBaseCornerCell

@property (nonatomic, copy) void (^ changeValueBlock)(BOOL status);

@end

NS_ASSUME_NONNULL_END
