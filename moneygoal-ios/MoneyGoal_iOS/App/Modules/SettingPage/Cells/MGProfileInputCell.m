//
//  MGProfileInputCell.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/12.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGProfileInputCell.h"

@implementation MGProfileInputCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    self.firstIconView = [UIImageView createAtSuperView:self.containerView iconName:@"record_next" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-10);
        make.centerY.equalTo(self.containerView);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-35);
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue {
    self.firstStringLabel.text = titleValue;
    self.secondStringLabel.text = detailValue;
}

@end
