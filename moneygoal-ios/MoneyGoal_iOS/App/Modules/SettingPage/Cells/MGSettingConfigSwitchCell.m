//
//  MGSettingConfigSwitchCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGSettingConfigSwitchCell.h"

@interface MGSettingConfigSwitchCell ()
@property (nonatomic, strong) UISwitch *switchView;

@end

@implementation MGSettingConfigSwitchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.top.equalTo(self.containerView).offset(10);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(5);
        make.right.equalTo(self.containerView.mas_centerX).offset(100);
    } configureBlock:^(UILabel *view) {
        view.numberOfLines = 0;
    }];
    
    self.switchView = [UISwitch createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.containerView);
        make.right.equalTo(self.containerView).offset(-15);
    } configureBlock:^(UISwitch *view) {
        
    }];
    
    @weakify(self);
    [[self.switchView rac_signalForControlEvents:UIControlEventValueChanged] subscribeNext:^(__kindof UISwitch * _Nullable x) {
        @strongify(self);
        if (self.changeValueBlock) {
            self.changeValueBlock(x.on);
        }
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(nonnull NSString *)detailValue status:(BOOL)status
{
    self.firstStringLabel.text = titleValue;
    self.secondStringLabel.attributedText = [NSMutableAttributedString createLineSpacingString:detailValue spacingValue:2 alignment:NSTextAlignmentLeft];
    self.switchView.on = status;
}

@end
