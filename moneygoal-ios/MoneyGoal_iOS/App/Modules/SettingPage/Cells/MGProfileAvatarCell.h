//
//  MGProfileAvatarCell.h
//  Decorate-iOS
//
//  Created by yyb on 2022/8/12.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGProfileAvatarCell : MGBaseCornerCell

@end

NS_ASSUME_NONNULL_END
