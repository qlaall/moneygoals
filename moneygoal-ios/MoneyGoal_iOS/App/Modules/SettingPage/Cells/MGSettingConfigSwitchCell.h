//
//  MGSettingConfigSwitchCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingConfigSwitchCell : MGBaseCornerCell

@property (nonatomic, copy) void (^ changeValueBlock)(BOOL status);
- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(nonnull NSString *)detailValue status:(BOOL)status;

@end

NS_ASSUME_NONNULL_END
