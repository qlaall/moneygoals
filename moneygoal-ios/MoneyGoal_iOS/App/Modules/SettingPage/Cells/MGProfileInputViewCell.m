//
//  MGProfileInputViewCell.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/12.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGProfileInputViewCell.h"

@implementation MGProfileInputViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.inputStringView = [UITextField createAtSuperView:self.containerView leftViewOffset:0 cornerRadius:0 fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 10));
    } configureBlock:^(UITextField *view) {
        view.clearButtonMode = UITextFieldViewModeWhileEditing;
        view.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 0)];
        view.leftViewMode = UITextFieldViewModeAlways;
        view.clearButtonMode = UITextFieldViewModeAlways;
    }];
    
    @weakify(self);
    [self.inputStringView.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        if (self.stringChangedBlock) {
            self.stringChangedBlock(x);
        }
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(nonnull NSString *)detailValue
{
    self.inputStringView.placeholder = [NSString stringWithFormat:@"输入%@", titleValue];
    self.inputStringView.text = detailValue;
}

@end
