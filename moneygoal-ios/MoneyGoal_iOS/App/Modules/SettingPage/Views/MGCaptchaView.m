//
//  MGCaptchaView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/2.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCaptchaView.h"

@implementation MGCaptchaView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_MEDIUM(25) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.top.equalTo(self).offset(30);
    } configureBlock:^(UILabel *view) {
        view.text = @"校验验证码";
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(10);
    } configureBlock:^(UILabel *view) {
        view.text = @"手机号如未注册，首次登录就会自动注册";
    }];
    
    self.inputStringView = [UITextField createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.right.equalTo(self).offset(-30);
        make.top.equalTo(self).offset(180);
        make.height.mas_equalTo(50);
    } configureBlock:^(UITextField *view) {
        view.placeholder = @"输入验证码";
        view.font = FONT_REGULAR(15);
        view.keyboardType = UIKeyboardTypeNumberPad;
    }];
    
    self.firstLineView = [UIView createViewAtSuperView:self backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.inputStringView);
        make.height.mas_equalTo(1);
        make.top.equalTo(self.inputStringView.mas_bottom);
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self cornerRadius:25 fontValue:FONT_MEDIUM(16) title:@"立即登录" titleColor:APP_COLOR_MAIN constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.inputStringView);
        make.top.equalTo(self.inputStringView.mas_bottom).offset(40);
        make.height.mas_equalTo(50);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:APP_COLOR_MAIN_LIGHT.colorToUIImage forState:0];
        [view setBackgroundImage:APP_COLOR_MAIN_LIGHT.colorToUIImage forState:UIControlStateHighlighted];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.handleLoginBlock) {
            if (self.inputStringView.text.length == 0) {
                [YYBAlertView showAlertViewWithStatusString:@"请输入验证码"];
            } else {
                self.handleLoginBlock(self.inputStringView.text);
            }
        }
    }];
    
    return self;
}

@end
