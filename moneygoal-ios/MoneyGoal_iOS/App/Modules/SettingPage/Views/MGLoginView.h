//
//  MGLoginView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/31.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGLoginView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionIndexBlock platformSelectBlock;
@property (nonatomic, copy) YYBStringChangedBlock getCaptchaBlock;

@end

NS_ASSUME_NONNULL_END
