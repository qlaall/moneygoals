//
//  MGNotLoginView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"
#import <AuthenticationServices/AuthenticationServices.h>
//#import "WXApi.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGNotLoginView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionBlock doLoginBlock;

@end

NS_ASSUME_NONNULL_END
