//
//  MGSettingUserView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGSettingUserView.h"
#import <SDWebImage/UIButton+WebCache.h>

@implementation MGSettingUserView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.wrapperView = [UIView createViewAtSuperView:self backgroundColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 10, 0, 10));
    } configureBlock:^(UIView *view) {
        [view cornerRadius:6];
    }];
    
    @weakify(self);
    [self.wrapperView whenTapped:^{
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    self.firstActionButton = [UIButton createAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.wrapperView);
        make.top.equalTo(self.wrapperView).offset(20);
        make.size.mas_equalTo(CGSizeMake(70, 70));
    } configureBlock:^(UIButton *view) {
        view.imageView.contentMode = UIViewContentModeScaleAspectFill;
        view.userInteractionEnabled = NO;
        [view cornerRadius:35];
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(16) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.wrapperView);
        make.top.equalTo(self.firstActionButton.mas_bottom).offset(10);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithDataModel:(MGUserModel *)dataModel
{
    NSURL *avatarURL = [NSURL URLWithString:[MGReqManager.shared avatarURL:dataModel.avatar]];
    
    [self.firstActionButton sd_setImageWithURL:avatarURL forState:0 placeholderImage:[UIImage imageNamed:@"logo_mini"]];
    self.firstStringLabel.text = dataModel.nick_name;
}

@end
