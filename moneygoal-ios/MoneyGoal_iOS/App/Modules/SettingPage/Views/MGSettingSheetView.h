//
//  MGSettingSheetView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingSheetView : YYBBaseView

@end

NS_ASSUME_NONNULL_END
