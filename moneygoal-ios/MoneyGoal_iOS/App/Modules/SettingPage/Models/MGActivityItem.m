//
//  MGActivityItem.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/3.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGActivityItem.h"
#import <LinkPresentation/LinkPresentation.h>

@implementation MGActivityItem

+ (instancetype)activityItemSourceUrlWithIcon:(UIImage *)icon {
    MGActivityItem *shareItem = [[self alloc] initWithPlaceholderItem:@""];
    shareItem.icon = icon;
    return shareItem;
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController {
    return self.icon;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType {
    return self.icon;
}

- (UIImage *)activityViewController:(UIActivityViewController *)activityViewController thumbnailImageForActivityType:(NSString *)activityType suggestedSize:(CGSize)size {
    return self.icon;
}

#if defined(__IPHONE_13_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_13_

- (nullable LPLinkMetadata *)activityViewControllerLinkMetadata:(UIActivityViewController *)activityViewController {
    LPLinkMetadata *data = [[LPLinkMetadata alloc] init];
    data.iconProvider = [[NSItemProvider alloc] initWithObject:self.icon];
    data.title = [NSBundle mainBundle].infoDictionary[@"CFBundleDisplayName"];
    
    NSString *appPath = @"https://itunes.apple.com/us/app/fa-bu-ce-shi/id1491424390?l=zh&ls=1&mt=8";
//    data.URL = [NSURL URLWithString:appPath];
    data.originalURL = [NSURL URLWithString:appPath];
    return data;
}

#endif

@end
