//
//  MGUserModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGUserModel : NSObject

@property (nonatomic, copy) NSString *username, *phone, *password;

@property (nonatomic, copy) NSString *uid, *apple_id, *wx_openid;
@property (nonatomic, copy) NSString *avatar, *nick_name, *access_token, *refresh_token, *province, *city;
@property (nonatomic, copy) NSString *gender; // sex转换
@property (nonatomic) NSInteger birthday;
@property (nonatomic) NSInteger sex; // 1.男 2.女 3.未知

@property (nonatomic, copy) NSString *repassword;
@property (nonatomic, copy) NSString *phone_code;

@end

NS_ASSUME_NONNULL_END
