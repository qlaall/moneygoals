//
//  MGCaptchaController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/2.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCaptchaController.h"
#import "MGCaptchaView.h"

@interface MGCaptchaController ()
@property (nonatomic, strong) MGCaptchaView *captchaView;

@end

@implementation MGCaptchaController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.captchaView = [MGCaptchaView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset([self customPageHeaderHeight]);
    } configureBlock:nil];
    
    @weakify(self);
    self.captchaView.handleLoginBlock = ^(NSString *text) {
        @strongify(self);
        [self handleLoginWithCaptcha:text];
    };
}

- (void)handleLoginWithCaptcha:(NSString *)captcha {
    self.refreshAlertView = [MGAlertView showQueryAlertView];
    
    @weakify(self);
    [MGReqManager.shared handleLoginWithPhoneNum:self.phoneNum captcha:captcha successBlock:^(id data, NSDictionary *params) {
        @strongify(self);
        [self.refreshAlertView closeAlertView];
        [YYBAlertView showAlertViewWithStatusString:@"登录成功"];
        [MGUserModel configUserInfo:data];
        [self.navigationController popToRootViewControllerAnimated:YES];
    } errorBlock:^(NSError *error, NSDictionary *params) {
        @strongify(self);
        [YYBAlertView showAlertViewWithError:error dismissAlertView:self.refreshAlertView];
    }];
}

@end
