//
//  MGSettingConfigController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingConfigController : MGTableController

@end

NS_ASSUME_NONNULL_END
