//
//  MGSettingController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/28.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingController : MGTableController

@end

NS_ASSUME_NONNULL_END
