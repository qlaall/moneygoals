//
//  MGSettingDataController.h
//  Decorate-iOS
//
//  Created by yyb on 2022/8/14.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingDataController : MGTableController

@end

NS_ASSUME_NONNULL_END
