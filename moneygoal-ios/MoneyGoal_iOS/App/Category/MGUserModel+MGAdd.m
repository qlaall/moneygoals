//
//  MGUserModel+DBAdd.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/5.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGUserModel+MGAdd.h"

NSString * const YYBUserLoginNotifictaion = @"YYBUserLoginNotifictaionKey";
NSString * const YYBUserLogoutNotifictaion = @"YYBUserLogoutNotifictaionKey";
NSString * const YYBUserUpdateNotifictaion = @"YYBUserUpdateNotifictaionKey";
NSString * const YYBUserBeforeLogoutNotifictaion = @"YYBUserBeforeLogoutNotifictaion";

@implementation MGUserModel (DBAdd)

+ (BOOL)isUserLogin {
    MGUserModel *user = MGConfigManager.shared.dataModel.userModel;
    return user && [user.uid isExist];
}

+ (void)configUserInfo:(NSDictionary *)userInfo {
    MGUserModel *user = [MGUserModel yy_modelWithDictionary:userInfo];
    [MGConfigManager.shared handleChangeUser:user];
    [[NSNotificationCenter defaultCenter] postNotificationName:YYBUserLoginNotifictaion object:nil];
}

+ (MGUserModel *)user {
    return MGConfigManager.shared.dataModel.userModel;
}

+ (void)doUserLogout {
    [MGConfigManager.shared handleChangeUser:nil];
    // 退出后，需要清空所有的数据，并且初始化账本
    [MGRecordAccessor.shared beginInitialize];
    [[NSNotificationCenter defaultCenter] postNotificationName:YYBUserLogoutNotifictaion object:nil];
}

+ (void)updateUserData:(NSDictionary *)data {
    if (data) {
        MGUserModel *user = [MGUserModel yy_modelWithJSON:data];
        [MGConfigManager.shared handleChangeUser:user];
        [[NSNotificationCenter defaultCenter] postNotificationName:YYBUserUpdateNotifictaion object:nil];
    }
}

+ (void)updateUserAvatar:(NSString *)avatar {
    MGUserModel *user = [MGUserModel user];
    user.avatar = avatar;
    [MGConfigManager.shared handleChangeUser:user];
    [[NSNotificationCenter defaultCenter] postNotificationName:YYBUserUpdateNotifictaion object:nil];
}

+ (void)updateUserNickName:(NSString *)nickName {
    MGUserModel *user = [MGUserModel user];
    user.nick_name = nickName;
    [MGConfigManager.shared handleChangeUser:user];
    [[NSNotificationCenter defaultCenter] postNotificationName:YYBUserUpdateNotifictaion object:nil];
}

@end
