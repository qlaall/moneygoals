//
//  MGUserModel+DBAdd.h
//  Decorate-iOS
//
//  Created by yyb on 2022/8/5.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGUserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGUserModel (MGAdd)

extern NSString * const YYBUserLoginNotifictaion;
extern NSString * const YYBUserBeforeLogoutNotifictaion;
extern NSString * const YYBUserLogoutNotifictaion;
extern NSString * const YYBUserUpdateNotifictaion;

+ (MGUserModel *)user;

+ (BOOL)isUserLogin;
+ (void)doUserLogout;
+ (void)updateUserData:(NSDictionary *)data;

+ (void)configUserInfo:(NSDictionary *)userInfo;
+ (void)updateUserAvatar:(NSString *)avatar;
+ (void)updateUserNickName:(NSString *)nickName;

@end

NS_ASSUME_NONNULL_END
