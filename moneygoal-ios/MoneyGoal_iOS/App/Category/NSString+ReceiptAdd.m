//
//  NSString+ReceiptAdd.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/28.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "NSString+ReceiptAdd.h"

@implementation NSString (ReceiptAdd)

- (CGFloat)calculateData
{
    // 查找第一个运算符号
    // 没有运算符号
    NSInteger firstSymbolIndex = [self nextSymbolIndex];
    if (firstSymbolIndex == 0) {
        return self.floatValue;
    }
    
    CGFloat result = [self substringToIndex:firstSymbolIndex].floatValue;
    NSString *sub = [self substringFromIndex:firstSymbolIndex];
    do {
        NSInteger nextSymbolIndex = [sub nextSymbolIndex];
        SMCalculateSymbol symbol = [[sub substringWithRange:NSMakeRange(nextSymbolIndex, 1)] symbol];
        
        if (sub.length == nextSymbolIndex + 1) { // 最后一个字符是运算符号
            sub = @"";
        } else {
            NSString *sub2 = [sub substringFromIndex:nextSymbolIndex + 1];
            if ([sub2 nextSymbolIndex] == 0) { // 下一截字符串没有运算符号
                if (symbol == SMCalculateSymbolPlus) {
                    result = result + sub2.floatValue;
                } else if (symbol == SMCalculateSymbolMinus) {
                    result = result - sub2.floatValue;
                }
                sub = @"";
            } else { // 下一截字符串有运算符号
                NSInteger nextSymbolIndex2 = [sub2 nextSymbolIndex];
                CGFloat nextNumber = [sub2 substringToIndex:nextSymbolIndex2].floatValue;
                
                if (symbol == SMCalculateSymbolPlus) {
                    result = result + nextNumber;
                } else if (symbol == SMCalculateSymbolMinus) {
                    result = result - nextNumber;
                }
                sub = [sub2 substringFromIndex:nextSymbolIndex2];
            }
        }
    } while ([sub containsSymbol] && sub.length > 0);
    
    return result;
}

- (NSInteger)nextSymbolIndex {
    for (NSInteger index = 0; index < self.length; index ++) {
        NSString *character = [self substringWithRange:NSMakeRange(index, 1)];
        if ([character symbol] != SMCalculateSymbolNone) {
            return index;
        }
    }
    return 0;
}

- (SMCalculateSymbol)symbol {
    if ([self isEqualToString:@"+"]) {
        return SMCalculateSymbolPlus;
    } else if ([self isEqualToString:@"-"]) {
        return SMCalculateSymbolMinus;
    }
    return SMCalculateSymbolNone;
}

- (BOOL)containsSymbol {
    NSInteger next = [self nextSymbolIndex];
    if (next != 0) {
        return YES;
    }
    if (self.length == 0) {
        return NO;
    }
    return [[self substringToIndex:1] symbol] != SMCalculateSymbolNone;
}

- (BOOL)isAllDesit {
    unichar c;
    for (int i = 0; i < self.length; i++) {
        c = [self characterAtIndex:i];
        if (!isdigit(c)) {
            return NO;
        }
    }
    return YES;
}

+ (NSString *)_handleCalculateWithSting:(NSString *)string calculateString:(nonnull NSString *)calculateString {
    if ([string isEqualToString:@"C"]) {
        if (calculateString.length != 0) {
            calculateString = [calculateString substringToIndex:calculateString.length - 1];
            if (calculateString.length == 0) {
                return nil;
            } else {
                return calculateString;
            }
        }
    } else {
        // 第一个数字不能是符号
        if ([string containsSymbol] && calculateString == nil) {
            return nil;
        } else {
            if (!calculateString) {
                return string;
            } else {
                // 不能输入三位小数
                if (calculateString.length >= 3) {
                    NSString *thirdLastString = [calculateString substringWithRange:NSMakeRange(calculateString.length - 3, 1)];
                    if ([thirdLastString isEqualToString:@"."]) {
                        NSString *lastStrings = [calculateString substringFromIndex:calculateString.length - 2];
                        if ([lastStrings isAllDesit] && ![string containsSymbol]) {
                            return calculateString;
                        }
                    }
                }
                
                // 不能连续输入两个..
                if ([string isEqualToString:@"."]) {
                    if (calculateString.length > 0) {
                        NSString *lastString = [calculateString substringFromIndex:calculateString.length - 1];
                        if ([lastString isEqualToString:@"."]) {
                            
                        } else {
                            // 不能两个.之间没有符号
                            if ([calculateString containsString:@"."]) {
                                NSArray *subStrings = [calculateString componentsSeparatedByString:@"."];
                                NSString *lastSubString = subStrings.lastObject;
                                if (![lastSubString containsSymbol]) {
                                    return calculateString;
                                }
                            }
                            calculateString = [calculateString stringByAppendingString:string];
                        }
                    }
                } else {
                    // 如果上一个是符号，并且该字符也是符号，则直接替换
                    NSString *lastString = [calculateString substringFromIndex:calculateString.length - 1];
                    if ([lastString containsSymbol] && [string containsSymbol]) {
                        NSString *sub = [calculateString substringToIndex:calculateString.length - 1];
                        calculateString = [sub stringByAppendingString:string];
                    } else {
                        calculateString = [calculateString stringByAppendingString:string];
                    }
                }
            }
        }
    }
    return calculateString;
}

@end
