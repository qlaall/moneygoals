//
//  NSString+MoneyCalculate.h
//  Decorate-iOS
//
//  Created by alchemy on 2022/3/26.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (MoneyCalculate)

- (NSString *)formatMoneyString;

@end

NS_ASSUME_NONNULL_END
