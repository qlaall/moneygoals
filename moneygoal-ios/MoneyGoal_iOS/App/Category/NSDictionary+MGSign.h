//
//  NSDictionary+MGSign.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYBBaseViews/NSArray+YYBAdd.h>
#import <YYBBaseViews/NSString+YYBAdd.h>
#import <CommonCrypto/CommonCrypto.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (MGSign)

- (NSDictionary *)createSign;

@end

NS_ASSUME_NONNULL_END
