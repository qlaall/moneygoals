//
//  NSString+ReceiptAdd.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/28.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SMCalculateSymbol) {
    SMCalculateSymbolNone,
    SMCalculateSymbolPlus,
    SMCalculateSymbolMinus,
};

NS_ASSUME_NONNULL_BEGIN

@interface NSString (ReceiptAdd)

- (CGFloat)calculateData;
- (NSInteger)nextSymbolIndex;
- (SMCalculateSymbol)symbol;
- (BOOL)containsSymbol;

+ (NSString *)_handleCalculateWithSting:(NSString *)string calculateString:(NSString *)calculateString;

@end

NS_ASSUME_NONNULL_END
