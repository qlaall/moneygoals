//
//  NSString+MoneyCalculate.m
//  Decorate-iOS
//
//  Created by alchemy on 2022/3/26.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "NSString+MoneyCalculate.h"

@implementation NSString (MoneyCalculate)

/**
 * 金额的格式转化
 * str : 金额的字符串
 * numberStyle : 金额转换的格式
 * return  NSString : 转化后的金额格式字符串
 */
- (NSString *)formatMoneyString {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    // 注意传入参数的数据长度，可用double
    NSString *money = [formatter stringFromNumber:[NSNumber numberWithDouble:[self doubleValue]]];
    return money;
}

@end
