//
//  MGAlertView+BaseViews.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertView+BaseViews.h"
#import "MGRecordsTypeView.h"
#import "MGAccountSelectView.h"
#import "MGCategorySelectView.h"
#import "MGCategoryTypesSelectView.h"
#import "MGAccountTypesSelectView.h"
#import "MGReceiptDateView.h"

@implementation MGAlertView (BaseViews)

+ (void)showAlertView:(MGAlertView *)alertView customView:(UIView *)customView viewsMaxHeight:(CGFloat)viewsMaxHeight
{
    alertView.backgroundView.backgroundColor = [alertView backgroundViewColor];
    alertView.animationStyle = YYBAlertViewAnimationStyleBottom;
        
    __weak typeof(alertView) weakAlertView = alertView;
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        [container removeContainerContentTapedBlock];
        container.maximalWidth = kScreenWidth;
        container.maximalHeight = viewsMaxHeight + [UIDevice safeAreaInsetsBottom];
        container.backgroundColor = [UIColor whiteColor];
        
        container.rectCornerRadius = [weakAlertView commonCornerRadius];
        container.rectCorner = UIRectCornerTopLeft | UIRectCornerTopRight;

        [container addCustomView:customView configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.size = CGSizeMake(kScreenWidth, viewsMaxHeight);
        }];
        
        [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.margin = UIEdgeInsetsMake(0, 0, [UIDevice safeAreaInsetsBottom], 0);
        }];
    }];
    [alertView showAlertViewOnKeyWindow];
}

+ (MGAlertView *)showOnlyTextAlertViewInView:(UIView *)inView insets:(UIEdgeInsets)insets title:(NSString *)title
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.backgroundColor = APP_COLOR_BACKGROUND;
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalHeight = 500;
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        
        if (title && title.length > 0) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(10, 0, 0, 0);
                
                label.text = title;
                label.textColor = [weakAlertView thirdTextColor];
                label.font = FONT_REGULAR(14);
            }];
        }
    }];
    
    [inView addSubview:alertView];
    alertView.frame = CGRectMake(insets.left, insets.top,
                                 CGRectGetWidth(inView.frame) - insets.left - insets.right,
                                 CGRectGetHeight(inView.frame) - insets.top - insets.bottom);
    [alertView showAlertView];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewInView:(UIView *)inView icon:(nullable UIImage *)icon iconSize:(CGSize)size
                                    title:(nullable NSString *)title content:(nullable NSString *)content
                             marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets
                              actionTitle:(nullable NSString *)actionTitle tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.backgroundColor = APP_COLOR_BACKGROUND;
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalHeight = 500;
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        
        if (icon) {
            [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
                action.size = CGSizeMake(0, paddingInsets.top * 2);
            }];
            [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
                action.size = size;
                imageView.image = icon;
            }];
        }
        if (title && title.length > 0) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(30, 0, 0, 0);
                
                label.text = title;
                label.textColor = [weakAlertView mainTextColor];
                label.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
            }];
        }
        if (content && content.length > 0) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(10, 0, 0, 0);
                
                label.text = content;
                label.textColor = [weakAlertView thirdTextColor];
                label.font = [UIFont systemFontOfSize:14];
            }];
        }
        if (actionTitle && actionTitle.length > 0) {
            [container addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.margin = UIEdgeInsetsMake(40, 0, 0, 0);
                action.size = CGSizeMake(100, 40);
                [button setBackgroundImage:APP_COLOR_MAIN.colorToUIImage forState:0];
                [button setTitle:actionTitle forState:0];
                [button setTitleColor:[UIColor whiteColor] forState:0];
                button.titleLabel.font = YYBFONT_BOLD(16);
                [button cornerRadius:20];
            } tapedOnBlock:^{
                if (tapedActionBlock) {
                    tapedActionBlock();
                }
            }];
        }
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = CGSizeMake(0, paddingInsets.bottom * 2);
        }];
    }];
    
    [inView addSubview:alertView];
    alertView.frame = CGRectMake(marginInsets.left, marginInsets.top,
                                 CGRectGetWidth(inView.frame) - marginInsets.left - marginInsets.right,
                                 CGRectGetHeight(inView.frame) - marginInsets.top - marginInsets.bottom);
    [alertView showAlertView];
    
    return alertView;
}

+ (MGAlertView *)showEmptyMaskAlertViewInView:(UIView *)inView insets:(UIEdgeInsets)insets title:(NSString *)title content:(nullable NSString *)content tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock
{
    return [self showAlertViewInView:inView icon:nil iconSize:CGSizeZero title:title content:content marginInsets:insets paddingInsets:UIEdgeInsetsZero actionTitle:@"添加" tapedActionBlock:tapedActionBlock];
}

+ (MGAlertView *)showRecordsTypeAlertViewWithTapedBlock:(void (^)(MGRecordType))tapedBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    
    alertView.backgroundView.backgroundColor = [alertView backgroundViewColor];
    alertView.animationStyle = YYBAlertViewAnimationStyleBottom;
    
    __weak typeof(alertView) weakAlertView = alertView;
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        
        [container removeContainerContentTapedBlock];
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        container.maximalHeight = 600;
        
        container.backgroundColor = [UIColor whiteColor];
        container.rectCornerRadius = 8;
        container.rectCorner = UIRectCornerTopLeft | UIRectCornerTopRight;
        
        MGRecordsTypeView *recordsView = [[MGRecordsTypeView alloc] init];
        recordsView.selectRecordTypeBlock = ^(MGRecordType type) {
            if (tapedBlock) {
                tapedBlock(type);
            }
            [weakAlertView closeAlertView];
        };
        
        recordsView.closeTapedBlock = ^{
            [weakAlertView closeAlertView];
        };
        [container addCustomView:recordsView configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            NSInteger rowCount = 3;
            action.size = CGSizeMake(0, 60 + 80 * rowCount + 15 * (rowCount - 1) + 80);
        }];
        
        [container addActionWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
            action.size = CGSizeMake(0, [UIDevice safeAreaInsetsBottom]);
            button.backgroundColor = [UIColor whiteColor];
        } tapedOnBlock:^(NSInteger index) {
            
        }];
    }];
    
    [alertView showAlertViewOnKeyWindow];
    return alertView;
}

+ (void)showAccountsAlertViewWithSelectModel:(MGAccountModel *)selectModel selectedBlock:(void (^)(MGAccountModel * _Nonnull))selectedBlock selectActionBlock:(void (^)(MGAlertViewActionType))selectActionBlock
{
    [self showAccountsAlertViewWithSelectModel:selectModel selectedBlock:selectedBlock selectActionBlock:selectActionBlock shouldShowAllAccounts:NO isSelectAllAccounts:NO hidesAllActions:NO];
}

+ (void)showAccountsAlertViewWithSelectModel:(nonnull MGAccountModel *)selectModel
                               selectedBlock:(void (^)(MGAccountModel * _Nonnull))selectedBlock
                           selectActionBlock:(void (^)(MGAlertViewActionType))selectActionBlock
                       shouldShowAllAccounts:(BOOL)shouldShowAllAccounts isSelectAllAccounts:(BOOL)isSelectAllAccounts hidesAllActions:(BOOL)hidesAllActions
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    
    __weak typeof(alertView) weakAlertView = alertView;
    MGAccountSelectView *view = [[MGAccountSelectView alloc] init];
    [view configViewWithDataModel:selectModel shouldShowAllAccounts:shouldShowAllAccounts selectAllAccounts:isSelectAllAccounts hidesAllActions:hidesAllActions];
    
    view.selectTypeBlock = ^(MGAlertViewActionType type) {
        if (type != MGAlertViewActionTypeCancel) {
            selectActionBlock(type);
        }
        [weakAlertView closeAlertView];
    };
    
    view.dataModelSelectBlock = ^(MGAccountModel * _Nonnull dataModel) {
        if (selectedBlock) {
            selectedBlock(dataModel);
        }
        [weakAlertView closeAlertView];
    };
    
    [self showAlertView:alertView customView:view viewsMaxHeight:500];
}

+ (void)showCategoriesAlertViewWithSelectModel:(nonnull MGCategoryModel *)selectModel
                                 selectedBlock:(nonnull void (^)(MGCategoryModel * _Nonnull))selectedBlock
                             selectActionBlock:(nonnull void (^)(MGAlertViewActionType))selectActionBlock
{
    [self showCategoriesAlertViewWithSelectModel:selectModel selectedBlock:selectedBlock selectActionBlock:selectActionBlock shouldShowAllCategories:NO isSelectAllCategories:NO hidesAllActions:NO];
}

+ (void)showCategoriesAlertViewWithSelectModel:(nonnull MGCategoryModel *)selectModel
                                 selectedBlock:(nonnull void (^)(MGCategoryModel * _Nonnull))selectedBlock
                             selectActionBlock:(nonnull void (^)(MGAlertViewActionType))selectActionBlock
                       shouldShowAllCategories:(BOOL)shouldShowAllCategories isSelectAllCategories:(BOOL)isSelectAllCategories hidesAllActions:(BOOL)hidesAllActions
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    
    __weak typeof(alertView) weakAlertView = alertView;
    MGCategorySelectView *view = [[MGCategorySelectView alloc] init];
    [view configViewWithDataModel:selectModel shouldShowAllCategories:shouldShowAllCategories selectAllCategories:isSelectAllCategories hidesAllActions:hidesAllActions];
    
    view.selectTypeBlock = ^(MGAlertViewActionType type) {
        if (type != MGAlertViewActionTypeCancel) {
            selectActionBlock(type);
        }
        [weakAlertView closeAlertView];
    };
    
    view.dataModelSelectBlock = ^(MGCategoryModel * _Nonnull dataModel) {
        if (selectedBlock) {
            selectedBlock(dataModel);
        }
        [weakAlertView closeAlertView];
    };
    
    [self showAlertView:alertView customView:view viewsMaxHeight:500];
}

+ (void)showCategoryTypesAlertViewWithSelectType:(MGCategoryType)selectType selectedBlock:(void (^)(MGCategoryType))selectedBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    
    __weak typeof(alertView) weakAlertView = alertView;
    MGCategoryTypesSelectView *view = [[MGCategoryTypesSelectView alloc] init];
    view.selectType = selectType;
    view.selectTypeBlock = ^(MGCategoryType type) {
        selectedBlock(type);
        [weakAlertView closeAlertView];
    };
    
    [self showAlertView:alertView customView:view viewsMaxHeight:465];
}

+ (void)showAccountTypesAlertViewWithSelectType:(MGAccountType)selectType selectedBlock:(void (^)(MGAccountType))selectedBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    
    __weak typeof(alertView) weakAlertView = alertView;
    MGAccountTypesSelectView *view = [[MGAccountTypesSelectView alloc] init];
    view.selectType = selectType;
    view.selectTypeBlock = ^(MGAccountType type) {
        selectedBlock(type);
        [weakAlertView closeAlertView];
    };
    
    [self showAlertView:alertView customView:view viewsMaxHeight:465];
}

+ (void)showReceiptDateAlertViewWithDate:(NSDate *)selectDate dateOrTime:(BOOL)dateOrTime dateSelectBlock:(nonnull void (^)(NSDate * _Nonnull, BOOL))dateSelectBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    
    __weak typeof(alertView) weakAlertView = alertView;
    MGReceiptDateView *view = [[MGReceiptDateView alloc] init];
    view.dateOrTime = dateOrTime;
    view.selectDate = selectDate;
    
    view.selectTypeBlock = ^(MGAccountType type) {
//        selectedBlock(type);
        [weakAlertView closeAlertView];
    };
    
    view.dateChangeBlock = ^(NSDate * _Nonnull date, BOOL dateOrTime) {
        if (dateSelectBlock) {
            dateSelectBlock(date, dateOrTime);
        }
        [weakAlertView closeAlertView];
    };
    
    [self showAlertView:alertView customView:view viewsMaxHeight:360];
}

// ===========================================================
+ (MGAlertView *)showEmptyStatusAlertViewInView:(UIView *)inView text:(NSString *)text actionText:(nonnull NSString *)actionText insets:(UIEdgeInsets)insets tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock
{
    return [self showStatusAlertViewInView:inView statusIcon:[UIImage imageNamed:@"state_blank"] text:text actionText:actionText marginInsets:insets paddingInsets:UIEdgeInsetsZero tapedActionBlock:tapedActionBlock];
}

+ (MGAlertView *)showTextAlertViewInView:(UIView *)inView text:(NSString *)text actionText:(nonnull NSString *)actionText insets:(UIEdgeInsets)insets tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock
{
    return [self showStatusAlertViewInView:inView statusIcon:nil text:text actionText:actionText marginInsets:insets paddingInsets:UIEdgeInsetsZero tapedActionBlock:tapedActionBlock];
}

+ (MGAlertView *)showStatusAlertViewInView:(UIView *)inView statusIcon:(nullable UIImage *)statusIcon text:(NSString *)text actionText:(NSString *)actionText
                              marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets tapedActionBlock:(YYBAlertViewBlock)tapedActionBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.backgroundColor = APP_COLOR_BACKGROUND;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalHeight = [UIScreen mainScreen].bounds.size.height;
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        
        if (statusIcon) {
            [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
                action.size = CGSizeMake(0, paddingInsets.top * 2);
            }];
            [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
                action.size = CGSizeMake(200, 200);
                imageView.image = statusIcon;
            }];
        }
        if (text && text.length > 0) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                label.text = text;
                label.textColor = TEXT_COLOR_FIRST;
                label.font = FONT_REGULAR(16);
            }];
        }
        if (actionText && actionText.length > 0) {
            [container addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.margin = UIEdgeInsetsMake(15, 0, 0, 0);
                action.size = CGSizeMake(120, 40);
                [button setTitle:actionText forState:0];
                [button setTitleColor:APP_COLOR_MAIN forState:0];
                [button setBackgroundImage:[APP_COLOR_MAIN colorWithAlphaComponent:0.2].colorToUIImage forState:0];
                [button setBackgroundImage:[APP_COLOR_MAIN colorWithAlphaComponent:0.1].colorToUIImage forState:UIControlStateHighlighted];
                button.titleLabel.font = YYBFONT_MEDIUM(14);
                [button cornerRadius:20];
            } tapedOnBlock:^{
                if (tapedActionBlock) {
                    tapedActionBlock();
                }
            }];
        }
        [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.size = CGSizeMake(0, paddingInsets.bottom * 2);
        }];
    }];
    
    [inView addSubview:alertView];
    alertView.frame = CGRectMake(marginInsets.left, marginInsets.top,
                                 CGRectGetWidth(inView.frame) - marginInsets.left - marginInsets.right,
                                 CGRectGetHeight(inView.frame) - marginInsets.top - marginInsets.bottom);
    [alertView showAlertView];
    
    return alertView;
}

@end
