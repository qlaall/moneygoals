//
//  MGAlertView+BaseViews.h
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertView.h"
#import "MGRecordTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertView (BaseViews)

+ (MGAlertView *)showOnlyTextAlertViewInView:(UIView *)inView insets:(UIEdgeInsets)insets title:(NSString *)title;

+ (MGAlertView *)showEmptyMaskAlertViewInView:(UIView *)inView insets:(UIEdgeInsets)insets title:(NSString *)title content:(nullable NSString *)content tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock;

+ (MGAlertView *)showRecordsTypeAlertViewWithTapedBlock:(void (^)(MGRecordType type))tapedBlock;

+ (void)showAccountsAlertViewWithSelectModel:(MGAccountModel *)selectModel selectedBlock:(void (^)(MGAccountModel * dataModel))selectedBlock selectActionBlock:(void (^)(MGAlertViewActionType type))selectActionBlock;
+ (void)showAccountsAlertViewWithSelectModel:(MGAccountModel *)selectModel selectedBlock:(void (^)(MGAccountModel * dataModel))selectedBlock selectActionBlock:(void (^)(MGAlertViewActionType type))selectActionBlock shouldShowAllAccounts:(BOOL)shouldShowAllAccounts isSelectAllAccounts:(BOOL)isSelectAllAccounts hidesAllActions:(BOOL)hidesAllActions;

+ (void)showCategoriesAlertViewWithSelectModel:(MGCategoryModel *)selectModel selectedBlock:(void (^)(MGCategoryModel * dataModel))selectedBlock selectActionBlock:(void (^)(MGAlertViewActionType type))selectActionBlock;
+ (void)showCategoriesAlertViewWithSelectModel:(MGCategoryModel *)selectModel selectedBlock:(void (^)(MGCategoryModel * dataModel))selectedBlock selectActionBlock:(void (^)(MGAlertViewActionType type))selectActionBlock shouldShowAllCategories:(BOOL)shouldShowAllCategories isSelectAllCategories:(BOOL)isSelectAllCategories hidesAllActions:(BOOL)hidesAllActions;

+ (void)showCategoryTypesAlertViewWithSelectType:(MGCategoryType)selectType selectedBlock:(void (^)(MGCategoryType type))selectedBlock;

+ (void)showAccountTypesAlertViewWithSelectType:(MGAccountType)selectType selectedBlock:(void (^)(MGAccountType type))selectedBlock;

+ (void)showAlertView:(MGAlertView *)alertView customView:(UIView *)customView viewsMaxHeight:(CGFloat)viewsMaxHeight;

+ (void)showReceiptDateAlertViewWithDate:(NSDate *)selectDate dateOrTime:(BOOL)dateOrTime dateSelectBlock:(void (^)(NSDate *date, BOOL dateOrTime))dateSelectBlock;

// =========================================
+ (MGAlertView *)showEmptyStatusAlertViewInView:(UIView *)view text:(NSString *)text actionText:(NSString *)actionText insets:(UIEdgeInsets)insets tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock;
+ (MGAlertView *)showTextAlertViewInView:(UIView *)view text:(NSString *)text actionText:(NSString *)actionText insets:(UIEdgeInsets)insets tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock;
+ (MGAlertView *)showStatusAlertViewInView:(UIView *)view statusIcon:(nullable UIImage *)statusIcon text:(NSString *)text actionText:(NSString *)actionText
                              marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock;

@end

NS_ASSUME_NONNULL_END
