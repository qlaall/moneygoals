//
//  MGConfigManager.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGConfigModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGConfigManager : NSObject

+ (MGConfigManager *)shared;
- (void)initialize;
- (void)syncConfigModel;

@property (nonatomic, strong) MGConfigModel *dataModel;

- (void)handleCommentApp;

- (void)handleChangeUser:(MGUserModel *)user;
- (void)configWithBlock:(void (^)(MGConfigModel *dataModel))configBlock;

@end

NS_ASSUME_NONNULL_END
