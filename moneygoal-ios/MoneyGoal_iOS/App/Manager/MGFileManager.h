//
//  MGFileManager.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/13.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGFileManager : NSObject

+ (MGFileManager *)shared;

- (void)createStaticDirectory:(nullable NSString *)storeDirectory completeBlock:(nullable void (^)(NSError *error))completeBlock;
- (NSString *)directoryWithSubPath:(NSString *)subPath;
- (void)initWithCompleteBlock:(YYBTapedActionBlock)completeBlock;

@end

NS_ASSUME_NONNULL_END
