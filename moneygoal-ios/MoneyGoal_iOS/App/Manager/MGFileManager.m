//
//  MGFileManager.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/13.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGFileManager.h"

@implementation MGFileManager

+ (MGFileManager *)shared
{
    static MGFileManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[MGFileManager alloc] init];
    });
    return manager;
}

- (void)initWithCompleteBlock:(YYBTapedActionBlock)completeBlock
{
    NSString *projectPath = [self directoryWithSubPath:@""];
    NSLog(@"%@", projectPath);
    
    [self createStaticDirectory:projectPath completeBlock:^(NSError * _Nonnull error) {
        if (error) {
            [YYBAlertView showAlertViewWithError:error];
        } else {
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            // 创建Record类型
            NSString *recordPath = [self directoryWithSubPath:RECORD_CACHE_PATH];
            [self createStaticDirectory:recordPath completeBlock:^(NSError * _Nonnull error) {
                NSLog(@"Record类型创建文件夹错误: %@", error);
                dispatch_semaphore_signal(semaphore);
            }];

//            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//            // 创建打卡类型
//            NSString *receiptPath = [self directoryWithSubPath:RECEIPT_CACHE_PATH];
//            [self createStaticDirectory:receiptPath completeBlock:^(NSError * _Nonnull error) {
//                NSLog(@"Receipts类型创建文件夹错误: %@", error);
//                dispatch_semaphore_signal(semaphore);
//            }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            // 创建账户类型
            NSString *accountPath = [self directoryWithSubPath:ACCOUNT_CACHE_PATH];
            [self createStaticDirectory:accountPath completeBlock:^(NSError * _Nonnull error) {
                NSLog(@"Accounts类型创建文件夹错误: %@", error);
                dispatch_semaphore_signal(semaphore);
            }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            // 创建分类类型
            NSString *categoryPath = [self directoryWithSubPath:CATEGORY_CACHE_PATH];
            [self createStaticDirectory:categoryPath completeBlock:^(NSError * _Nonnull error) {
                NSLog(@"Categories类型创建文件夹错误: %@", error);
                dispatch_semaphore_signal(semaphore);
            }];
            
            if (completeBlock) {
                completeBlock();
            }
        }
    }];
}

// 创建文件夹
- (void)createStaticDirectory:(nullable NSString *)storeDirectory completeBlock:(nullable void (^)(NSError * _Nonnull))completeBlock {
    if (storeDirectory) {
        NSError *error = nil;
        if (![[NSFileManager defaultManager] fileExistsAtPath:storeDirectory]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:storeDirectory withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        NSLog(@"Directory%@ == %@",storeDirectory, error);
        if (completeBlock) {
            completeBlock(error);
        }
    }
}

- (NSString *)directoryWithSubPath:(NSString *)subpath {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) objectAtIndex:0];
    path = [path stringByAppendingPathComponent:@"com.Moneyease.Records"];
    
    if ([subpath isExist]) {
        path = [path stringByAppendingPathComponent:subpath];
    }
    return path;
}

@end
