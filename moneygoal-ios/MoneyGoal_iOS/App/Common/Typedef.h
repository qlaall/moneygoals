//
//  Typedef.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#ifndef Typedef_h
#define Typedef_h


// =====================================
//#define AES_KEY_DEV @"kuch$s#dcc!locsa" // dev
#define AES_KEY_DEV @"a#8qxlv!219k2dDX" // 正式服务器
//#define AES_KEY_DEV @"#cjdh76wwe@19j2d" // 家里本地key

#define MGNOTI_REFRESH_DATA @"NOTI_REFRESH_DATA"
#define MGNOTI_CREATERECEIPT @"MGNOTI_CREATERECEIPT"
#define MGNOTI_RECORDCHANGE @"MGNOTI_RECORDCHANGE"

#define UNIQUE_ID_CANUPLOAD 8888888888
#define UNIQUE_ID_CANTUPLOAD 9999999999


#endif /* Typedef_h */
