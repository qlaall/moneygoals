//
//  YYBMultiScrollContainerView.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/13.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBMultiScrollContainerView.h"

@interface YYBMultiScrollContainerView () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
@property (nonatomic, strong) NSMutableDictionary *containerViews;

@end

@implementation YYBMultiScrollContainerView

- (instancetype)init
{
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    return [self initWithScrollView:scrollView];
}

- (instancetype)initWithScrollView:(UIScrollView *)scrollView
{
    self = [super init];
    if (!self) return nil;
    
    _containerViews = [NSMutableDictionary dictionary];
    
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    
    _scrollView = scrollView;
    [self addSubview:_scrollView];
    
    return self;
}

- (UITableView *)currentTableView
{
    return [self getTableViewWithIndex:_itemIndex];
}

- (UITableView *)tableViewAtIndex:(NSInteger)itemIndex
{
    return [self getTableViewWithIndex:itemIndex];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSInteger count = [self.delegate numbersOfSectionInContainerView:self];
    
    _scrollView.frame = self.bounds;
    _scrollView.contentSize = CGSizeMake(count * CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    for (NSNumber *keys in self.containerViews) {
        NSInteger index = keys.integerValue;
        
        UITableView *view = [self.containerViews objectForKey:keys];
        view.frame = CGRectMake(index * width, 0, width, height);
    }
}

- (void)refreshScrollContainerView
{
    while (self.scrollView.subviews.count) {
        [self.scrollView.subviews.lastObject removeFromSuperview];
    }
    
    _containerViews = [NSMutableDictionary dictionary];
    
    NSInteger count = 0;
    if ([self.delegate respondsToSelector:@selector(numbersOfSectionInContainerView:)]) {
        count = [self.delegate numbersOfSectionInContainerView:self];
    }
    
    _scrollView.contentSize = CGSizeMake(count * CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    [self createOrUpdateScrollViewAtItemIndex:self.initialSection];
    
    if (self.initialSection != 0) {
        [_scrollView setContentOffset:CGPointMake(self.initialSection * CGRectGetWidth(self.frame), 0) animated:NO];
    }
}

- (void)refreshContainerViewAtIndex:(NSInteger)index
{
    UITableView *tableView = [self getTableViewWithIndex:index];
    if (!tableView) {
        [self createOrUpdateScrollViewAtItemIndex:index];
        tableView = [self getTableViewWithIndex:index];
    }
    
    if (tableView) {
        [tableView reloadData];
    }
}

- (void)containerViewScrollToItemIndex:(NSInteger)itemIndex
{
    // BUG 调用的时候很有可能未初始化frame
    CGFloat width = CGRectGetWidth(self.scrollView.frame);
    [_scrollView setContentOffset:CGPointMake(width * itemIndex, 0) animated:NO];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat x = scrollView.contentOffset.x;
    CGFloat width = CGRectGetWidth(scrollView.frame);
    NSInteger index = x / width;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(containerView:scrollViewDidScrollWithOffset:)]) {
        [self.delegate containerView:self scrollViewDidScrollWithOffset:x];
    }

    if (scrollView == _scrollView && width != 0) {
        if (index != _itemIndex) {
            _itemIndex = index;
            [self createOrUpdateScrollViewAtItemIndex:index];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(containerView:scrollToItemAtIndex:)]) {
                [self.delegate containerView:self scrollToItemAtIndex:_itemIndex];
            }
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(containerView:tableViewDidScrollWithOffset:itemIndex:)]) {
            [self.delegate containerView:self tableViewDidScrollWithOffset:scrollView.contentOffset.y itemIndex:index];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _scrollView) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(containerView:didEndScrollAnimationAtItemIndex:)]) {
            [self.delegate containerView:self didEndScrollAnimationAtItemIndex:_itemIndex];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView != _scrollView) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(containerView:didEndDraggingAtItemIndex:)]) {
            [self.delegate containerView:self didEndDraggingAtItemIndex:_itemIndex];
        }
    }
}

- (UITableView *)getTableViewWithIndex:(NSInteger)indexKey
{
    NSString *key = @(indexKey).stringValue;
    
    if ([self.containerViews.allKeys containsObject:key]) {
        return [self.containerViews objectForKey:key];
    }
    
    return nil;
}

- (void)createOrUpdateScrollViewAtItemIndex:(NSInteger)itemIndex
{
    UITableView *tableView = [self getTableViewWithIndex:itemIndex];
    
    if (tableView == nil) {
        tableView = [self createTableViewWithIndex:itemIndex];
        
        [self.scrollView addSubview:tableView];
        [self.delegate configureTableViewInContainerView:self tableView:tableView itemIndex:itemIndex];
    }
}

- (UITableView *)createTableViewWithIndex:(NSInteger)indexKey
{
    NSString *key = @(indexKey).stringValue;
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    CGRect rect = CGRectMake(indexKey * width, 0, width, height);
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:rect];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.estimatedRowHeight = 0;
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.containerViews setValue:tableView forKey:key];
    
    return tableView;
}

- (NSInteger)keyForTableView:(UITableView *)tableView
{
    NSInteger valueIndex = [self.containerViews.allValues indexOfObject:tableView];
    NSInteger key = [[self.containerViews.allKeys objectAtIndex:valueIndex] integerValue];
    return key;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger key = [self keyForTableView:tableView];
    return [self.delegate numbersOfSectionsInContainerView:self tableView:tableView itemIndex:key];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger key = [self keyForTableView:tableView];
    return [self.delegate numbersOfRowsInContainerView:self tableView:tableView itemIndex:key section:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger key = [self keyForTableView:tableView];
    return [self.delegate heightForRowsInContainerView:self tableView:tableView itemIndex:key indexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger key = [self keyForTableView:tableView];
    return [self.delegate cellForRowInContainerView:self tableView:tableView itemIndex:key indexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger key = [self keyForTableView:tableView];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedRowInContainerView:tableView:itemIndex:indexPath:)]) {
        [self.delegate didSelectedRowInContainerView:self tableView:tableView itemIndex:key indexPath:indexPath];
    }
}

@end
