//
//  YYBRoundLoadingView.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/17.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRoundLoadingView : UIView

@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, strong) CAGradientLayer *gradientLayer;

- (void)beginAnimation;
- (void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
