//
//  YYBMultiScrollContainerView.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/13.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class YYBMultiScrollContainerView;

@protocol YYBMultiScrollContainerViewDelegate <NSObject>

@optional

- (NSInteger)numbersOfSectionInContainerView:(YYBMultiScrollContainerView *)containerView;
/// itemIndex sectonView的序号
- (void)configureTableViewInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex;

- (void)containerView:(YYBMultiScrollContainerView *)containerView scrollToItemAtIndex:(NSInteger)itemIndex;

- (NSInteger)numbersOfSectionsInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex;
- (NSInteger)numbersOfRowsInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex section:(NSInteger)section;

- (CGFloat)heightForRowsInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath;

- (UITableViewCell *)cellForRowInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath;

- (void)didSelectedRowInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath;

- (void)containerView:(YYBMultiScrollContainerView *)containerView tableViewDidScrollWithOffset:(CGFloat)offset itemIndex:(NSInteger)itemIndex;
- (void)containerView:(YYBMultiScrollContainerView *)containerView scrollViewDidScrollWithOffset:(CGFloat)offset;
// 滚动动画结束后
- (void)containerView:(YYBMultiScrollContainerView *)containerView didEndScrollAnimationAtItemIndex:(NSInteger)itemIndex;
- (void)containerView:(YYBMultiScrollContainerView *)containerView didEndDraggingAtItemIndex:(NSInteger)itemIndex;

@end

@interface YYBMultiScrollContainerView : UIView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView;

@property (nonatomic, weak) id <YYBMultiScrollContainerViewDelegate> delegate;
@property (nonatomic, strong) UIScrollView *scrollView;

- (void)refreshScrollContainerView;
- (void)refreshContainerViewAtIndex:(NSInteger)index;

- (UITableView *)tableViewAtIndex:(NSInteger)itemIndex;

- (void)containerViewScrollToItemIndex:(NSInteger)itemIndex;
- (void)createOrUpdateScrollViewAtItemIndex:(NSInteger)itemIndex;

- (UITableView *)currentTableView;

@property (nonatomic) NSInteger initialSection;
@property (nonatomic) NSInteger itemIndex;

@end

NS_ASSUME_NONNULL_END
