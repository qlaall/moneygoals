//
//  YYBPlaceholderTextView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/8/6.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYBPlaceholderTextView : UITextView

// 修改控件的内边距用‘textContainerInset’属性
// 占位字符串
@property (nonatomic, copy) NSString *placeholder;

// 控件占位文字属性
// 默认‘黑色’ 0.4alpha的颜色 15号字体
@property (nonatomic,strong) NSDictionary *placeholderTextAttributes;
// 默认是居左
@property (nonatomic) NSTextAlignment placeholderTextAlignment;

- (void)reloadPlaceholder;
- (void)renderWithFont:(UIFont *)font placeholderColor:(UIColor *)placeholderColor lineSpace:(CGFloat)lineSpace;

@end
