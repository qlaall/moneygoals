//
//  YYBTabBar.m
//  YYBBaseViews
//
//  Created by alchemy on 2019/1/2.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBTabBar.h"

@interface YYBTabBar ()
@property (nonatomic, strong) YYBNavigationBarContainer *selectedContainer;
@property (nonatomic, strong) NSMutableDictionary *containerKeys;

@end

@implementation YYBTabBar
{
    NSInteger _selectedIndex;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    _shadowView = [[UIView alloc] initWithFrame:frame];
    _shadowView.backgroundColor = [UIColor clearColor];
    [self addSubview:_shadowView];
    
    _contentBackgroundView = [[UIImageView alloc] init];
    _contentBackgroundView.contentMode = UIViewContentModeScaleAspectFill;
    _contentBackgroundView.clipsToBounds = YES;
    [_shadowView addSubview:_contentBackgroundView];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = [UIColor clearColor];
    [_shadowView addSubview:_contentView];
    
    _separateView = [[UIView alloc] init];
    _separateView.backgroundColor = [UIColor colorWithHexValue:0xDDDDDD];
    [_contentView addSubview:_separateView];
    
    _visualEffectView = [[UIVisualEffectView alloc] init];
    _visualEffectView.alpha = 1.0;
    _visualEffectView.hidden = YES;
    [_contentView addSubview:_visualEffectView];
    
    _componentsBackgroundView = [UIImageView new];
    _componentsBackgroundView.contentMode = UIViewContentModeScaleAspectFill;
    _componentsBackgroundView.clipsToBounds = YES;
    [_contentView addSubview:_componentsBackgroundView];
    
    _componentsView = [[UIView alloc] init];
    _componentsView.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_componentsView];
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat height = CGRectGetHeight(self.frame);
    CGFloat width = CGRectGetWidth(self.frame);
    
    CGFloat offsetHeight = height - _containerOffset;
    
    _shadowView.frame = self.bounds;
    _contentBackgroundView.frame = self.bounds;
    _contentView.frame = _contentBackgroundView.frame;
    _visualEffectView.frame = self.bounds;
    _separateView.frame = CGRectMake(0, 0, width, 0.5f);
    _componentsBackgroundView.frame = CGRectMake(0, 0, width, offsetHeight);
    _componentsView.frame = _componentsBackgroundView.frame;
    
    NSInteger containersCount = _containerKeys.allKeys.count;
    
    NSMutableArray *keys = [_containerKeys.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        return obj1.integerValue > obj2.integerValue;
    }].mutableCopy;
    
    CGFloat x = 0;
    CGFloat widthSpecific = 0; // 先算一遍 获得特殊的宽度总长，余下的就均分
    NSMutableDictionary *widthKeys = [NSMutableDictionary new];
    
    for (NSString *key in keys) {
        NSInteger index = key.integerValue;
        
        CGSize itemSize = CGSizeZero;
        if (self.delegate && [self.delegate respondsToSelector:@selector(containerSizeWithComponentIndex:)]) {
            itemSize = [self.tabBarDelegate containerSizeWithComponentIndex:index];
        }
        
        if (!CGSizeEqualToSize(CGSizeZero, itemSize)) {
            [widthKeys setObject:[NSValue valueWithCGSize:itemSize] forKey:key];
            widthSpecific += itemSize.width;
        }
    }
    
    CGFloat widthOther = (width - widthSpecific) / (containersCount - widthKeys.allKeys.count);
    for (NSString *key in keys) {
        YYBNavigationBarContainer *container = [_containerKeys objectForKey:key];
        
        CGSize itemSize = CGSizeZero;
        if (![widthKeys.allKeys containsObject:key]) {
            itemSize = CGSizeMake(widthOther, offsetHeight);
        } else {
            itemSize = [[widthKeys objectForKey:key] CGSizeValue];
        }
        
        CGFloat offsetX = container.contentEdgeInsets.left - container.contentEdgeInsets.right;
        CGFloat offsetY = container.contentEdgeInsets.top - container.contentEdgeInsets.bottom;
        
        container.frame = CGRectMake(x + offsetX, height - itemSize.height - _containerOffset + offsetY, itemSize.width, itemSize.height);
        x += itemSize.width;
    }
}

- (void)refreshTabBarContainers
{
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    if (self.tabBarDelegate && [self.tabBarDelegate respondsToSelector:@selector(customBlurEffect)]) {
        effect = [self.tabBarDelegate customBlurEffect];
    }
    
    _visualEffectView.effect = effect;
    
    NSInteger count = 0;
    if (self.tabBarDelegate && [self.tabBarDelegate respondsToSelector:@selector(numbersOfContainersInTabBar:)]) {
        count = [self.tabBarDelegate numbersOfContainersInTabBar:self];
    }
    
    _containerKeys = [NSMutableDictionary new];
    for (NSInteger index = 0; index < count; index ++) {
        YYBNavigationBarContainer *containerItem = [_tabBarDelegate tabBarContainerInTabBar:self componentIndex:index];
        [_containerKeys setValue:containerItem forKey:@(index).stringValue];
    }
    
    for (UIView *subView in _componentsView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSMutableArray *keys = [_containerKeys.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        return obj1.integerValue > obj2.integerValue;
    }].mutableCopy;
    
    for (NSString *containerKey in keys) {
        NSInteger containerIndex = [containerKey integerValue];
        YYBNavigationBarContainer *containerItem = [_containerKeys objectForKey:containerKey];
        
        __weak typeof(self) weakself = self;
        containerItem.tapedActionBlock = ^(YYBNavigationBarContainer *view) {
            [weakself containerSelectedAction:view];
        };
        
        [_componentsView addSubview:containerItem];
        
        NSInteger tabBarIndex = 0;
        if (self.delegate && [self.tabBarDelegate respondsToSelector:@selector(firstSelectedContainerIndex)]) {
            tabBarIndex = [self.tabBarDelegate firstSelectedContainerIndex];
        }
        
        if (tabBarIndex == containerIndex) {
            _selectedContainer.selected = NO;
            containerItem.selected = YES;
            _selectedContainer = containerItem;
        }
    }
    
    [self setNeedsLayout];
}

- (NSInteger)componentIndexWithContainer:(YYBNavigationBarContainer *)container
{
    for (NSString *key in _containerKeys.allKeys) {
        if ([_containerKeys objectForKey:key] == container) {
            return key.integerValue;
        }
    }
    return 0;
}

- (void)containerSelectedAction:(YYBNavigationBarContainer *)sender
{
    NSInteger index = [self componentIndexWithContainer:sender];
    
    BOOL status = YES;
    if (self.tabBarDelegate && [self.tabBarDelegate respondsToSelector:@selector(containerCouldSelectedWithTabBar:componentIndex:)]) {
        status = [self.tabBarDelegate containerCouldSelectedWithTabBar:self componentIndex:index];
    }
    
    if (self.delegate && status) {
        _selectedContainer.selected = NO;
        sender.selected = YES;
        _selectedContainer = sender;
        
        if (self.tabBarDelegate && [self.delegate respondsToSelector:@selector(didSelectedContainerInTabBar:componentIndex:)]) {
            [self.tabBarDelegate didSelectedContainerInTabBar:self componentIndex:index];
        }
    }
}

- (void)selectComponentAtComponentIndex:(NSInteger)index
{
    YYBNavigationBarContainer *container = [_containerKeys objectForKey:@(index).stringValue];
    if (container) {
        [self containerSelectedAction:container];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (self.tabBarDelegate && [self.tabBarDelegate respondsToSelector:@selector(shouldHandleHitTestWithTabBar:)]) {
        if ([self.tabBarDelegate shouldHandleHitTestWithTabBar:self]) {
            
            NSArray *blocks = self.containerKeys.allValues;
            if (blocks && blocks.count > 0) {
                for (NSInteger index = 0; index < blocks.count; index ++) {
                    YYBNavigationBarControl *container = [blocks objectAtIndex:index];
                    CGPoint convertPoint = [self convertPoint:point toView:container];
                    if ([container pointInside:convertPoint withEvent:event]) {
                        return container;
                    }
                }
            }
        }
    }
    
    return [super hitTest:point withEvent:event];
}

@end
