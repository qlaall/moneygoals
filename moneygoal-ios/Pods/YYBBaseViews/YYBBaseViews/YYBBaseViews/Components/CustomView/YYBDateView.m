//
//  YYBDateView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/11/1.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBDateView.h"

@implementation YYBDateView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    __weak typeof(self) weakself = self;
    
     _cancelButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.height.mas_equalTo(45);
        make.top.equalTo(self);
        make.width.mas_equalTo(100);
    } configureBlock:^(UIButton *button) {
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
        [button setTitle:@"取消" forState:0];
    } tapedBlock:^(UIButton *sender) {
        if (weakself.cancelTapedBlock) {
            weakself.cancelTapedBlock();
        }
    }];
    
    _submitButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-20);
        make.height.mas_equalTo(45);
        make.top.equalTo(self);
        make.width.mas_equalTo(100);
    } configureBlock:^(UIButton *button) {
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        button.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
        [button setTitle:@"确定" forState:0];
    } tapedBlock:^(UIButton *sender) {
        if (weakself.confirmTapedBlock) {
            weakself.confirmTapedBlock(weakself.datePicker.date);
        }
    }];
    
    _datePicker = [UIDatePicker createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.top.equalTo(self.cancelButton.mas_bottom);
    } configureBlock:^(UIDatePicker *view) {
        view.datePickerMode = UIDatePickerModeTime;
        view.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
        if(@available(iOS 13.4, *)) {
            view.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
    }];
    
    return self;
}

@end
