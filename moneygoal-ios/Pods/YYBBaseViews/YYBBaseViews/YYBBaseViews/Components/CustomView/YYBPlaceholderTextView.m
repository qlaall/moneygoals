//
//  YYBPlaceholderTextView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/8/6.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPlaceholderTextView.h"

@implementation YYBPlaceholderTextView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.placeholderTextAlignment = NSTextAlignmentLeft;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    return self;
}

- (void)textDidChanged:(UITextView *)textView
{
    [self setNeedsDisplay];
}

- (void)reloadPlaceholder
{
    [self setNeedsDisplay];
}

- (void)renderWithFont:(UIFont *)font placeholderColor:(UIColor *)placeholderColor lineSpace:(CGFloat)lineSpace
{
    self.font = font;
    
    NSMutableParagraphStyle *graph = [[NSMutableParagraphStyle alloc] init];
    graph.lineSpacing = lineSpace;
    graph.alignment = NSTextAlignmentLeft;
    
    NSMutableDictionary *attr = [NSMutableDictionary new];
    attr[NSFontAttributeName] = font;
    attr[NSParagraphStyleAttributeName] = graph;

    self.typingAttributes = attr;
    self.placeholderTextAttributes = @{NSForegroundColorAttributeName : placeholderColor,
                                       NSFontAttributeName : font,
                                       NSParagraphStyleAttributeName : graph };
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (!self.placeholder) {
        return;
    }
    
    if (self.text.length > 0) {
        return;
    }
    
    NSDictionary *placeholderAttribute = @{NSForegroundColorAttributeName:[[UIColor blackColor] colorWithAlphaComponent:0.4f],
                                           NSFontAttributeName:[UIFont systemFontOfSize:15]};
    if (self.placeholderTextAttributes) {
        placeholderAttribute = self.placeholderTextAttributes;
    }
    
    CGSize placeholderSize = [self.placeholder boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.frame) - self.textContainerInset.left - self.textContainerInset.right, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:placeholderAttribute context:nil].size;
    
    CGRect frame = CGRectZero;
    switch (self.placeholderTextAlignment)
    {
        case NSTextAlignmentCenter:
        {
            frame = CGRectMake((CGRectGetWidth(self.frame) - placeholderSize.width) / 2,
                               self.textContainerInset.top,
                               placeholderSize.width,
                               placeholderSize.height);
        }
            break;
        case NSTextAlignmentLeft:
        {
            frame = CGRectMake(self.textContainerInset.left + self.textContainer.lineFragmentPadding,
                               self.textContainerInset.top,
                               placeholderSize.width,
                               placeholderSize.height);
        }
            break;
        case NSTextAlignmentRight:
        {
            frame = CGRectMake(CGRectGetWidth(self.frame) - placeholderSize.width - self.textContainerInset.right - self.textContainer.lineFragmentPadding,
                               self.textContainerInset.top,
                               placeholderSize.width,
                               placeholderSize.height);
        }
            break;
        default:
            break;
    }
    
    [self.placeholder drawInRect:frame
                  withAttributes:placeholderAttribute];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
