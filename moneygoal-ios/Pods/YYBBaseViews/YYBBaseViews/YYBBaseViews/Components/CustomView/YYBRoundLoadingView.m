//
//  YYBRoundLoadingView.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/17.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRoundLoadingView.h"

@interface YYBRoundLoadingView ()

@end

@implementation YYBRoundLoadingView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor clearColor];
    
    _gradientLayer = [CAGradientLayer layer];
    _gradientLayer.colors = @[(id)[UIColor colorWithHexValue:0x000000 alpha:0.8].CGColor,
                              (id)[UIColor colorWithHexValue:0x000000 alpha:0.5].CGColor];
    _gradientLayer.startPoint = CGPointMake(0, 0);
    _gradientLayer.endPoint = CGPointMake(0, 1);
    _gradientLayer.locations = @[@(0),@(1)];
    
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.lineWidth = 4.0f;
    _shapeLayer.fillColor = [UIColor clearColor].CGColor;
    _shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    _gradientLayer.mask = _shapeLayer;
    
    [self.layer addSublayer:_gradientLayer];
    
    return self;
}

- (void)beginAnimation
{
    CABasicAnimation *anim = [self createAnimation];
    [self.gradientLayer addAnimation:anim forKey:nil];
}

- (void)stopAnimation
{
    [self.gradientLayer removeAllAnimations];
}

- (CABasicAnimation *)createAnimation
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.toValue = @(M_PI * 2);
    animation.duration = 1.0f;
    animation.autoreverses = NO;
    animation.repeatCount = MAXFLOAT;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    return animation;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _shapeLayer.frame = self.bounds;
    _gradientLayer.frame = self.bounds;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);

    CGFloat radius = MIN(width, height) / 2 - 4.0f;
    _shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width / 2, height / 2) radius:radius startAngle:0 endAngle:M_PI * 2 clockwise:NO].CGPath;
}

@end
