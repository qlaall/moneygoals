//
//  YYBDateView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/11/1.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "YYBCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBDateView : UIView

@property (nonatomic,strong) UIDatePicker *datePicker;
@property (nonatomic,strong) UIButton *cancelButton, *submitButton;

@property (nonatomic, copy) void (^ cancelTapedBlock)(void);
@property (nonatomic, copy) void (^ confirmTapedBlock)(NSDate *date);

@end

NS_ASSUME_NONNULL_END
