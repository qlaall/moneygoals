//
//  YYBMultiScrollContainersView.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/13.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBMultiScrollContainersView.h"

@interface YYBMultiScrollContainersView () <YYBMultiScrollContainerViewDelegate>
@property (nonatomic, strong) NSMutableDictionary *containerViews;

@end

@implementation YYBMultiScrollContainersView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.containerViews = [NSMutableDictionary new];
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    for (UIView *subView in self.subviews) {
        subView.frame = self.bounds;
    }
}

- (void)refreshScrollContainers
{
    self.containerViews = [[NSMutableDictionary alloc] init];
    
    while (self.subviews.count) {
        [self.subviews.lastObject removeFromSuperview];
    }
    
    NSInteger selectedIndex = [self selectedIndex];
    [self createContainerViewWithIndex:selectedIndex];
    if (selectedIndex != 0) {
        [self scrollContainerToIndex:selectedIndex];
    }
}

- (void)refreshContainer:(NSInteger)containerIndex containerItemIndex:(NSInteger)itemIndex scroll:(BOOL)scroll
{
    NSInteger count = [self.delegate numbersOfContainersInContainersView:self];
    
    if (containerIndex < count) {
        YYBMultiScrollContainerView *view = [self getContainerViewWithIndex:containerIndex];
        if (!view) {
            view = [self createContainerViewWithIndex:containerIndex];
        }
        
        [view refreshContainerViewAtIndex:itemIndex];
        
        if (scroll) {
            [view containerViewScrollToItemIndex:itemIndex];
        }
    }
}

- (void)scrollContainerToIndex:(NSInteger)index
{
    NSInteger count = [self.delegate numbersOfContainersInContainersView:self];
    
    if (index < count) {
        YYBMultiScrollContainerView *view = [self getContainerViewWithIndex:index];
        if (!view) {
            view = [self createContainerViewWithIndex:index];
        }
        
        NSArray *keys = _containerViews.allKeys;
        for (NSInteger i = 0; i < keys.count; i ++) {
            NSString *key = [keys objectAtIndex:i];
            YYBMultiScrollContainerView *view = [_containerViews objectForKey:key];
            view.hidden = key.integerValue != index;
        }
    }
}


- (YYBMultiScrollContainerView *)containerViewAtIndex:(NSInteger)containerIndex
{
    YYBMultiScrollContainerView *containerView = nil;
    NSString *key = @(containerIndex).stringValue;
    
    if ([self.containerViews.allKeys containsObject:key]) {
        containerView = [self.containerViews objectForKey:key];
    }
    
    return containerView;
}

// 获取当前选中的index
- (NSInteger)selectedIndex {
    NSInteger selectedIndex = 0;
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedContainerIndexAtContainersView:)]) {
        selectedIndex = [self.delegate selectedContainerIndexAtContainersView:self];
    }
    return selectedIndex;
}

- (YYBMultiScrollContainerView *)currentContainerView
{
    return [self containerViewAtIndex:[self selectedIndex]];
}

- (YYBMultiScrollContainerView *)createContainerViewWithIndex:(NSInteger)indexKey
{
    NSString *key = @(indexKey).stringValue;
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    CGRect rect = CGRectMake(0, 0, width, height);
    
    YYBMultiScrollContainerView *view = [[YYBMultiScrollContainerView alloc] init];
    view.frame = rect;
    view.delegate = self;

    [self addSubview:view];
    [self.containerViews setValue:view forKey:key];
    
    // 初始化选中的tableView
    NSInteger selectedItemIndex = 0;
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedItemIndexAtContainersView:containerIndex:)]) {
        selectedItemIndex = [self.delegate selectedItemIndexAtContainersView:self containerIndex:indexKey];
    }
    
    view.initialSection = selectedItemIndex;
    view.itemIndex = selectedItemIndex;
    
    [view refreshScrollContainerView];
    
    return view;
}

- (YYBMultiScrollContainerView *)getContainerViewWithIndex:(NSInteger)indexKey
{
    NSString *key = @(indexKey).stringValue;
    if ([self.containerViews.allKeys containsObject:key]) {
        return [self.containerViews objectForKey:key];
    }
    return nil;
}

- (void)refreshAndScrollContainerViewAtItemIndex:(NSInteger)itemIndex
{
    YYBMultiScrollContainerView *view = [self currentContainerView];
    if (view) {
        [view containerViewScrollToItemIndex:itemIndex];
        [view refreshContainerViewAtIndex:itemIndex];
    }
}

// ============================ 代理
// 获取container的key
- (NSInteger)indexForContainer:(YYBMultiScrollContainerView *)container
{
    if (container) {
        for (NSString *key in self.containerViews.allKeys) {
            YYBMultiScrollContainerView *targetView = [self.containerViews objectForKey:key];
            if (container == targetView) {
                return key.integerValue;
            }
        }
    }
    return 0;
}

///  每个containerView中的section数目
- (NSInteger)numbersOfSectionInContainerView:(YYBMultiScrollContainerView *)containerView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(numbersOfSectionInContainersView:containerIndex:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        return [self.delegate numbersOfSectionInContainersView:self containerIndex:containerIndex];
    }
    return 0;
}

- (void)configureTableViewInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(configureTableViewInContainersView:containerIndex:tableView:itemIndex:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        [self.delegate configureTableViewInContainersView:self containerIndex:containerIndex tableView:tableView itemIndex:itemIndex];
    }
}

- (void)containerView:(YYBMultiScrollContainerView *)containerView scrollToItemAtIndex:(NSInteger)itemIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(containersView:containerIndex:scrollToItemAtIndex:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        [self.delegate containersView:self containerIndex:containerIndex scrollToItemAtIndex:itemIndex];
    }
}

- (NSInteger)numbersOfSectionsInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(numbersOfSectionInContainersView:containerIndex:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        return [self.delegate numbersOfSectionsInContainersView:self containerIndex:containerIndex tableView:tableView itemIndex:itemIndex];
    }
    return 0;
}

- (NSInteger)numbersOfRowsInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex section:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(numbersOfRowsInContainersView:containerIndex:tableView:itemIndex:section:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        return [self.delegate numbersOfRowsInContainersView:self containerIndex:containerIndex tableView:tableView itemIndex:itemIndex section:section];
    }
    return 0;
}

- (CGFloat)heightForRowsInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(heightForRowsInContainersView:containerIndex:tableView:itemIndex:indexPath:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        return [self.delegate heightForRowsInContainersView:self containerIndex:containerIndex tableView:tableView itemIndex:itemIndex indexPath:indexPath];
    }
    return 0;
}

- (UITableViewCell *)cellForRowInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cellForRowInContainersView:containerIndex:tableView:itemIndex:indexPath:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        return [self.delegate cellForRowInContainersView:self containerIndex:containerIndex tableView:tableView itemIndex:itemIndex indexPath:indexPath];
    }
    return nil;
}

- (void)didSelectedRowInContainerView:(YYBMultiScrollContainerView *)containerView tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedRowInContainersView:containerIndex:tableView:itemIndex:indexPath:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        [self.delegate didSelectedRowInContainersView:self containerIndex:containerIndex tableView:tableView itemIndex:itemIndex indexPath:indexPath];
    }
}

- (void)containerView:(YYBMultiScrollContainerView *)containerView tableViewDidScrollWithOffset:(CGFloat)offset itemIndex:(NSInteger)itemIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(containersView:containerIndex:tableViewDidScrollWithOffset:itemIndex:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        [self.delegate containersView:self containerIndex:containerIndex tableViewDidScrollWithOffset:offset itemIndex:itemIndex];
    }
}

- (void)containerView:(YYBMultiScrollContainerView *)containerView didEndScrollAnimationAtItemIndex:(NSInteger)itemIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(containersView:containerIndex:didEndScrollAnimationAtItemIndex:)]) {
        NSInteger containerIndex = [self indexForContainer:containerView];
        [self.delegate containersView:self containerIndex:containerIndex didEndScrollAnimationAtItemIndex:itemIndex];
    }
}

@end
