//
//  YYBTabBar.h
//  YYBBaseViews
//
//  Created by alchemy on 2019/1/2.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBCategory.h"
#import "YYBNavigationBarContainer.h"
#import "YYBNavigationBarLabel.h"
#import "YYBNavigationBarButton.h"
#import "YYBNavigationBarImageView.h"
#import "YYBNavigationBarControl.h"
#import "YYBNavigationBarContainer+Create.h"

NS_ASSUME_NONNULL_BEGIN

@class YYBTabBar;

@protocol YYBTabBarDelegate <NSObject>

@optional

- (NSInteger)numbersOfContainersInTabBar:(YYBTabBar *)tabBar;
- (YYBNavigationBarContainer *)tabBarContainerInTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex;

- (void)didSelectedContainerInTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex;
- (NSInteger)firstSelectedContainerIndex;

- (BOOL)containerCouldSelectedWithTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex;

// 自定义container的宽度，默认为frame width / count
- (CGSize)containerSizeWithComponentIndex:(NSInteger)componentIndex;

// 是否可以相应hittest事件 页面push后，也可以在这个位置点击containr，并且做出相应的动作 需要杜绝
// 默认为NO
- (BOOL)shouldHandleHitTestWithTabBar:(YYBTabBar *)tabBar;
- (UIBlurEffect *)customBlurEffect;

@end

@interface YYBTabBar : UITabBar

@property (nonatomic,weak) id<YYBTabBarDelegate> tabBarDelegate;

// contentView 距离底部的间距
// 比如X则要设置为safeareabottom
@property (nonatomic) CGFloat containerOffset;

@property (nonatomic, strong) UIImageView *contentBackgroundView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIView *shadowView;

@property (nonatomic, strong) UIImageView *componentsBackgroundView;
@property (nonatomic, strong) UIView *componentsView;

@property (nonatomic, strong) UIView *separateView;
// 默认隐藏 hidden = YES
@property (nonatomic, strong) UIVisualEffectView *visualEffectView;

- (void)refreshTabBarContainers;
- (void)selectComponentAtComponentIndex:(NSInteger)componentIndex;

@end

NS_ASSUME_NONNULL_END
