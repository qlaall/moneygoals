//
//  YYBAlertView.m
//  YYBAlertView
//
//  Created by alchemy on 2018/8/30.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBAlertView.h"

#define _UIKeyboardFrameEndUserInfoKey (&UIKeyboardFrameEndUserInfoKey != NULL ? UIKeyboardFrameEndUserInfoKey : @"UIKeyboardBoundsUserInfoKey")

@interface YYBAlertView ()

@end

@implementation YYBAlertView
{
    NSTimer *_autoHideTimer;
    UITapGestureRecognizer *_taped;
}

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _isResizeContainer = NO;
    _isAutoManageKeyboardOffset = YES;
    _isEnableKeyboardNotification = YES;
    _isDismissWhenOccurTapAction = YES;
    _isResizeContentViewForContainer = YES;
    
    _containers = [NSMutableArray new];
    
    _backgroundView = [UIImageView new];
    _backgroundView.userInteractionEnabled = YES;
    [self addSubview:_backgroundView];
    
    _contentView = [UIView new];
    [self addSubview:_contentView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_keyBoardWillChangeFrameBlock:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_actionBlock:) name:@"YYBALERTVIEW_ACTION_NOTIFICATION" object:nil];
    
    _taped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapedOnAction)];
    [_backgroundView addGestureRecognizer:_taped];
    
    return self;
}

- (void)resignKeyboardResponder
{
    YYBAlertViewContainer *container = [_containers objectAtIndex:_visibleContainerIndex];
    UIView *firstResponder = [container keyboardResponder];
    if (firstResponder)
    {
        [firstResponder resignFirstResponder];
    }
}

- (void)removeContentTapedBlock
{
    [_contentView removeGestureRecognizer:_taped];
}

- (void)contentViewTapedOnAction
{
    YYBAlertViewContainer *container = [_containers objectAtIndex:_visibleContainerIndex];
    UIView *firstResponder = [container keyboardResponder];
    if (firstResponder)
    {
        [firstResponder resignFirstResponder];
    }
    else
    {
        if (self.isDismissWhenOccurTapAction)
        {
            [self closeAlertView];
        }
    }
}

- (void)_actionBlock:(NSNotification *)noti
{
    if ([noti.userInfo.allKeys containsObject:@"hash"])
    {
        NSUInteger hash = [[noti.userInfo objectForKey:@"hash"] integerValue];
        for (YYBAlertViewContainer *container in _containers)
        {
            if (container.hash == hash)
            {
                [self _closeContainerAtIndex:[_containers indexOfObject:container]];
            }
        }
    }
}

- (void)_keyBoardWillChangeFrameBlock:(NSNotification *)noti
{
    if (_containers.count > _visibleContainerIndex)
    {
        YYBAlertViewContainer *container = [_containers objectAtIndex:_visibleContainerIndex];
        UIView *firstResponder = [container keyboardResponder];
        if (_isEnableKeyboardNotification)
        {
            CGRect keyboardRect = [[noti.userInfo objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue];
            CGFloat keyboardy = CGRectGetMinY(keyboardRect);
            
            if (keyboardRect.origin.y == [UIScreen mainScreen].bounds.size.height)
            {
                container.transform = CGAffineTransformIdentity;
                [firstResponder resignFirstResponder];
            }
            else
            {
                if (_isManageOffsetWithContainter == NO)
                {
                    CGFloat offset = 0;
                    if (_isAutoManageKeyboardOffset == NO)
                    {
                        if (self.offsetOfContainerToKeyboard)
                        {
                            offset = self.offsetOfContainerToKeyboard(_visibleContainerIndex);
                        }
                    }
                    
                    // 计算container上的firstResponder相对于self的位置
                    CGRect responderRect = [self rectForResponder:firstResponder responderRect:firstResponder.bounds superView:firstResponder.superview];
                    // 在屏幕上的firstResponder最大高度
                    CGFloat responderMaxy = CGRectGetMaxY(responderRect) - container.scrollView.contentOffset.y;
                    
                    if (responderMaxy > keyboardy)
                    {
                        CGFloat y = keyboardy - CGRectGetHeight(responderRect);
                        CGFloat ty = y - (responderMaxy - CGRectGetHeight(responderRect)) + container.transform.ty;
                        container.transform = CGAffineTransformMakeTranslation(0, ty - offset);
                    }
                }
                else
                {
                    CGFloat offset = 0;
                    if (self.offsetOfContainerToKeyboard)
                    {
                        offset = self.offsetOfContainerToKeyboard(_visibleContainerIndex);
                    }
                    
//                    CGRect containerRect = [self rectForResponder:container.contentView responderRect:container.contentView.bounds superView:container.contentView.superview];
                    
                    UIView *keyWindow = [UIApplication sharedApplication].keyWindow;
                    CGRect containerRect = [container.contentView convertRect:container.contentView.bounds toView:keyWindow];
                    CGFloat containerMaxy = CGRectGetMaxY(containerRect);
                    CGFloat ty = container.transform.ty - (containerMaxy - (keyboardy - offset));
                    container.transform = CGAffineTransformMakeTranslation(0, ty);
                }
            }
        }
    }
}

- (CGRect)rectForResponder:(UIView *)responder responderRect:(CGRect)responderRect superView:(UIView *)superView
{
    if (superView)
    {
        CGRect rect_reponder = [superView convertRect:responder.frame fromView:superView];
        if (CGRectEqualToRect(responderRect, CGRectZero))
        {
            responderRect = rect_reponder;
        }
        else
        {
            responderRect = CGRectMake(CGRectGetMinX(rect_reponder) + CGRectGetMinX(responderRect),
                                       CGRectGetMinY(rect_reponder) + CGRectGetMinY(responderRect),
                                       responderRect.size.width, responderRect.size.height);
        }
        
        UIView *super_superView = superView.superview;
        if (super_superView != self.contentView.superview)
        {
            CGRect rect = [self rectForResponder:superView responderRect:responderRect superView:super_superView];
            return rect;
        }
        else
        {
            return responderRect;
        }
    }
    else
    {
        return responderRect;
    }
}

- (void)_layoutAlertViewContainers
{
    _contentView.frame = self.bounds;
    _backgroundView.frame = self.bounds;
    
    if (_containers.count == 1 && _isResizeContainer)
    {
        YYBAlertViewContainer *container = [_containers objectAtIndex:0];
        container.frame = _contentView.bounds;
        [_contentView addSubview:container];
        
        CGSize containerSize = container.contentSizeWithActions;
        CGRect contentRect = CGRectMake((CGRectGetMinX(self.frame) + (CGRectGetWidth(self.frame) - containerSize.width) / 2),
                                        (CGRectGetMinY(self.frame) + (CGRectGetHeight(self.frame) - containerSize.height) / 2),
                                        containerSize.width, containerSize.height);

        self.frame = contentRect;
        _contentView.frame = self.bounds;
        _backgroundView.frame = self.bounds;
        container.frame = self.bounds;
        
        if (container.rectCornerRadius != 0)
        {
            UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:container.bounds byRoundingCorners:container.rectCorner cornerRadii:CGSizeMake(container.rectCornerRadius, container.rectCornerRadius)];
            
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            shapeLayer.frame = container.bounds;
            shapeLayer.path = bezierPath.CGPath;
            
            container.layer.mask = shapeLayer;
        }
        
        [container createActionViews];
    }
    else
    {
        if (_containers.count > 0 || (_containers.count == 1 && _isResizeContentViewForContainer))
        {
            for (NSInteger idx = 0; idx < _containers.count; idx ++)
            {
                YYBAlertViewContainer *container = [_containers objectAtIndex:idx];
                CGRect rect = CGRectZero;
                if (self.delegate && [self.delegate respondsToSelector:@selector(alertView:containerRectsWithIndex:container:)])
                {
                    rect = [self.delegate alertView:self containerRectsWithIndex:idx container:container];
                }
                else if (self.createRectBlock)
                {
                    rect = self.createRectBlock(idx,container);
                }
                
                if (CGRectEqualToRect(CGRectZero, rect))
                {
                    if (idx > 0)
                    {
                        rect = CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
                    }
                    else
                    {
                        rect = self.bounds;
                    }
                }
                
                if (container)
                {
                    if (_containers.count == 1 && _isResizeContentViewForContainer)
                    {
                        _contentView.frame = rect;
                        container.frame = _contentView.bounds;
                    }
                    else
                    {
                        container.frame = rect;
                    }
                    
                    [_contentView addSubview:container];
                    
                    if (container.rectCornerRadius != 0)
                    {
                        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:container.bounds byRoundingCorners:container.rectCorner cornerRadii:CGSizeMake(container.rectCornerRadius, container.rectCornerRadius)];
                        
                        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
                        shapeLayer.frame = container.bounds;
                        shapeLayer.path = bezierPath.CGPath;
                        
                        container.layer.mask = shapeLayer;
                    }
                    
                    [container createActionViews];
                }
            }
        }
    }
}

- (void)showAlertViewOnKeyboardWindow
{
    Class UIApplicationClass = NSClassFromString(@"UIApplication");
    BOOL hasApplication = UIApplicationClass && [UIApplicationClass respondsToSelector:@selector(sharedApplication)];
    if (hasApplication) {
        UIApplication * app = [UIApplicationClass performSelector:@selector(sharedApplication)];

        UIView *showView = nil;
        for (UIView *window in app.windows)
        {
            if( [window isKindOfClass:NSClassFromString(@"UIRemoteKeyboardWindow")] )
            {
                showView = window;
                break;
            }
        }
        
        if (showView == nil)
        {
            showView = app.keyWindow;
        }
        
        [showView addSubview:self];
        self.frame = showView.bounds;
        [self showAlertView];
    }
}

- (void)showAlertViewOnKeyWindow
{
    NSArray *windows = [UIApplication sharedApplication].windows;
    for (UIWindow *window in windows)
    {
        if (window.windowLevel == UIWindowLevelNormal && window.isKeyWindow)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [window addSubview:self];
                self.frame = window.bounds;
                [self showAlertView];
            });
            break;
        }
    }
}

- (void)showAlertView
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    switch (_animationStyle)
    {
        case YYBAlertViewAnimationStyleNone:
        {
            
        }
            break;
        case YYBAlertViewAnimationStyleCenter:
        {
            self.createRectBlock = ^CGRect(NSInteger index, YYBAlertViewContainer *container) {
                CGSize contentSize = container.contentSizeWithActions;
                return CGRectMake((width - contentSize.width) / 2, (height - contentSize.height) / 2, contentSize.width, contentSize.height);
            };
            
            self.showContainerBlock = ^BOOL(NSInteger index, YYBAlertViewContainer *container) {
                container.alpha = 0;
                [UIView animateWithDuration:0.1 animations:^{
                    container.alpha = 1;
                }];
                
                return YES;
            };
            
            self.closeContainerBlock = ^BOOL(NSInteger index, YYBAlertViewContainer *container, void (^removeSubviewsBlock)(void)) {
                [UIView animateWithDuration:0.1 animations:^{
                    container.alpha = 0;
                } completion:^(BOOL finished) {
                    removeSubviewsBlock();
                }];
                
                return YES;
            };
        }
            break;
        case YYBAlertViewAnimationStyleBottom:
        {
            self.createRectBlock = ^CGRect(NSInteger index, YYBAlertViewContainer *container) {
                CGSize contentSize = container.contentSizeWithActions;
                return CGRectMake((width - contentSize.width) / 2, height - contentSize.height, contentSize.width, contentSize.height);
            };
            
            __weak typeof(self) wself = self;
            self.showContainerBlock = ^BOOL(NSInteger index, YYBAlertViewContainer *container) {
                container.transform = CGAffineTransformMakeTranslation(0, container.contentSizeWithActions.height);
                wself.backgroundView.alpha = 0;
                
                [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    container.transform = CGAffineTransformIdentity;
                    wself.backgroundView.alpha = 1;
                } completion:nil];
                
                return YES;
            };
            
            self.closeContainerBlock = ^BOOL(NSInteger index, YYBAlertViewContainer *container, void (^removeSubviewsBlock)(void)) {
                wself.backgroundView.alpha = 1;
                [UIView animateWithDuration:0.2f animations:^{
                    container.transform = CGAffineTransformMakeTranslation(0, container.contentSizeWithActions.height);
                    wself.backgroundView.alpha = 0;
                } completion:^(BOOL finished) {
                    removeSubviewsBlock();
                }];
                
                return YES;
            };
            
        }
            break;
        case YYBAlertViewAnimationStyleCenterShrink:
        {
            self.createRectBlock = ^CGRect(NSInteger index, YYBAlertViewContainer *container) {
                CGSize contentSize = container.contentSizeWithActions;
                return CGRectMake((width - contentSize.width) / 2, (height - contentSize.height) / 2, contentSize.width, contentSize.height);
            };
            
            __weak typeof(self) wself = self;
            
            self.showContainerBlock = ^BOOL(NSInteger index, YYBAlertViewContainer *container) {
                container.alpha = 0;
                container.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
                wself.backgroundView.alpha = 0;
                [UIView animateWithDuration:0.2f animations:^{
                    container.alpha = 1;
                    container.transform = CGAffineTransformIdentity;
                    wself.backgroundView.alpha = 1;
                }];
                
                return YES;
            };
            
            self.closeContainerBlock = ^BOOL(NSInteger index, YYBAlertViewContainer *container, void (^removeSubviewsBlock)(void)) {
                [UIView animateWithDuration:0.2f animations:^{
                    container.alpha = 0;
                    container.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
                    wself.backgroundView.alpha = 0;
                } completion:^(BOOL finished) {
                    removeSubviewsBlock();
                }];
                
                return YES;
            };
        }
            break;
        default:
            break;
    }
    
    [self _layoutAlertViewContainers];
    [self showContainerAtIndex:0];
    [self _setupAutoHideAction];
}

- (void)showContainerAtIndex:(NSInteger)index;
{
    if (_containers.count > index)
    {
        YYBAlertViewContainer *container = [_containers objectAtIndex:index];
        BOOL isAnimated = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(alertView:showContainerAtIndex:container:)])
        {
            isAnimated = [self.delegate alertView:self showContainerAtIndex:index container:container];
        }
        else if (self.showContainerBlock)
        {
            isAnimated = self.showContainerBlock(index,container);
        }
        
        // 默认的视图加载方式
        if (isAnimated == NO)
        {
            [UIView animateWithDuration:0.25f animations:^{
                if (index == 0)
                {
                    container.transform = CGAffineTransformIdentity;
                }
                else
                {
                    container.transform = CGAffineTransformMakeTranslation(0, - CGRectGetHeight(container.frame));
                }
            }];
        }
        
        _visibleContainerIndex = index;
    }
}

- (void)_closeContainerAtIndex:(NSInteger)index
{
    if (_containers.count > index)
    {
        YYBAlertViewContainer *container = [_containers objectAtIndex:index];
        BOOL isAnimated = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(alertView:closeContainerWithIndex:container:removeSubviewsBlock:)])
        {
            __weak typeof(self) weakself = self;
            isAnimated = [self.delegate alertView:self closeContainerWithIndex:index container:container removeSubviewsBlock:^{
                [weakself _removeContainerSubview];
            }];
        }
        else if (self.closeContainerBlock)
        {
            __weak typeof(self) weakself = self;
            isAnimated = self.closeContainerBlock(index,container,^{
                [weakself _removeContainerSubview];
            });
        }
        
        // 默认的视图移除方式
        if (isAnimated == NO)
        {
            [UIView animateWithDuration:0.25f animations:^{
                container.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                [self _removeContainerSubview];
            }];
        }
    }
}

- (void)_removeContainerSubview
{
    [_containers removeAllObjects];
    [self removeSubViews];
    [self removeFromSuperview];
}

- (void)closeContainerAtIndex:(NSInteger)index
{
    if (_containers.count > 1)
    {
        NSInteger idx = index;
        if (idx > 1)
        {
            idx --;
        }
        else
        {
            idx = 0;
        }
        
        [self _closeContainerAtIndex:index];
        [self showContainerAtIndex:idx];
    }
    else
    {
        [self closeAlertView];
    }
}

- (void)addContainerViewWithBlock:(void (^)(YYBAlertViewContainer *))block
{
    if (block)
    {
        YYBAlertViewContainer *container = [[YYBAlertViewContainer alloc] initWithFlexDirection:YYBAlertViewFlexDirectionVertical];
        block(container);
        
        [_containers addObject:container];
    }
}

- (void)_setupAutoHideAction
{
    if (_autoHideTimeInterval == 0 || _autoHideTimeInterval == MAXFLOAT)
    {
        return;
    }
    _autoHideTimer = [NSTimer scheduledTimerWithTimeInterval:_autoHideTimeInterval
                                                      target:self
                                                    selector:@selector(autoHideTimerAction)
                                                    userInfo:nil
                                                     repeats:NO];
}

- (void)autoHideTimerAction
{
    [self _closeContainerAtIndex:_visibleContainerIndex];
}

- (void)closeAlertView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(alertView:closeAllContainersWithRemoveSubviewsBlock:)])
    {
        [self.delegate alertView:self closeAllContainersWithRemoveSubviewsBlock:^{
            [self _removeContainerSubview];
        }];
    }
    else if (self.closeAllContainersBlock)
    {
        __weak typeof(self) wself = self;
        self.closeAllContainersBlock(^{
            [wself _removeContainerSubview];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _closeContainerAtIndex:self.visibleContainerIndex];
        });
    }
}

// 协议中的Color
- (UIColor *)cancelTintColor
{
    return [UIColor colorWithHexValue:0x929aab];
}

- (UIColor *)renderTintColor
{
    return [UIColor colorWithHexValue:0x51CCFF];
}

- (UIColor *)backgroundViewColor
{
    return [UIColor colorWithHexValue:0x000000 alpha:0.6f];
}

- (UIColor *)containerViewBackgroundColor
{
    return [UIColor colorWithHexValue:0xF5F6F9];
}

- (UIColor *)mainTextColor
{
    return [UIColor colorWithHexValue:0x222831];
}

- (UIColor *)secondTextColor
{
    return [UIColor colorWithHexValue:0x393e46];
}

- (UIColor *)thirdTextColor
{
    return [UIColor colorWithHexValue:0x9DA2A9];
}

- (UIColor *)inputTextColor
{
    return [UIColor colorWithHexValue:0x2A2B31];
}

- (UIImage *)normalStateBackgroundImage
{
    return [UIColor whiteColor].colorToUIImage;
}

- (UIImage *)highlightStateBackgroundImage
{
    return [UIColor colorWithHexValue:0xeeeeee].colorToUIImage;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (CGFloat)commonCornerRadius
{
    return 16;
}

@end
