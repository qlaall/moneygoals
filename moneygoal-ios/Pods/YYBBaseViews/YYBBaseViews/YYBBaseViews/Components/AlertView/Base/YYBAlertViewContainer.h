//
//  YYBAlertViewContainer.h
//  YYBAlertView
//
//  Created by alchemy on 2018/8/30.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBAlertViewAction.h"
#import "YYBAlertViewTypedef.h"

@interface YYBAlertViewContainer : UIView

// 层级结构如下所示
@property (nonatomic, strong, readonly) UIView *shadowView;
@property (nonatomic, strong, readonly) UIImageView *contentView;
@property (nonatomic, strong, readonly) UIScrollView *scrollView;

@property (nonatomic, strong, readonly) NSMutableArray *innerViews, *innerActions;

@property (nonatomic) BOOL scrollViewScrollable;

@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, assign) CGSize contentSizeWithActions;
@property (nonatomic, assign) UIEdgeInsets padding;
@property (nonatomic, assign) UIEdgeInsets margin;

@property (nonatomic, assign) CGFloat minimalWidth;
@property (nonatomic, assign) CGFloat maximalWidth;
@property (nonatomic, assign) CGFloat minimalHeight;
@property (nonatomic, assign) CGFloat maximalHeight;

// 设置特定圆角
@property (nonatomic) UIRectCorner rectCorner;
@property (nonatomic) CGFloat rectCornerRadius;

// 初始化方法 默认为纵向
// 子视图排列方式
- (YYBAlertViewContainer *)initWithFlexDirection:(YYBAlertViewFlexDirection)direction;

@property (nonatomic, assign) YYBAlertViewFlexDirection flexDirection;
@property (nonatomic, assign) YYBAlertViewFlexPosition flexPosition;

// 视图间距,仅当flexPosition为YYBAlertViewFlexPositionStretch时候有效
// 由于父视图为流式布局,该属性对顶级父视图无效,仅对当前视图的子container有效
@property (nonatomic, assign) CGFloat stretchValue;

// 存放响应按钮的视图
// 该属性中无对应的相同子视图,为nil
@property (nonatomic, strong, readonly) YYBAlertViewContainer *actionsContainer;
- (void)removeContainerContentTapedBlock;

// 键盘响应者
// 如果没有则为nil
- (UIView *)keyboardResponder;

// 添加输入视图
- (void)addLabelWithBlock:(void(^)(YYBAlertViewAction *action, UILabel *label))block;
// 添加一个图片视图
- (void)addIconViewWithBlock:(void(^)(YYBAlertViewAction *action, UIImageView *imageView))block;
// 添加一个按钮视图
- (void)addButtonWithBlock:(void(^)(YYBAlertViewAction *action, UIButton *button))block tapedOnBlock:(void(^)(void))tapedOnBlock;
// 添加一个输入框视图
- (void)addTextFieldWithBlock:(void(^)(YYBAlertViewAction *action, UITextField *textField))block editingChangedBlock:(void(^)(NSString *string))editBlock;
// 添加一个输入框视图
- (void)addTextViewWithBlock:(void(^)(YYBAlertViewAction *action, YYBPlaceholderTextView *textView))block editingChangedBlock:(void(^)(NSString *string))editBlock;
// 添加一个自定义视图
- (void)addCustomView:(UIView *)customView configureBlock:(void(^)(YYBAlertViewAction *action, UIView *view))block;
// 添加选项按钮
- (void)addActionWithBlock:(void(^)(YYBAlertViewAction *action, UIButton *button))block tapedOnBlock:(void(^)(NSInteger index))tapedOnBlock;
// 添加等待视图
- (void)addActivityIndicatorWithBlock:(void(^)(YYBAlertViewAction *action, UIActivityIndicatorView *view))block;
// 添加一个子单元元素
- (void)addContainerViewWithBlock:(void(^)(YYBAlertViewContainer *container))block;

- (void)createActionViews;

@end
