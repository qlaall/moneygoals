//
//  UIView+Responder.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/23.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Responder)

- (UIView *)searchKeyboardResponder;
- (void)removeSubViews;

@end
