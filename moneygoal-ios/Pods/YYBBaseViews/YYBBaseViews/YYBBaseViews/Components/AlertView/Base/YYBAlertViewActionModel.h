//
//  YYBAlertViewActionModel.h
//  YYBBaseViews
//
//  Created by yyb on 2022/8/17.
//  Copyright © 2022 Univease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYBCategory.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YYBAlertViewActionType)
{
    YYBAlertViewActionTypeNormal,
    YYBAlertViewActionTypeWarning
};

@interface YYBAlertViewActionModel : NSObject

@property (nonatomic, copy) NSString *actionTitle;
@property (nonatomic) YYBAlertViewActionType actionType;

- (UIColor *)getActionTypeColor;

@end

NS_ASSUME_NONNULL_END
