//
//  YYBAlertView+YYBCommon.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBAlertView+YYBCommon.h"
#import "YYBDateView.h"
#import "YYBRotateLoadingView.h"
#import "YYBAlertViewActionModel.h"

@implementation YYBAlertView (YYBCommon)

+ (void)showAlertViewWithActionModels:(NSArray *)actionModels title:(nullable NSString *)title actionTapedBlock:(nonnull YYBAlertViewTapedBlock)actionTapedBlock
{
    NSArray *actionModels2 = [actionModels map:^id(id obj, NSInteger index) {
        if (![obj isKindOfClass:[YYBAlertViewActionModel class]]) {
            if ([obj isKindOfClass:[NSString class]]) {
                YYBAlertViewActionModel *actionModel = [[YYBAlertViewActionModel alloc] init];
                actionModel.actionTitle = obj;
                actionModel.actionType = YYBAlertViewActionTypeNormal;
                return actionModel;
            } else {
                return [YYBAlertViewActionModel new];
            }
        } else {
            return obj;
        }
    }];
    
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.backgroundView.backgroundColor =  [alertView backgroundViewColor];
    alertView.animationStyle = YYBAlertViewAnimationStyleBottom;
    
    __weak typeof(alertView) weakAlertView = alertView;
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        container.maximalHeight = 400;
        
        container.backgroundColor = [UIColor whiteColor];
        container.rectCornerRadius = [weakAlertView commonCornerRadius];
        container.rectCorner = UIRectCornerTopLeft | UIRectCornerTopRight;
        
        if (title) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.size = CGSizeMake([UIScreen mainScreen].bounds.size.width, 50);
                label.textColor = [weakAlertView thirdTextColor];
                label.text = title;
                label.font = YYBFONT_NORMAL(14);
                label.backgroundColor = [UIColor whiteColor];
            }];
        }
        
        for (NSInteger index = 0; index < actionModels2.count; index ++) {
            YYBAlertViewActionModel *dataModel = actionModels2[index];
            
            [container addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.size = CGSizeMake(0, 60);
                
                button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
                [button setBackgroundImage:YYBCOLOR(0xF7F7F7).colorToUIImage forState:0];
                [button setBackgroundImage:[YYBCOLOR(0xF7F7F7) colorWithAlphaComponent:0.8].colorToUIImage forState:UIControlStateHighlighted];
                button.titleLabel.font = YYBFONT_NORMAL(16);
                [button setTitle:dataModel.actionTitle forState:0];
                [button setTitleColor:[dataModel getActionTypeColor] forState:0];
            } tapedOnBlock:^{
                if (actionTapedBlock) {
                    [weakAlertView closeAlertView];
                    actionTapedBlock(index);
                }
            }];
            
            if (index != actionModels2.count - 1) {
                [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
                    action.size = CGSizeMake(0, 0.5);
                    view.backgroundColor = [UIColor colorWithHexValue:0xDDDDDD alpha:0.6];
                }];
            }
        }
        
        [container addActionWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
            action.margin = UIEdgeInsetsMake(0, 20, 0, 20);
            action.size = CGSizeMake(0, 60);
            
            button.titleLabel.font = YYBFONT_NORMAL(16);
            [button setTitle:@"取消" forState:0];
            [button setTitleColor:[UIColor colorWithHexValue:0x292C35] forState:0];
            [button setBackgroundImage:[UIColor whiteColor].colorToUIImage forState:0];
            [button setBackgroundImage:[[UIColor whiteColor] colorWithAlphaComponent:0.4].colorToUIImage forState:UIControlStateHighlighted];
            [button cornerRadius:25];
        } tapedOnBlock:^(NSInteger index) {
            [weakAlertView closeAlertView];
        }];
        
        [container addActionWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
            action.size = CGSizeMake(0, [UIDevice safeAreaInsetsBottom]);
            button.backgroundColor = [UIColor whiteColor];
        } tapedOnBlock:^(NSInteger index) {
            
        }];
    }];
    
    [alertView showAlertViewOnKeyWindow];
}

+ (void)showAlertViewWithTitle:(NSString *)title detail:(NSString *)detail
              firstActionTitle:(NSString *)firstActionTitle firstActionTapedBlock:(nullable YYBAlertViewBlock)firstActionTapedBlock
             secondActionTitle:(NSString *)secondActionTitle secondActionTapedBlock:(YYBAlertViewBlock)secondActionTapedBlock
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.backgroundView.backgroundColor = [alertView backgroundViewColor];
    alertView.animationStyle = YYBAlertViewAnimationStyleBottom;
    
    __weak typeof(alertView) weakAlertView = alertView;
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        container.maximalHeight = 400;
        
        container.backgroundColor = [UIColor colorWithHexValue:0xF7F7F7];
        container.rectCornerRadius = [weakAlertView commonCornerRadius];
        container.rectCorner = UIRectCornerTopLeft | UIRectCornerTopRight;
        
        [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
            action.margin = UIEdgeInsetsMake(40, 30, 0, 30);
            
            label.font = YYBFONT_NORMAL(18);
            label.textColor = [UIColor colorWithHexValue:0x292C35];
            label.attributedText = [NSMutableAttributedString createLineSpacingString:title spacingValue:4.0f alignment:NSTextAlignmentLeft];
        }];
        
        [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
            CGFloat top = title.isExist ? 10 : 0;
            action.margin = UIEdgeInsetsMake(top, 30, 40, 30);
            
            label.font = YYBFONT_NORMAL(14);
            label.textColor = [UIColor colorWithHexValue:0x9D9CA2];
            label.attributedText = [NSMutableAttributedString createLineSpacingString:detail spacingValue:4 alignment:NSTextAlignmentLeft];
        }];
        
        [container addContainerViewWithBlock:^(YYBAlertViewContainer *actionsContainer) {
            actionsContainer.flexPosition = YYBAlertViewFlexPositionStretch;
            actionsContainer.flexDirection = YYBAlertViewFlexDirectionHorizonal;
            actionsContainer.minimalWidth = [UIScreen mainScreen].bounds.size.width;
            actionsContainer.backgroundColor = [UIColor whiteColor];
            
            if (firstActionTitle) {
                [actionsContainer addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                    action.size = CGSizeMake(0, 50);
                    
                    [button setBackgroundImage:[UIColor whiteColor].colorToUIImage forState:0];
                    [button setBackgroundImage:[[UIColor whiteColor] colorWithAlphaComponent:0.4].colorToUIImage forState:UIControlStateHighlighted];
                    [button setTitle:firstActionTitle forState:0];
                    [button setTitleColor:[UIColor colorWithHexValue:0x292C35] forState:0];
                    [button setTitleColor:[UIColor colorWithHexValue:0xEFF1F4] forState:UIControlStateSelected];
                    button.titleLabel.font = YYBFONT_NORMAL(16);
                } tapedOnBlock:^{
                    [weakAlertView closeAlertView];
                    if (firstActionTapedBlock) {
                        firstActionTapedBlock();
                    }
                }];
            }
            
            if (firstActionTitle) {
                [actionsContainer addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
                    action.size = CGSizeMake(1, 20);
                    view.backgroundColor = [YYBCOLOR(0xDDDDDD) colorWithAlphaComponent:0.6];
                }];
            }
            
            if (secondActionTitle) {
                [actionsContainer addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                    action.size = CGSizeMake(0, 50);
                    
                    [button setBackgroundImage:[UIColor whiteColor].colorToUIImage forState:0];
                    [button setBackgroundImage:[[UIColor whiteColor] colorWithAlphaComponent:0.4].colorToUIImage forState:UIControlStateHighlighted];
                    [button setTitle:secondActionTitle forState:0];
                    [button setTitleColor:[UIColor colorWithHexValue:0x292C35] forState:0];
                    [button setTitleColor:[UIColor colorWithHexValue:0xEFF1F4] forState:UIControlStateSelected];
                    button.titleLabel.font = YYBFONT_NORMAL(16);
                } tapedOnBlock:^{
                    [weakAlertView closeAlertView];
                    if (secondActionTapedBlock) {
                        secondActionTapedBlock();
                    }
                }];
            }
        }];
        
        [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.size = CGSizeMake(0, [UIDevice safeAreaInsetsBottom]);
            view.backgroundColor = [UIColor whiteColor];
        }];
    }];
    
    [alertView showAlertViewOnKeyWindow];
}

+ (YYBAlertView *)showIndicatorAlertViewWithView:(UIView *)view backgroundColor:(UIColor *)backgroundColor
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    
    alertView.backgroundView.backgroundColor = backgroundColor;
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.animationStyle = YYBAlertViewAnimationStyleCenter;
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.rectCornerRadius = [weakAlertView commonCornerRadius];
        container.rectCorner = UIRectCornerAllCorners;
        
        [container addActivityIndicatorWithBlock:^(YYBAlertViewAction *action, UIActivityIndicatorView *view) {
            action.size = CGSizeMake(80, 80);
            
            view.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [view startAnimating];
        }];
    }];
    
    [view addSubview:alertView];
    alertView.frame = view.bounds;
    [alertView showAlertView];
    
    return alertView;
}

+ (YYBAlertView *)showQueryAlertView
{
    return [self showQueryStringAlertViewWithString:nil inView:nil isResizeContainer:YES];
}

+ (YYBAlertView *)showQueryAlertViewAtSuperView:(UIView *)superView
{
    return [self showQueryStringAlertViewWithString:nil inView:superView isResizeContainer:YES];
}

+ (YYBAlertView *)showQueryStringAlertView
{
    return [self showQueryStringAlertViewWithString:@"加载中" inView:nil isResizeContainer:NO];
}

+ (YYBAlertView *)showQueryStringAlertViewWithString:(nullable NSString *)text inView:(nullable UIView *)inView isResizeContainer:(BOOL)isResizeContainer
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.animationStyle = YYBAlertViewAnimationStyleCenter;
    alertView.isResizeContainer = isResizeContainer;
//    alertView.backgroundView.backgroundColor = [[alertView backgroundViewColor] colorWithAlphaComponent:0.5];
    
    YYBRotateLoadingView *custom = [[YYBRotateLoadingView alloc] init];
    custom.strokeColor = [UIColor whiteColor];
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.backgroundColor = [UIColor blackColor];
        container.rectCornerRadius = 10;
        container.rectCorner = UIRectCornerAllCorners;
        
        [container addCustomView:custom configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            if (text && [text isExist]) {
                action.margin = UIEdgeInsetsMake(20, 20, 0, 20);
                action.size = CGSizeMake(80, 40);
            } else {
                action.margin = UIEdgeInsetsMake(20, 20, 20, 20);
                action.size = CGSizeMake(40, 40);
            }
        }];
        
        if (text) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(15, 20, 20, 20);
                
                label.text = text;
                label.font = YYBFONT_NORMAL(16);
                label.textColor = [UIColor whiteColor];
            }];
        }
    }];
    
    if (inView) {
        [inView addSubview:alertView];
        alertView.frame = inView.bounds;
        [alertView showAlertView];
    } else {
        [alertView showAlertViewOnKeyWindow];
    }
    
    [custom beginAnimation];
    return alertView;
}

+ (void)showDatePickerAlertViewWithSelectedBlock:(void (^)(NSDate * _Nonnull))dateSelectedBlock
{
    [self showDatePickerAlertViewWithSelectedBlock:dateSelectedBlock mode:UIDatePickerModeTime date:[NSDate date]];
}

+ (void)showDatePickerAlertViewWithSelectedBlock:(void (^)(NSDate * _Nonnull))dateSelectedBlock mode:(UIDatePickerMode)mode date:(NSDate *)date
{
    [self showDatePickerAlertViewWithSelectedBlock:dateSelectedBlock date:date datePickerSetupBlock:^(UIDatePicker *datePicker) {
        datePicker.datePickerMode = mode;
    }];
}

+ (void)showDatePickerAlertViewWithSelectedBlock:(void (^)(NSDate * _Nonnull))dateSelectedBlock date:(NSDate *)date datePickerSetupBlock:(void (^)(UIDatePicker *datePicker))datePickerSetupBlock
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    
    alertView.backgroundView.backgroundColor = [alertView backgroundViewColor];
    alertView.animationStyle = YYBAlertViewAnimationStyleBottom;
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalWidth = [UIScreen mainScreen].bounds.size.width;
        container.backgroundColor = [UIColor whiteColor];
        
        container.rectCornerRadius = [weakAlertView commonCornerRadius];
        container.rectCorner = UIRectCornerTopLeft | UIRectCornerTopRight;
        
        YYBDateView *view = [[YYBDateView alloc] init];
        view.datePicker.date = date;
        [view.cancelButton setTitleColor:[weakAlertView cancelTintColor] forState:0];
        [view.submitButton setTitleColor:[UIColor blackColor] forState:0];
        
        view.confirmTapedBlock = ^(NSDate * _Nonnull date) {
            if (dateSelectedBlock) {
                dateSelectedBlock(date);
            }
            [weakAlertView closeAlertView];
        };
        
        view.cancelTapedBlock = ^{
            [weakAlertView closeAlertView];
        };
        
        if (datePickerSetupBlock) {
            datePickerSetupBlock(view.datePicker);
        }
        
        [container addCustomView:view configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.size = CGSizeMake([UIScreen mainScreen].bounds.size.width, 260);
        }];
        
        [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.margin = UIEdgeInsetsMake(0, 0, [UIDevice safeAreaInsetsBottom], 0);
        }];
    }];
    
    [alertView showAlertViewOnKeyWindow];
}

+ (void)showAlertViewWithStatusString:(NSString *)status
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    
    alertView.animationStyle = YYBAlertViewAnimationStyleCenterShrink;
    alertView.autoHideTimeInterval = 2;
    alertView.isResizeContainer = YES;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.backgroundColor = [UIColor blackColor];
        container.rectCornerRadius = 18;
        container.rectCorner = UIRectCornerAllCorners;
        
        [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
            action.margin = UIEdgeInsetsMake(10, 15, 10, 15);

            label.textColor = [UIColor whiteColor];
            label.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
            label.numberOfLines = 0;
            
            NSString *text = [status isExist] ? status : @"出错了";
            label.attributedText = [NSMutableAttributedString createLineSpacingString:text spacingValue:3 alignment:NSTextAlignmentCenter];
        }];
    }];
    
    [alertView showAlertViewOnKeyWindow];
}

+ (void)showSuccessStatusViewWithString:(NSString *)text duration:(NSTimeInterval)duration
{
    [self showStatusViewWithText:text icon:@"ic_status_success" duration:duration];
}

+ (void)showErrorStatusViewWithString:(NSString *)text duration:(NSTimeInterval)duration
{
    [self showStatusViewWithText:text icon:@"ic_status_error" duration:duration];
}

+ (void)showStatusViewWithText:(NSString *)text icon:(NSString *)icon duration:(NSTimeInterval)duration
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    
    alertView.animationStyle = YYBAlertViewAnimationStyleCenter;
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.isResizeContainer = YES;
    alertView.autoHideTimeInterval = duration;
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.flexDirection = YYBAlertViewFlexDirectionHorizonal;
        container.backgroundColor = [UIColor blackColor];
        container.rectCornerRadius = [weakAlertView commonCornerRadius];
        container.rectCorner = UIRectCornerAllCorners;

        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = CGSizeMake(20, 20);
            action.margin = UIEdgeInsetsMake(15, 20, 15, 10);
            
            imageView.image = [NSBundle imageWithBundleName:@"Icon_AlertView" imageName:icon];
        }];
        
        [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
            action.margin = UIEdgeInsetsMake(0, 0, 0, 20);
            
            label.text = text;
            label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
            label.textColor = [UIColor whiteColor];
        }];
    }];
    
    [alertView showAlertViewOnKeyWindow];
}

+ (void)showAlertViewWithError:(NSError *)error
{
    NSString *description = error.localizedDescription;
    NSString *message = [error.userInfo objectForKey:@"message"];
    
    if ([message isExist]) {
        [YYBAlertView showAlertViewWithStatusString:message];
    } else if ([description isExist]) {
        [YYBAlertView showAlertViewWithStatusString:description];
    } else {
        [YYBAlertView showAlertViewWithStatusString:@"请求出错"];
    }
}

+ (void)showAlertViewWithError:(NSError *)error dismissAlertView:(YYBAlertView *)dismissAlertView
{
    [dismissAlertView closeAlertView];
    [self showAlertViewWithError:error];
}

+ (YYBAlertView *)showEmptyDataAlertViewInView:(UIView *)inView
{
    UIImage *icon = [NSBundle imageWithBundleName:@"Icon_AlertView" imageName:@"empty_data"];
    return [self showMaskAlertViewInView:inView icon:icon iconSize:CGSizeMake(200, 150) title:@"暂无数据" content:@"请稍后再尝试重新获取" marginInsets:UIEdgeInsetsZero paddingInsets:UIEdgeInsetsZero actionTitle:nil tapedActionBlock:^{
        
    }];
}

+ (YYBAlertView *)showNetworkErrorAlertViewInView:(UIView *)inView
{
    UIImage *icon = [NSBundle imageWithBundleName:@"Icon_AlertView" imageName:@"network_error"];
    return [self showMaskAlertViewInView:inView icon:icon iconSize:CGSizeMake(200, 150) title:@"暂无数据" content:@"当前无法获取数据" marginInsets:UIEdgeInsetsZero paddingInsets:UIEdgeInsetsZero actionTitle:nil tapedActionBlock:^{
        
    }];
}

+ (YYBAlertView *)showRefreshDataLoadingAlertViewInView:(UIView *)inView
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.animationStyle = YYBAlertViewAnimationStyleCenter;
    alertView.backgroundColor = [UIColor whiteColor];
    
    YYBRotateLoadingView *custom = [[YYBRotateLoadingView alloc] init];
    custom.backgroundColor = [UIColor whiteColor];

    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        [container addCustomView:custom configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.margin = UIEdgeInsetsMake(20, 20, 0, 20);
            action.size = CGSizeMake(80, 40);
        }];
    }];
    
    [inView addSubview:alertView];
    alertView.frame = inView.bounds;
    [alertView showAlertView];
    
    [custom beginAnimation];
    return alertView;
}

+ (YYBAlertView *)showEmptyDataAlertViewWithTitle:(NSString *)title inView:(UIView *)inView margin:(UIEdgeInsets)margin
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.backgroundColor = [UIColor whiteColor];
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        container.maximalHeight = [UIScreen mainScreen].bounds.size.height;
        
        [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
            label.text = title;
            label.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
            label.textColor = [[weakAlertView thirdTextColor] colorWithAlphaComponent:0.5f];
        }];
    }];
    
    [inView addSubview:alertView];
    
    alertView.frame = CGRectMake(0, 0, CGRectGetWidth(inView.frame), CGRectGetHeight(inView.frame) - margin.top - margin.bottom);
    [alertView showAlertView];
    
    return alertView;
}

+ (YYBAlertView *)showMaskAlertViewInView:(UIView *)inView icon:(UIImage *)icon iconSize:(CGSize)size
                                    title:(nullable NSString *)title content:(nullable NSString *)content
                             marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets
                              actionTitle:(nullable NSString *)actionTitle tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.backgroundColor = [UIColor whiteColor];
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalHeight = 500;
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = CGSizeMake(0, paddingInsets.top * 2);
        }];
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = size;
            
            imageView.image = icon;
        }];
        
        if (title && title.length > 0)
        {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(30, 0, 0, 0);
                
                label.text = title;
                label.textColor = [weakAlertView mainTextColor];
                label.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
            }];
        }
        
        if (content && content.length > 0)
        {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(5, 0, 0, 0);
                
                label.text = content;
                label.textColor = [weakAlertView thirdTextColor];
                label.font = [UIFont systemFontOfSize:12];
            }];
        }
        
        if (actionTitle && actionTitle.length > 0)
        {
            [container addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.margin = UIEdgeInsetsMake(40, 0, 0, 0);
                action.size = CGSizeMake(120, 45);
                
                [button setBackgroundImage:[weakAlertView renderTintColor].colorToUIImage forState:0];
                [button setTitle:actionTitle forState:0];
                button.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
                [button cornerRadius:22.5];
                
            } tapedOnBlock:^{
                if (tapedActionBlock) {
                    tapedActionBlock();
                }
            }];
        }
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = CGSizeMake(0, paddingInsets.bottom * 2);
        }];
    }];
    
    [inView addSubview:alertView];
    
    alertView.frame = CGRectMake(marginInsets.left,
                                 marginInsets.top,
                                 CGRectGetWidth(inView.frame) - marginInsets.left - marginInsets.right,
                                 CGRectGetHeight(inView.frame) - marginInsets.top - marginInsets.bottom);
    [alertView showAlertView];
    
    return alertView;
}

+ (void)showInputAlertViewWithTitle:(NSString *)title content:(NSString *)content placeholder:(NSString *)placeholder
                        stringChangedBlock:(void (^)(NSString * _Nonnull))stringChangedBlock
                         cancelActionTitle:(NSString *)cancelActionTitle cancelTapedBlock:(void (^)(void))cancelTapedBlock
                        confirmActionTitle:(NSString *)confirmActionTitle confirmTapedBlock:(void (^)(void))confirmTapedBlock
{
    YYBAlertView *alertView = [[[self class] alloc] init];
    alertView.backgroundView.backgroundColor = [UIColor colorWithHexValue:0x000000 alpha:0.6f];
    alertView.animationStyle = YYBAlertViewAnimationStyleCenterShrink;
    alertView.isEnableKeyboardNotification = YES;
    alertView.isManageOffsetWithContainter = YES;
    alertView.offsetOfContainerToKeyboard = ^CGFloat(NSInteger containerIndex) {
        return 30;
    };
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    CGFloat widthMax = [UIScreen mainScreen].bounds.size.width - 50;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalWidth = widthMax;
        container.maximalHeight = MAXFLOAT;
        
        container.backgroundColor = [UIColor whiteColor];
        container.rectCornerRadius = 25;
        container.rectCorner = UIRectCornerAllCorners;
        
        container.actionsContainer.flexPosition = YYBAlertViewFlexPositionStretch;
        container.actionsContainer.flexDirection = YYBAlertViewFlexDirectionHorizonal;
        container.actionsContainer.backgroundColor = [UIColor colorWithHexValue:0xF5F5F5];
        
        [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
            action.margin = UIEdgeInsetsMake(35, 30, 0, 30);
            action.size = CGSizeMake(widthMax - 60, 0);
            
            label.font = [UIFont systemFontOfSize:20 weight:UIFontWeightSemibold];
            label.textColor = [UIColor blackColor];
            label.attributedText = [NSMutableAttributedString createLineSpacingString:title spacingValue:4 alignment:NSTextAlignmentLeft];
        }];
        
        [container addTextFieldWithBlock:^(YYBAlertViewAction *action, UITextField *textField) {
            action.margin = UIEdgeInsetsMake(20, 30, 0, 30);
            action.size = CGSizeMake(0, 50);
            
            textField.text = content;
            textField.placeholder = placeholder;
            textField.textColor = [UIColor blackColor];
            [textField cornerRadius:0 width:1 color:[UIColor colorWithHexValue:0xE7E7E7]];
            
            textField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
            textField.leftViewMode = UITextFieldViewModeAlways;
            textField.clearButtonMode = UITextFieldViewModeAlways;
            
            textField.tintColor = [[weakAlertView renderTintColor] colorWithAlphaComponent:0.4f];
            
        } editingChangedBlock:stringChangedBlock];
        
        [container addCustomView:[UIView new] configureBlock:^(YYBAlertViewAction *action, UIView *view) {
            action.margin = UIEdgeInsetsMake(30, 0, 0, 0);
            action.size = CGSizeMake(0, 1);
            
            view.backgroundColor = [UIColor colorWithHexValue:0xF5F5F5];
        }];
        
        if (cancelActionTitle)
        {
            [container addActionWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.size = CGSizeMake(0, 60);
                action.margin = UIEdgeInsetsMake(0, 0, 0, 0.5);
                
                button.backgroundColor = [UIColor whiteColor];
                [button setTitle:cancelActionTitle forState:0];
                [button setTitleColor:[UIColor blackColor] forState:0];
                [button setTitleColor:[UIColor colorWithHexValue:0xEFF1F4] forState:UIControlStateSelected];
                button.titleLabel.font = [UIFont systemFontOfSize:16];
            } tapedOnBlock:^(NSInteger index) {
                if (cancelTapedBlock) {
                    cancelTapedBlock();
                }
            }];
        }
        
        if (confirmActionTitle)
        {
            [container addActionWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.size = CGSizeMake(0, 60);
                action.margin = UIEdgeInsetsMake(0, 0.5, 0, 0);
                
                button.backgroundColor = [UIColor whiteColor];
                [button setTitle:confirmActionTitle forState:0];
                [button setTitleColor:[UIColor blackColor] forState:0];
                [button setTitleColor:[UIColor colorWithHexValue:0xEFF1F4] forState:UIControlStateSelected];
                button.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
            } tapedOnBlock:^(NSInteger index) {
                if (confirmTapedBlock) {
                    confirmTapedBlock();
                }
            }];
        }
    }];
    
    [alertView showAlertViewOnKeyWindow];
}

@end
