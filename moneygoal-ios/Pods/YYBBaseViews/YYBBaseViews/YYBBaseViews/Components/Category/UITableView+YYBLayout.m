//
//  UITableView+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/24.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UITableView+YYBLayout.h"

@implementation UITableView (YYBLayout)

+ (UITableView *)createAtSuperView:(UIView *)superView delagateBlock:(id)delegeteTarget constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock dequeueCellIdentifiers:(NSArray *)dequeueCellIdentifiers configureBlock:(YYBTableViewConfigBlock)configureBlock
{
    UITableView *view = [[UITableView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (dequeueCellIdentifiers && dequeueCellIdentifiers.count > 0) {
        for (NSString *className in dequeueCellIdentifiers) {
            Class class = NSClassFromString(className);
            if (class) {
                [view registerClass:class forCellReuseIdentifier:className];
            }
        }
    }
    
    if (delegeteTarget) {
        view.delegate = delegeteTarget;
        view.dataSource = delegeteTarget;
    }
    
    if (superView) {
        [superView addSubview:view];
    }
    
    if (constraintBlock) {
        [view mas_makeConstraints:constraintBlock];
    }
    
    if (configureBlock) {
        configureBlock(view);
    }
    return view;
}

@end
