//
//  NSMutableAttributedString+YYBAdd.h
//  YYBBaseViews
//
//  Created by alchemy on 2021/2/22.
//  Copyright © 2021 Univease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (YYBAdd)

+ (NSMutableAttributedString *)createLineSpacingString:(NSString *)string spacingValue:(CGFloat)spacingValue alignment:(NSTextAlignment)alignment;

@end

NS_ASSUME_NONNULL_END
