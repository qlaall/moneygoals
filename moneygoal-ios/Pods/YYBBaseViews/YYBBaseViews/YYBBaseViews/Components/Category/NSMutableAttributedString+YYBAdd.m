//
//  NSMutableAttributedString+YYBAdd.m
//  YYBBaseViews
//
//  Created by alchemy on 2021/2/22.
//  Copyright © 2021 Univease Co., Ltd. All rights reserved.
//

#import "NSMutableAttributedString+YYBAdd.h"

@implementation NSMutableAttributedString (YYBAdd)

+ (NSMutableAttributedString *)createLineSpacingString:(NSString *)string spacingValue:(CGFloat)spacingValue alignment:(NSTextAlignment)alignment
{
    NSMutableParagraphStyle *graph = [[NSMutableParagraphStyle alloc] init];
    graph.alignment = alignment;
    graph.lineSpacing = spacingValue;
    graph.lineBreakMode = NSLineBreakByTruncatingTail;
    
    if (string.length == 0 || !string) {
        string = @"";
    }
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:string];
    [attributed addAttribute:NSParagraphStyleAttributeName value:graph range:NSMakeRange(0, string.length)];
    
    return attributed;
}

@end
