//
//  UICollectionView+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/23.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UICollectionView+YYBLayout.h"

@implementation UICollectionView (YYBLayout)

+ (UICollectionView *)createAtSuperView:(UIView *)superView delegeteTarget:(id)delegeteTarget edgeInsets:(UIEdgeInsets)edgeInsets scrollDirection:(UICollectionViewScrollDirection)scrollDirection constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock dequeueCellIdentifiers:(NSArray *)dequeueCellIdentifiers configureBlock:(YYBCollectionViewConfigBlock)configureBlock
{
    return [self createAtSuperView:superView delegeteTarget:delegeteTarget constraintBlock:constraintBlock dequeueCellIdentifiers:dequeueCellIdentifiers configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        
        view.contentInset = edgeInsets;
        layout.scrollDirection = scrollDirection;
        
        if (configureBlock) {
            configureBlock(view, layout);
        }
    }];
}

+ (UICollectionView *)createAtSuperView:(UIView *)superView delegeteTarget:(id)delegeteTarget constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock dequeueCellIdentifiers:(NSArray *)dequeueCellIdentifiers configureBlock:(YYBCollectionViewConfigBlock)configureBlock
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *view = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    view.backgroundColor = [UIColor whiteColor];
    
    if (dequeueCellIdentifiers && dequeueCellIdentifiers.count > 0) {
        for (NSString *className in dequeueCellIdentifiers) {
            Class class = NSClassFromString(className);
            if (class) {
                [view registerClass:class forCellWithReuseIdentifier:className];
            }
        }
    }
    
    if (delegeteTarget) {
        view.delegate = delegeteTarget;
        view.dataSource = delegeteTarget;
    }
    
    if (superView) {
        [superView addSubview:view];
    }
    
    if (constraintBlock) {
        [view mas_makeConstraints:constraintBlock];
    }
    
    if (configureBlock) {
        configureBlock(view ,layout);
    }
    return view;
}

@end
