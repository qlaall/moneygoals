//
//  UITextField+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UITextField+YYBLayout.h"

@implementation UITextField (YYBLayout)

+ (UITextField *)createAtSuperView:(UIView *)superView leftViewOffset:(CGFloat)leftViewOffset cornerRadius:(CGFloat)cornerRadius fontValue:(UIFont *)fontValue textColor:(UIColor *)textColor constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBTextFieldConfigBlock)configureBlock
{
    UITextField *view = [[UITextField alloc] init];
    [superView addSubview:view];
    
    view.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftViewOffset, 0)];
    view.leftViewMode = UITextFieldViewModeAlways;
    view.font = fontValue;
    view.textColor = textColor;
    
    view.layer.cornerRadius = cornerRadius;
    view.clipsToBounds = YES;
    
    if (constraintBlock) {
        [view mas_makeConstraints:constraintBlock];
    }
    
    if (configureBlock) {
        configureBlock(view);
    }
    
    return view;
}


@end
