//
//  UIImageView+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

typedef void (^ YYBImageViewConfigBlock) (UIImageView *view);

@interface UIImageView (YYBLayout)

+ (instancetype)createAtSuperView:(UIView *)superView iconName:(NSString *)iconName constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBImageViewConfigBlock)configureBlock;

+ (instancetype)createAvatarAtSuperView:(UIView *)superView cornerRadius:(CGFloat)cornerRadius constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBImageViewConfigBlock)configureBlock;

@end
