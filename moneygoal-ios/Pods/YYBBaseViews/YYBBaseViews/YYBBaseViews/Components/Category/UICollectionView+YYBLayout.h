//
//  UICollectionView+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/23.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

typedef void (^ YYBCollectionViewConfigBlock) (UICollectionView *view , UICollectionViewFlowLayout *layout);

@interface UICollectionView (YYBLayout)

+ (UICollectionView *)createAtSuperView:(UIView *)superView delegeteTarget:(id)delegeteTarget constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock dequeueCellIdentifiers:(NSArray *)dequeueCellIdentifiers configureBlock:(YYBCollectionViewConfigBlock)configureBlock;

+ (UICollectionView *)createAtSuperView:(UIView *)superView delegeteTarget:(id)delegeteTarget edgeInsets:(UIEdgeInsets)edgeInsets scrollDirection:(UICollectionViewScrollDirection)scrollDirection constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock dequeueCellIdentifiers:(NSArray *)dequeueCellIdentifiers configureBlock:(YYBCollectionViewConfigBlock)configureBlock;

@end
