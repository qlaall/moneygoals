//
//  UILabel+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UILabel+YYBLayout.h"

@implementation UILabel (YYBLayout)

+ (instancetype)createAtSuperView:(UIView *)superView fontValue:(UIFont *)fontValue textColor:(UIColor *)textColor constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBLabelConfigBlock)configureBlock
{
    UILabel *view = [UILabel new];
    view.textColor = textColor;
    view.font = fontValue;
    
    if (superView) {
        [superView addSubview:view];
        if (constraintBlock) {
            [view mas_makeConstraints:constraintBlock];
        }
        if (configureBlock) {
            configureBlock(view);
        }
    }
    return view;
}

+ (instancetype)createAtSuperView:(UIView *)superView string:(NSString *)string lineSpacingValue:(CGFloat)lineSpacingValue alignment:(NSTextAlignment)alignment fontValue:(UIFont *)fontValue textColor:(UIColor *)textColor constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBLabelConfigBlock)configureBlock
{
    return [self createAtSuperView:superView fontValue:fontValue textColor:textColor constraintBlock:constraintBlock configureBlock:^(UILabel *view) {
        view.numberOfLines = 0;
        view.attributedText = [NSMutableAttributedString createLineSpacingString:string spacingValue:lineSpacingValue alignment:alignment];
        
        if (configureBlock) {
            configureBlock(view);
        }
    }];
}

@end
