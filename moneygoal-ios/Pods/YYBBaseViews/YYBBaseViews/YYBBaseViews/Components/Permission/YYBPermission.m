//
//  YYBPermission.m
//  YYBBaseViews
//
//  Created by alchemy on 2019/4/2.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPermission.h"

static NSString * const YYBUserBiometricsStatus = @"YYBUserBiometricsStatusKey";

@implementation YYBPermission

+ (void)photoPermissionWithCompleteBlock:(void (^)(void))completeBlock
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completeBlock();
                });
            }
        }];
    } else if (status == PHAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completeBlock();
        });
    } else {
        [YYBAlertView showAlertViewWithTitle:@"权限请求失败" detail:@"您没有打开相册权限，打开相关设置后才能继续使用功能" firstActionTitle:@"取消" firstActionTapedBlock:nil secondActionTitle:@"设置" secondActionTapedBlock:^{
            [NSObject openURL:UIApplicationOpenSettingsURLString];
        }];
    }
}

+ (void)permissionUserUnLock
{
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    if (error == nil) {
        if (@available(iOS 11.0, *)) {
            switch (context.biometryType) {
                case LABiometryTypeNone: {
                    
                }
                    break;
                case LABiometryTypeFaceID: {
                    [YYBAlertView showAlertViewWithTitle:@"安全提醒" detail:@"是否启用FaceID以解锁应用" firstActionTitle:@"取消" firstActionTapedBlock:nil secondActionTitle:@"启用" secondActionTapedBlock:^{
                        [self noteUserUsingBiometricsStatus:YES];
                    }];
                }
                    break;
                case LABiometryTypeTouchID: {
                    [YYBAlertView showAlertViewWithTitle:@"安全提醒" detail:@"是否启用TouchID以解锁应用" firstActionTitle:@"取消" firstActionTapedBlock:nil secondActionTitle:@"启用" secondActionTapedBlock:^{
                        [self noteUserUsingBiometricsStatus:YES];
                    }];
                }
                    break;
                default:
                    break;
            }
        } else {
            [YYBAlertView showAlertViewWithTitle:@"安全提醒" detail:@"是否启用TouchID以解锁应用" firstActionTitle:@"取消" firstActionTapedBlock:nil secondActionTitle:@"启用" secondActionTapedBlock:^{
                [self noteUserUsingBiometricsStatus:YES];
            }];
        }
    } else {
        [YYBAlertView showAlertViewWithError:error];
    }
}

+ (void)noteUserUsingBiometricsStatus:(BOOL)status
{
    [[NSUserDefaults standardUserDefaults] setObject:@(status) forKey:YYBUserBiometricsStatus];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)userBiometricsStatus
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:YYBUserBiometricsStatus] boolValue];
}

+ (void)getUserUnLockPermissionWithCompleteBlock:(void (^)(BOOL, NSError * _Nullable))completionBlock
{
    if ([self userBiometricsStatus] == YES)
    {
        LAContext *context = [[LAContext alloc] init];
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"验证以解锁应用" reply:completionBlock];
    }
}

@end
