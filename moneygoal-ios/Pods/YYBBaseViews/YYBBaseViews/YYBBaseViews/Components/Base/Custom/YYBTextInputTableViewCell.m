//
//  YYBTextInputTableViewCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBTextInputTableViewCell.h"

@implementation YYBTextInputTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.maxInputCount = 10000;
    
    self.placeholderStringView = [[YYBPlaceholderTextView alloc] init];
    [self.contentView addSubview:self.placeholderStringView];
    [self.placeholderStringView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    self.placeholderStringView.textContainerInset = UIEdgeInsetsZero;
    self.placeholderStringView.textContainer.lineFragmentPadding = 0;
    
    @weakify(self);
    [self.placeholderStringView.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        NSString *text = [self textChangedCallbackActionWithInputText:x];
        
        if (self.stringChangedBlock) {
            self.stringChangedBlock(text);
        }
    }];
    
    return self;
}

- (NSString *)textChangedCallbackActionWithInputText:(NSString *)inputText
{
    if (inputText.length > _maxInputCount) {
        inputText = [inputText substringToIndex:_maxInputCount];
    }
    
    return inputText;
}

- (void)reloadViewWithInputTextValue:(NSString *)inputTextValue placeholder:(NSString *)placeholder
{
    self.placeholderStringView.text = inputTextValue;
    self.placeholderStringView.placeholder = placeholder;
}

@end
