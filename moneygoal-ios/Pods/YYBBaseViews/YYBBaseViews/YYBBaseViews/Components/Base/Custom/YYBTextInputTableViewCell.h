//
//  YYBTextInputTableViewCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBTableViewCell.h"
#import <ReactiveObjC/ReactiveObjC.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYBTextInputTableViewCell : YYBTableViewCell

// 默认10000
@property (nonatomic) NSInteger maxInputCount;
- (NSString *)textChangedCallbackActionWithInputText:(NSString *)inputText;

- (void)reloadViewWithInputTextValue:(NSString *)inputTextValue placeholder:(NSString *)placeholder;

@end

NS_ASSUME_NONNULL_END
