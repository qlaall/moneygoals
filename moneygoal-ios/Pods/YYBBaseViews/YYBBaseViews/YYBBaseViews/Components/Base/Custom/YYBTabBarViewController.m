//
//  YYBTabBarViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/10/28.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBTabBarViewController.h"

@interface YYBTabBarViewController ()

@end

@implementation YYBTabBarViewController

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    YYBTabBarViewController *viewController = [super allocWithZone:zone];
    
    @weakify(viewController);
    [[viewController rac_signalForSelector:@selector(viewDidLoad)]
     subscribeNext:^(id x) {
        @strongify(viewController);
        [viewController afterConfigViewAction];
        [viewController beginQueryData];
        [viewController beginCreateNotifications];
     }];
    
    return viewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.iconSize = CGSizeMake(24, 24);
    self.titleFont = [UIFont systemFontOfSize:11 weight:UIFontWeightSemibold];
    self.plainTitleColor = [UIColor colorWithHexValue:0x373a40];
    self.selectedTitleColor = [UIColor colorWithHexValue:0x373a40];
    
    NSArray *vcs = [self customViewControllerNames];
    if (vcs.count > 0) {
        self.viewControllers = [vcs map:^id(NSString *obj, NSInteger index) {
            UIViewController *vc = [[NSClassFromString(obj) alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            return nav;
        }];
    }
    
    CGFloat tabBarHeight = 49 + [UIDevice safeAreaInsetsBottom];
    
    _customTabBar = [[YYBTabBar alloc] initWithFrame:CGRectZero];
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    _customTabBar.frame = CGRectMake(0, screenSize.height - tabBarHeight, screenSize.width, tabBarHeight);
    _customTabBar.tabBarDelegate = self;
    [_customTabBar setBackgroundImage:[UIColor clearColor].colorToUIImage];
    [_customTabBar setShadowImage:[UIColor clearColor].colorToUIImage];
    [self setValue:_customTabBar forKey:@"tabBar"];
    
    _customTabBar.contentView.backgroundColor = [UIColor whiteColor];
    _customTabBar.separateView.backgroundColor = [UIColor whiteColor];
    
    _customTabBar.shadowView.layer.shadowColor = [[UIColor grayColor] colorWithAlphaComponent:0.1f].CGColor;
    _customTabBar.shadowView.layer.shadowOffset = CGSizeZero;
    _customTabBar.shadowView.layer.shadowRadius = 4;
    _customTabBar.shadowView.layer.shadowOpacity = 1;
}

- (void)afterConfigViewAction
{
    _tabBarContainerList = [[NSMutableArray alloc] init];
    if ([self respondsToSelector:@selector(customTabBarContainerList)]) {
        _tabBarContainerList = [self customTabBarContainerList];
    }
    
    [_customTabBar refreshTabBarContainers];

    self.selectedIndex = [self firstSelectedContainerIndex];
}

- (NSMutableArray<YYBNavigationBarContainer *> *)customTabBarContainerList
{
    return [NSMutableArray new];
}

- (NSArray *)customViewControllerNames
{
    return @[];
}

- (void)beginQueryData
{
    
}

- (void)beginCreateNotifications
{
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    _customTabBar.containerOffset = [UIDevice safeAreaInsetsBottom];
    
    for (UIView *tabBar in self.tabBar.subviews) {
        if ([tabBar isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [tabBar removeFromSuperview];
        }
    }
}

- (NSInteger)firstSelectedContainerIndex
{
    return 0;
}

- (NSInteger)numbersOfContainersInTabBar:(YYBTabBar *)tabBar
{
    return _tabBarContainerList.count;
}

- (YYBNavigationBarContainer *)tabBarContainerInTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex
{
    return [_tabBarContainerList objectAtIndex:componentIndex];
}

- (BOOL)containerCouldSelectedWithTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex
{
    return YES;
}

- (void)didSelectedContainerInTabBar:(YYBTabBar *)tabBar componentIndex:(NSInteger)componentIndex
{
    self.selectedIndex = componentIndex;
}

- (BOOL)shouldHandleHitTestWithTabBar:(YYBTabBar *)tabBar
{
    return NO;
}

- (YYBNavigationBarControl *)commonContainerWithPlainStateIcon:(id)plainStateIcon selectedStateIcon:(id)selectedStateIcon title:(NSString *)title
{
    return [YYBNavigationBarControl controlWithConfigureBlock:^(YYBNavigationBarControl *container, UIImageView *pictureView, UILabel *label) {
        
        container.imageSize = self.iconSize;
        container.style = YYBNavigationBarControlStyleTop;
        container.labelEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
        container.iconEdgeInsets = UIEdgeInsetsMake(4, 0, 0, 0);
        
        [container setBarButtonTitle:title controlState:0];
        [container setBarButtonTextFont:self.titleFont controlState:0];
        [container setBarButtonTextColor:self.plainTitleColor controlState:0];
        [container setBarButtonTextColor:self.selectedTitleColor controlState:UIControlStateSelected];

        if ([plainStateIcon isKindOfClass:[UIImage class]]) {
            [container setBarButtonImage:plainStateIcon controlState:0];
        } else if ([plainStateIcon isKindOfClass:[NSString class]]) {
            [container setBarButtonImage:[UIImage imageNamed:plainStateIcon] controlState:0];
        }
        
        if ([selectedStateIcon isKindOfClass:[UIImage class]]) {
            [container setBarButtonImage:selectedStateIcon controlState:UIControlStateSelected];
            [container setBarButtonImage:selectedStateIcon controlState:UIControlStateHighlighted];
        } else if ([selectedStateIcon isKindOfClass:[NSString class]]) {
            [container setBarButtonImage:[UIImage imageNamed:selectedStateIcon] controlState:UIControlStateSelected];
            [container setBarButtonImage:[UIImage imageNamed:selectedStateIcon] controlState:UIControlStateHighlighted];
        }
        
    } tapedActionBlock:nil];
}

@end
