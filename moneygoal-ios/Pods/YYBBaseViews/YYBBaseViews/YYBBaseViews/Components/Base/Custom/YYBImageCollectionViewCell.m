//
//  YYBImageCollectionViewCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBImageCollectionViewCell.h"

@implementation YYBImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;

    self.backgroundIconView = [[UIImageView alloc] init];
    self.backgroundIconView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.backgroundIconView];
    [self.backgroundIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    return self;
}

- (void)configViewWithIconURL:(NSString *)iconURL cacheImage:(UIImage *)cacheImage placeholderImage:(nonnull UIImage *)placeholderImage
{
    if (iconURL) {
        [self.backgroundIconView sd_setImageWithURL:[NSURL URLWithString:iconURL] placeholderImage:placeholderImage];
    } else if (cacheImage) {
        self.backgroundIconView.image = cacheImage;
    }
}

@end
