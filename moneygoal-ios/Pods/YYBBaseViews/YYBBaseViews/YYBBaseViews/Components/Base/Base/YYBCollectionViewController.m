//
//  YYBCollectionViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/10/23.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBCollectionViewController.h"

@interface YYBCollectionViewController ()

@end

@implementation YYBCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dataList = [NSMutableArray new];
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    _collectionView = [[[self handleUserCollectionViewClass] alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.contentInset = [self handleEdgeInsets];
    _collectionView.alwaysBounceVertical = YES;
    [self.view addSubview:_collectionView];
    
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    for (NSString *className in [self dequeueCellIdentifiers]) {
        [_collectionView registerClass:NSClassFromString(className) forCellWithReuseIdentifier:className];
    }
    
    UITapGestureRecognizer *taped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTaped:)];
    taped.cancelsTouchesInView = NO;
    [_collectionView addGestureRecognizer:taped];
}

- (void)handleTaped:(UITapGestureRecognizer *)gesture
{
    if ([self respondsToSelector:@selector(handleBackgroundTapedAction)]) {
        [self handleBackgroundTapedAction];
    }
    [self.view endEditing:YES];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers
{
    return @[];
}

- (void)beginQueryData
{
    [_collectionView setContentOffset:CGPointMake(0, -_collectionView.contentInset.top) animated:NO];
}

- (Class)handleUserCollectionViewClass
{
    return [UICollectionView class];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    YYBNavigationTitleStyle style = [self customHeaderStyle];
    if (style == YYBNavigationTitleStyleScroll) {
        CGFloat customPageHeaderHeight = [self customPageHeaderHeight];
        CGFloat scrollTitleViewHeight = [self scrollTitleViewHeight];
        
        CGFloat offset = customPageHeaderHeight + scrollTitleViewHeight + scrollView.contentOffset.y;
        offset = MAX(offset, 0);
        offset = MIN(offset, scrollTitleViewHeight);
        
        self.navigationBar.titleBarLabel.alpha = offset / scrollTitleViewHeight;
        self.scrollTitleView.transform = CGAffineTransformMakeTranslation(0, -offset);
    }
}

- (UIEdgeInsets)handleEdgeInsets
{
    CGFloat top = [self customPageHeaderHeight] + [self scrollTitleViewHeight];
    return UIEdgeInsetsMake(top, 0, [UIDevice safeAreaInsetsBottom], 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

@end
