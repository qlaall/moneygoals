//
//  YYBBaseGlobal.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/1/26.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "YYBNavigationBar.h"
#import "YYBBaseTypedef.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YYBReturnButtonStyle)
{
    YYBReturnButtonStyleBack,
    YYBReturnButtonStyleBackWithTitle,
    YYBReturnButtonStyleDismiss,
    
    YYBReturnButtonStyleBackWhite,
    YYBReturnButtonStyleBackWithTitleWhite,
    YYBReturnButtonStyleDismissWhite
};

@protocol YYBBaseGlobal <NSObject>

- (NSString *)customPageTitle;
- (UIColor *)customPageHeaderTintColor;
- (CGFloat)customPageHeaderHeight;
// Gesoverlay 使用
- (CGFloat)customPageHeaderEdgeInsetTop;

// NavigationBar
- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar;
// 设置返回按钮样式与标题样式
- (void)configCustomReturnButton:(YYBNavigationBarControl *)returnBarButton;
// 设置中心标题的样式
- (void)configCustomPageTitleView:(YYBNavigationBarLabel *)titleView;

// 特殊的NavigationBar的中心View
- (UIView *)customNavigationBarTitleView;

- (YYBNavigationTitleStyle)customHeaderStyle;

// 当 customHeaderStyle为Scroll时候, 设置该标题Label的属性
- (void)configCustomHeaderScrollTitleView:(YYBNavigationBarLabel *)scrollTitleView;
- (CGFloat)scrollTitleViewHeight;

// 默认的主题颜色
- (UIColor *)contentBackgroundColor;
- (YYBReturnButtonStyle)returnButtonStyle;

@end

NS_ASSUME_NONNULL_END
