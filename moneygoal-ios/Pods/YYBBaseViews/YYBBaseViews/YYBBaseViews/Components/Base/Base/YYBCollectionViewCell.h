//
//  YYBCollectionViewCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "YYBBaseTypedef.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIView *wrapperView, *firstWrapperView, *secondWrapperView;
@property (nonatomic, strong) UIView *firstLineView, *secondLineView, *thirdLineView;

@property (nonatomic, strong) UICollectionView *gatherView;

@property (nonatomic, strong) UIImageView *backgroundIconView;
@property (nonatomic, strong) UIImageView *avatorView;
@property (nonatomic, strong) UIImageView *firstIconView, *secondIconView, *thirdIconView;

@property (nonatomic, strong) UILabel *userNameLabel, *createTimeLabel, *updateTimeLabel, *moneyStringLabel;
@property (nonatomic, strong) UILabel *firstStringLabel, *secondStringLabel, *thirdStringLabel, *forthStringLabel;
@property (nonatomic, strong) UILabel *firstValueStringLabel, *secondValueStringLabel, *thirdValueStringLabel, *forthValueStringLabel;

@property (nonatomic, strong) UIButton *firstActionButton, *secondActionButton, *thirdActionButton;

@property (nonatomic, copy) YYBTapedActionBlock tapedActionBlock;
@property (nonatomic, copy) YYBStringChangedBlock stringChangedBlock;
@property (nonatomic, copy) YYBStatusBlock statusBlock;
@property (nonatomic, copy) YYBTextBlock textBlock;

- (void)configViewWithDataModel:(id)dataModel;
- (void)configViewWithDataModel:(id)dataModel status:(BOOL)status;

- (void)configViewWithTitleValue:(NSString *)titleValue;
- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon;
- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status;

// ===============================
- (void)viewBeginConfigConstraints;
- (void)viewBeginConfigData;
- (void)viewBeginConfigBlocks;

@end

NS_ASSUME_NONNULL_END
