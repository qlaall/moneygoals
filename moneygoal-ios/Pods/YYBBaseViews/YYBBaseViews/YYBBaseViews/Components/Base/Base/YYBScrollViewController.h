//
//  YYBScrollViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/10/28.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBScrollViewController : YYBViewController

@property (nonatomic, strong, readonly) UIScrollView *scrollView;

@end

NS_ASSUME_NONNULL_END
