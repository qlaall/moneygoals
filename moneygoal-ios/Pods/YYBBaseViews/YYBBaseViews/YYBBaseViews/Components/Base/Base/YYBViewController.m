//
//  YYBViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBViewController.h"
#import <Masonry/Masonry.h>

@interface YYBViewController ()

@end

@implementation YYBViewController

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    YYBViewController *viewController = [super allocWithZone:zone];
    
    @weakify(viewController);
    [[viewController rac_signalForSelector:@selector(viewDidLoad)]
     subscribeNext:^(id x) {
        @strongify(viewController);
        [viewController beforeConfigViewAction];
        [viewController beginConfigViewAction];
        [viewController renderGlobalNavigationBar];
        [viewController afterConfigViewAction];
        [viewController beginQueryData];
        [viewController beginCreateNotifications];
     }];
    
    return viewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.fd_prefersNavigationBarHidden = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [UIScrollView appearanceWhenContainedInInstancesOfClasses:@[self.class]].contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    
    _isHiddenNavigationBar = NO;
    _isHiddenNavigationReturnButton = NO;
}

- (void)beforeConfigViewAction
{
    
}

- (void)beginConfigViewAction
{
    
}

- (void)afterConfigViewAction
{
    
}

- (void)beginQueryData
{
    
}

- (void)beginCreateNotifications
{
    
}

- (void)renderGlobalNavigationBar
{
    if (_isHiddenNavigationBar) return;
    
    YYBNavigationTitleStyle style = [self customHeaderStyle];
    if (style == YYBNavigationTitleStyleScroll || style == YYBNavigationTitleStyleOnlyScroll) {
        _scrollTitleView = [[YYBNavigationBarLabel alloc] init];
        
        [self configCustomHeaderScrollTitleView:_scrollTitleView];
        [self.view addSubview:_scrollTitleView];
        
        _scrollTitleView.label.text = [self customPageTitle];
        [_scrollTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.view).offset([self customPageHeaderHeight]);
            make.height.mas_equalTo([self scrollTitleViewHeight]);
        }];
    }
    
    _navigationBar = [[YYBNavigationBar alloc] init];
    _navigationBar.contentViewTopEdgeInset = [UIDevice safeAreaInsetsTop];
    [self.view addSubview:_navigationBar];
    
    UIView *customNavigationTitleView = [self customNavigationBarTitleView];
    if (!customNavigationTitleView)
    {
        if (style != YYBNavigationTitleStyleOnlyScroll) {
            if (style == YYBNavigationTitleStyleGeneric || style == YYBNavigationTitleStyleScroll) {
                _navigationBar.titleBarLabel.label.text = [self customPageTitle];
                [self configCustomPageTitleView:_navigationBar.titleBarLabel];
            }
        }
    }
    else
    {
        _navigationBar.titleCustomView = customNavigationTitleView;
    }

    [self configCustomNavigationBar:_navigationBar];
    _navigationBar.backgroundColor = [self customPageHeaderTintColor];
    
    [_navigationBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset([self customPageHeaderEdgeInsetTop]);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo([self customPageHeaderHeight]);
    }];
    
    if (!_isHiddenNavigationReturnButton) {
        @weakify(self);
        _returnBarButton = [[YYBNavigationBarControl alloc] init];
        _returnBarButton.tapedActionBlock = ^(YYBNavigationBarContainer *view) {
            @strongify(self);
            [self handleBackBarButtonAction];
        };
        
        [self configCustomReturnButton:_returnBarButton];
        _navigationBar.leftBarContainers = @[_returnBarButton];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)handleBackBarButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

// Protocol Methods
- (NSString *)customPageTitle
{
    return @"";
}

- (UIColor *)customPageHeaderTintColor
{
    return [UIColor whiteColor];
}

 - (CGFloat)customPageHeaderHeight
{
    return 44.0 + [UIDevice safeAreaInsetsTop];
}

- (YYBReturnButtonStyle)returnButtonStyle
{
    return YYBReturnButtonStyleBackWithTitle;
}

- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar
{
    navigationBar.backgroundColor = [UIColor whiteColor];
    navigationBar.bottomLayerView.backgroundColor = [UIColor whiteColor];
}

- (void)configCustomReturnButton:(YYBNavigationBarControl *)returnBarButton
{
    returnBarButton.style = YYBNavigationBarControlStyleLeft;
    
    YYBReturnButtonStyle style = [self returnButtonStyle];
    
    if (style == YYBReturnButtonStyleBackWithTitle || style == YYBReturnButtonStyleBackWithTitleWhite) {
        returnBarButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        returnBarButton.iconEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6);
        returnBarButton.imageSize = CGSizeMake(26, 44);

        [returnBarButton setBarButtonTextColor:[UIColor blackColor] controlState:0];
        
        YYBNavigationTitleStyle customHeaderStyle = [self customHeaderStyle];
        if (customHeaderStyle == YYBNavigationTitleStyleBackButton) {
            [returnBarButton setBarButtonTitle:[self customPageTitle] controlState:0];
        } else {
            [returnBarButton setBarButtonTitle:@"返回" controlState:0];
        }
        
        [returnBarButton setBarButtonTextFont:[UIFont systemFontOfSize:14] controlState:0];
        [returnBarButton setBarButtonTextFont:[UIFont systemFontOfSize:14] controlState:UIControlStateHighlighted];
        
        UIImage *icon = nil;
        if (style == YYBReturnButtonStyleBackWithTitle) {
            icon = [NSBundle imageWithBundleName:@"Icon_Base" imageName:@"back_black_half"];
        } else if (style == YYBReturnButtonStyleBackWithTitleWhite) {
            icon = [NSBundle imageWithBundleName:@"Icon_Base" imageName:@"back_white_half"];
        }
        [returnBarButton setBarButtonImage:icon controlState:0];
    } else if (style == YYBReturnButtonStyleBack || style == YYBReturnButtonStyleBackWithTitle) {
        returnBarButton.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        returnBarButton.imageSize = CGSizeMake(44, 44);

        UIImage *icon = nil;
        if (style == YYBReturnButtonStyleBack) {
            icon = [NSBundle imageWithBundleName:@"Icon_Base" imageName:@"back_black"];
        } else if (style == YYBReturnButtonStyleBackWhite) {
            icon = [NSBundle imageWithBundleName:@"Icon_Base" imageName:@"back_white"];
        }
        [returnBarButton setBarButtonImage:icon controlState:0];
    } else if (style == YYBReturnButtonStyleDismiss || style == YYBReturnButtonStyleDismissWhite) {
        returnBarButton.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        returnBarButton.imageSize = CGSizeMake(44, 44);
        
        UIImage *icon = nil;
        if (style == YYBReturnButtonStyleDismiss) {
            icon = [NSBundle imageWithBundleName:@"Icon_Base" imageName:@"dismiss_black"];
        } else if (style == YYBReturnButtonStyleDismissWhite) {
            icon = [NSBundle imageWithBundleName:@"Icon_Base" imageName:@"dismiss_white"];
        }
        [returnBarButton setBarButtonImage:icon controlState:0];
    }
}

- (void)configCustomPageTitleView:(YYBNavigationBarLabel *)titleView
{
    titleView.label.textColor = [UIColor blackColor];
    titleView.label.font = [UIFont systemFontOfSize:17];
    titleView.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)customPageHeaderEdgeInsetTop
{
    return 0;
}

- (UIView *)customNavigationBarTitleView
{
    return nil;
}

- (void)configCustomHeaderScrollTitleView:(YYBNavigationBarLabel *)scrollTitleView
{
    scrollTitleView.label.font = [UIFont systemFontOfSize:32 weight:UIFontWeightSemibold];
    scrollTitleView.label.textColor = [UIColor blackColor];
    scrollTitleView.backgroundColor = [UIColor whiteColor];
    scrollTitleView.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
}

- (YYBNavigationTitleStyle)customHeaderStyle
{
    return YYBNavigationTitleStyleGeneric;
}

- (CGFloat)scrollTitleViewHeight
{
    if ([self customHeaderStyle] == YYBNavigationTitleStyleScroll ||
        [self customHeaderStyle] == YYBNavigationTitleStyleOnlyScroll) {
        return 64;
    }
    return 0;
}

- (UIColor *)contentBackgroundColor
{
    return [UIColor colorWithHexValue:0x2772db];
}

- (void)dealloc
{
    NSLog(@"dealloc - %@",NSStringFromClass([self class]));
}

@end
