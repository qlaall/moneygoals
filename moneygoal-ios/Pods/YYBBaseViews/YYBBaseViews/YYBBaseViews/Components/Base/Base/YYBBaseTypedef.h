//
//  YYBBaseTypedef.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/1/27.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#ifndef YYBBaseTypedef_h
#define YYBBaseTypedef_h

typedef NS_ENUM(NSInteger, YYBNavigationTitleStyle)
{
    // 标题在中间
    YYBNavigationTitleStyleGeneric = 0,
    // 标题在左侧包含一个箭头
    YYBNavigationTitleStyleBackButton = 1,
    // 标题有一个大的 顶上去后Navigation的中央会显示标题
    YYBNavigationTitleStyleScroll = 2,
    // 无Navigation的标题
    YYBNavigationTitleStyleOnlyScroll = 3
};

typedef void (^ YYBTapedActionBlock)(void);

typedef void (^ YYBTapedActionIndexBlock)(NSInteger index);
typedef void (^ YYBStringChangedBlock)(NSString *text);
typedef void (^ YYBFloatChangedBlock)(CGFloat value);

typedef BOOL (^ YYBStatusBlock)(void);
typedef BOOL (^ YYBStatusStringBlock)(NSString *text);

typedef NSString * (^ YYBTextBlock)(void);

#endif /* YYBBaseTypedef_h */
