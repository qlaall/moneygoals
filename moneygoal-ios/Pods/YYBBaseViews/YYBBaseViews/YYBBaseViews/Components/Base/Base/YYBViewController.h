//
//  YYBViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "YYBBaseGlobal.h"
#import "YYBCategory.h"
#import "YYBAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBViewController : UIViewController <YYBBaseGlobal>

@property (nonatomic, strong, readonly, nonnull) YYBNavigationBar *navigationBar;
@property (nonatomic, strong, readonly, nonnull) YYBNavigationBarControl *returnBarButton;
- (void)handleBackBarButtonAction;

@property (nonatomic, strong, nullable) YYBNavigationBarLabel *scrollTitleView;
@property (nonatomic, strong, nullable) YYBAlertView *refreshAlertView;

@property (nonatomic) BOOL isHiddenNavigationBar;
@property (nonatomic) BOOL isHiddenNavigationReturnButton;

// =================================
- (void)beforeConfigViewAction;
- (void)beginConfigViewAction;
// 所有自定义的视图加载完毕后，Navibar加载完后
- (void)afterConfigViewAction;

- (void)beginQueryData;
- (void)beginCreateNotifications;

// =================================

@end

NS_ASSUME_NONNULL_END
