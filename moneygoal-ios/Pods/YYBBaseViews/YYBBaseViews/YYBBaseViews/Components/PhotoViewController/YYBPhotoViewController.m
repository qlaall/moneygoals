//
//  YYBPhotoViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPhotoViewController.h"
#import "YYBPhotoCollectionsView.h"
#import "YYBPhotoAssetCell.h"
#import "YYBPhotoNavigationSelectionView.h"
#import "YYBPhotoToolbarView.h"

NSString * const YYBPhotoViewSelectNotification = @"YYBPhotoViewSelectNotification";

@interface YYBPhotoViewController ()<YYBPhotoCollectionsViewDelegate, YYBPhotoToolbarViewDelegate, YYBPhotoAssetCellDelegate>
@property (nonatomic, strong) YYBPhotoCollectionsView *collectionsView;

@property (nonatomic, strong) UIView *backgroundContentView; // 黑色遮罩
@property (nonatomic, strong) YYBPhotoNavigationSelectionView *navigationSelectionView;
@property (nonatomic, strong) YYBPhotoToolbarView *selectionsView;

@property (nonatomic, strong) dispatch_semaphore_t semaphore;
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) YYBAlertView *alertView;

@end

@implementation YYBPhotoViewController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _maxSelectAssetsCount = 9;
    _semaphore = dispatch_semaphore_create(0);
    _queue = dispatch_queue_create("YYBPHOTOVIEWCONTROLLER.CONCURRENT.QUEUE", DISPATCH_QUEUE_CONCURRENT);
    _requiredSubtype = PHAssetCollectionSubtypeSmartAlbumUserLibrary;
    _selectedAssets = [NSMutableArray new];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.clipsToBounds = YES;
}

- (void)beforeConfigViewAction
{
    [super beforeConfigViewAction];
    
    @weakify(self);
    
    UIColor *backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    _backgroundContentView = [UIView createViewAtSuperView:self.view backgroundColor:backgroundColor constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    } configureBlock:^(UIView *view) {
        view.hidden = YES;
        view.alpha = 0;
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTaped)];
    [_backgroundContentView addGestureRecognizer:tap];
    
    _collectionsView = [YYBPhotoCollectionsView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_top).offset([self customPageHeaderHeight]);
        make.height.mas_equalTo(320);
    } configureBlock:nil];
    
    _collectionsView.delegate = self;
    
    if (_selectionStyle != YYBPhotoViewSelectionStyleOnlyOneAsset) {
        _selectionsView = [YYBPhotoToolbarView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(50 + [UIDevice safeAreaInsetsBottom]);
        } configureBlock:nil];
        
        _selectionsView.finishSelectedBlock = ^{
            @strongify(self);
            [self handleProcessAssetsAction];
        };
    }
}

- (void)beginQueryData
{
    [super beginQueryData];
    
    if (_initialSelectedAssets) {
        _selectedAssets = [_initialSelectedAssets mutableCopy];
    } else {
        _selectedAssets = [NSMutableArray new];
    }
    
    self.collectionView.backgroundColor = [UIColor colorWithHexValue:0x393e46];
    
    Class customClass = [self customPhotoItemCellClass];
    [self.collectionView registerClass:customClass forCellWithReuseIdentifier:NSStringFromClass(customClass)];
    
    [self queryPhotoCollectionsAction];
}

- (YYBAlertView *)customPhotoProcessAlertView
{
    return [YYBAlertView showQueryAlertView];
}

- (Class)customPhotoItemCellClass
{
    return [YYBPhotoAssetCell class];
}

- (void)handleTaped
{
    [_navigationSelectionView handlePhotoSelectionStatus];
}

// 回调UIImage或者Asset
- (void)handleProcessAssetsAction
{
    __weak typeof(self) weakself = self;
    
    dispatch_async(_queue, ^{
        NSMutableArray *results = [NSMutableArray new];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.alertView = [self customPhotoProcessAlertView];
        });
        
        if (self.isProduceAsUIImage) {
            for (NSInteger index = 0; index < self.selectedAssets.count; index ++) {
                PHAsset *asset = [self.selectedAssets objectAtIndex:index];
                [asset queryImageWithCompletionBlock:^(UIImage *targetImage) {
                    [results addObject:targetImage];
                    dispatch_semaphore_signal(weakself.semaphore);
                }];
                dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
            }
        } else {
            results = self.selectedAssets;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.alertView closeAlertView];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(photoView:didCompleteFetchAssets:originalAssets:)]) {
                [self.delegate photoView:self didCompleteFetchAssets:results originalAssets:self.selectedAssets];
            }
        });
    });
}

// =============== YYBPhotoCollectionsViewDelegate
- (void)didTapedFetchResult:(PHFetchResult *)fetchResult assetCollection:(PHAssetCollection *)assetCollection
{
    _assetCollection = assetCollection;
    _fetchResult = fetchResult;
    
    [_navigationSelectionView handlePhotoSelectionStatus];
    [self refreshNavigatioSelectionView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

// 获取所有的相册
- (void)queryPhotoCollectionsAction
{
    PHFetchOptions *option = [[PHFetchOptions alloc] init];
    option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    NSMutableArray *assetCollections = [NSMutableArray new];
    
    // Videos,Bursts,Hidden,Camera Roll,Selfies,Panoramas,ecently Deleted,Time-lapse,Favorites,Recently Added,Slo-mo,Screenshots,Portrait,Live Photos,Animated,Long Exposure
    
    // PHAssetCollectionTypeSmartAlbum 系统自己生成的相册
    // PHAssetCollectionTypeAlbum 应用生成的相册
    PHFetchResult *fetchResults = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    
    for (NSInteger index = 0; index < fetchResults.count; index ++) {
        PHAssetCollection *assetCollection = [fetchResults objectAtIndex:index];
        
        if (assetCollection.assetCollectionSubtype == _requiredSubtype) {
            [assetCollections insertObject:assetCollection atIndex:0];
            
            PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:option];
            [self didTapedFetchResult:fetchResult assetCollection:assetCollection];
        } else if (assetCollection.assetCollectionSubtype == PHAssetCollectionSubtypeSmartAlbumAllHidden) {
            
        } else {
            PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
            if (result.count != 0) {
                [assetCollections addObject:assetCollection];
            }
        }
    }
    
    _assetCollections = assetCollections;
    _collectionsView.assetCollections = assetCollections;
}

// ======================== YYBPhotoAssetCellDelegate

- (NSInteger)selectedIndexWithAsset:(PHAsset *)asset
{
    if (![_selectedAssets containsObject:asset]) {
        return 0;
    }
    return [_selectedAssets indexOfObject:asset] + 1;
}

- (BOOL)isAppendVideoAssetEnableWithAsset:(PHAsset *)asset
{
    // 如果已经有图片选择 则不允许选择
    if (_selectedAssets.count > 0) {
        return NO;
    }
    return YES;
}

- (BOOL)isAppendPhotoAssetEnableWithAsset:(PHAsset *)asset
{
    // 如果选择的图片已经是最大数目 则不允许选择
    if (_selectedAssets.count >= _maxSelectAssetsCount) {
        return NO;
    }
    return YES;
}

- (void)didSelectedAssetWithAsset:(PHAsset *)asset
{
    if ([_selectedAssets containsObject:asset]) {
        [_selectedAssets removeObject:asset];
    } else {
        // 如果选择的图片已经是最大数目 则不允许选择
        if (_selectedAssets.count >= _maxSelectAssetsCount) {
            return;
        }
        
        [_selectedAssets addObject:asset];
    }
    
    // 发送通知 刷新选择状态
    [[NSNotificationCenter defaultCenter] postNotificationName:YYBPhotoViewSelectNotification object:nil];
    // 改变Toolbar
    [_selectionsView refreshViewWithImagesCount:_selectedAssets.count];
}

- (BOOL)isAppendMultiAssetsEnable
{
    return _selectionStyle != YYBPhotoViewSelectionStyleOnlyOneAsset;
}

// ======================== YYBPhotoAssetCellDelegate END

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat size = (CGRectGetWidth(self.view.frame) - 3) / 4;
    return CGSizeMake(size, size);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YYBPhotoAssetCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YYBPhotoAssetCell" forIndexPath:indexPath];
    
    PHAsset *asset = [_fetchResult objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    [cell refreshViewWithAsset:asset];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _fetchResult.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectionStyle == YYBPhotoViewSelectionStyleOnlyOneAsset) {
        PHAsset *asset = [_fetchResult objectAtIndex:indexPath.row];
        _selectedAssets = [NSMutableArray arrayWithObject:asset];
        
        [self handleProcessAssetsAction];
    } else {
        // 预览 TODO
        NSMutableArray *dataModels = [NSMutableArray new];
        
        for (NSInteger index = 0; index < _fetchResult.count; index ++) {
            YYBAdjustedImageModel *dataModel = [[YYBAdjustedImageModel alloc] init];
            dataModel.imageAsset = [_fetchResult objectAtIndex:index];
            
            [dataModels addObject:dataModel];
        }
        
        [self showImageBrowserWithImageModels:dataModels selectedIndex:indexPath.row];
    }
}

// ========================
// 自定义界面信息

- (UIView *)customNavigationBarTitleView
{
    _navigationSelectionView = [[YYBPhotoNavigationSelectionView alloc] init];
    _navigationSelectionView.delegate = self;
    
    return _navigationSelectionView;
}

- (void)refreshNavigatioSelectionView
{
    NSString *text = self.assetCollection.localizedTitle;
    
    _navigationSelectionView.collectionTextLabel.text = text;
    
    CGSize preferSize = [_navigationSelectionView preferSizeWithCollectionTextValue:text];
    
    CGFloat y = ([self customPageHeaderHeight] - self.navigationBar.contentViewTopEdgeInset - preferSize.height) / 2;
    _navigationSelectionView.frame = CGRectMake((CGRectGetWidth(self.view.frame) - preferSize.width) / 2, y,
                                                preferSize.width, preferSize.height);
}

- (void)photoSelectionViewWithStatus:(BOOL)status
{
    self.backgroundContentView.hidden = NO;
    [UIView animateWithDuration:0.25f animations:^{
        if (status) {
            self.backgroundContentView.alpha = 1;
            self.collectionsView.transform = CGAffineTransformMakeTranslation(0, CGRectGetHeight(self.collectionsView.frame));
        } else {
            self.backgroundContentView.alpha = 0;
            self.collectionsView.transform = CGAffineTransformIdentity;
        }
    } completion:^(BOOL finished) {
        if (status == NO) {
            self.backgroundContentView.hidden = YES;
        }
    }];
}

- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar
{
    [super configCustomNavigationBar:navigationBar];
    
    navigationBar.bottomLayerView.backgroundColor = [UIColor clearColor];
}

- (void)handleBackBarButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (YYBReturnButtonStyle)returnButtonStyle
{
    return YYBReturnButtonStyleDismissWhite;
}

- (UIColor *)customPageHeaderTintColor
{
    return [UIColor colorWithHexValue:0x222831];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIEdgeInsets)handleEdgeInsets
{
    UIEdgeInsets insets = [super handleEdgeInsets];
    if (_selectionStyle == YYBPhotoViewSelectionStyleOnlyOneAsset) {
        return insets;
    } else {
        insets.bottom += 50;
        return insets;
    }
}

@end
