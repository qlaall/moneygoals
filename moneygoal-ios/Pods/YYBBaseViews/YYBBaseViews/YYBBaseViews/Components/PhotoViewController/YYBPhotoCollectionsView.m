//
//  YYBPhotoCollectionsView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPhotoCollectionsView.h"

@interface YYBPhotoCollectionsView ()

@end

@implementation YYBPhotoCollectionsView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _tableView = [UITableView createAtSuperView:self delagateBlock:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } dequeueCellIdentifiers:[self registerCellNames] configureBlock:nil];
    
    return self;
}

- (NSArray *)registerCellNames
{
    return @[@"YYBPhotoCollectionsCell"];
}

- (void)setAssetCollections:(NSMutableArray *)assetCollections
{
    _assetCollections = assetCollections;
    [_tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _assetCollections.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YYBPhotoCollectionsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YYBPhotoCollectionsCell"];
    cell.collection = [_assetCollections objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PHAssetCollection *collection = [_assetCollections objectAtIndex:indexPath.row];
    
    PHFetchOptions *option = [[PHFetchOptions alloc] init];
    option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:collection options:option];
    
    if ([self.delegate respondsToSelector:@selector(didTapedFetchResult:assetCollection:)]) {
        [self.delegate didTapedFetchResult:result assetCollection:collection];
    }
}

@end
