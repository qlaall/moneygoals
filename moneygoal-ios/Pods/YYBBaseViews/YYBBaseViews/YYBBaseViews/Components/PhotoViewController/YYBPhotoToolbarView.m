//
//  YYBPhotoToolbarView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/11/3.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPhotoToolbarView.h"

@interface YYBPhotoToolbarView ()

@end

@implementation YYBPhotoToolbarView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor colorWithHexValue:0x222831];
    
    @weakify(self);
    _finishButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(- 15);
        make.size.mas_equalTo(CGSizeMake(80, 34));
        make.top.equalTo(self).offset(10);
    } configureBlock:^(UIButton *button) {
        button.backgroundColor = [UIColor colorWithHexValue:0x393e46];
        [button setTitle:@"使用" forState:0];
        button.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
        [button cornerRadius:17];
    } tapedBlock:^(UIButton *sender) {
        @strongify(self);
        if (self.finishSelectedBlock) {
            self.finishSelectedBlock();
        }
    }];
    
    return self;
}

- (void)refreshViewWithImagesCount:(NSInteger)count
{
    if (count == 0) {
        _finishButton.enabled = NO;
        [_finishButton setTitle:@"使用" forState:0];
    } else {
        _finishButton.enabled = TRUE;
        [_finishButton setTitle:[NSString stringWithFormat:@"使用 (%@)",@(count).stringValue] forState:0];
    }
}

@end
