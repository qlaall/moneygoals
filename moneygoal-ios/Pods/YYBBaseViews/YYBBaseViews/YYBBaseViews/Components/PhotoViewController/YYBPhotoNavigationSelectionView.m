//
//  YYBPhotoNavigationSelectionView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/11/3.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPhotoNavigationSelectionView.h"

@interface YYBPhotoNavigationSelectionView ()
@property (nonatomic, strong) UIButton *actionButton;

@end

@implementation YYBPhotoNavigationSelectionView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor colorWithHexValue:0x393e46];
    [self cornerRadius:15];
    
    _switchIconView = [UIImageView createAtSuperView:self iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-5);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    } configureBlock:^(UIImageView *view) {
        view.image = [NSBundle imageWithBundleName:@"Icon_PhotoViewController" imageName:@"header_switch"];
    }];
    
    _collectionTextLabel = [UILabel createAtSuperView:self fontValue:[UIFont systemFontOfSize:16 weight:UIFontWeightMedium] textColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.switchIconView);
        make.left.equalTo(self).offset(13);
    } configureBlock:nil];
    
    __weak typeof(self) weakself = self;
    _actionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } configureBlock:^(UIButton *view) {
        
    } tapedBlock:^(UIButton *view) {
        [weakself handlePhotoSelectionStatus];
    }];
    
    return self;
}

- (void)handlePhotoSelectionStatus
{
    BOOL notSelected = CGAffineTransformEqualToTransform(CGAffineTransformIdentity, self.switchIconView.transform);
    
    [UIView animateWithDuration:0.2f animations:^{
        if (notSelected) {
            self.switchIconView.transform = CGAffineTransformMakeRotation(M_PI);
        } else {
            self.switchIconView.transform = CGAffineTransformIdentity;
        }
    }];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(photoSelectionViewWithStatus:)]) {
        [self.delegate photoSelectionViewWithStatus:!notSelected];
    }
}

- (CGSize)preferSizeWithCollectionTextValue:(NSString *)textValue
{
    CGSize size = [textValue sizeWithFont:_collectionTextLabel.font lineSpacing:0 size:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    return CGSizeMake(13 + size.width + 7 + 20 + 5, 30);
}

@end
