//
//  YYBPhotoCollectionsView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "YYBPhotoCollectionsCell.h"
#import "YYBCategory.h"

@protocol YYBPhotoCollectionsViewDelegate <NSObject>

- (void)didTapedFetchResult:(PHFetchResult *)fetchResult assetCollection:(PHAssetCollection *)assetCollection;

@end

@interface YYBPhotoCollectionsView : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id<YYBPhotoCollectionsViewDelegate> delegate;

@property (nonatomic, strong) UITableView *tableView;
- (NSArray *)registerCellNames;

@property (nonatomic, strong) NSMutableArray *assetCollections;

@end
