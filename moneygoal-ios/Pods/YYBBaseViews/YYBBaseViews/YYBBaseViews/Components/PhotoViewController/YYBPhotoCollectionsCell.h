//
//  YYBPhotoCollectionsCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <Masonry/Masonry.h>
#import <ReactiveObjC/RACEXTScope.h>
#import "PHAsset+YYBPhotoViewController.h"
#import "YYBCategory.h"

@interface YYBPhotoCollectionsCell : UITableViewCell

@property (nonatomic,strong) PHAssetCollection *collection;

@property (nonatomic,strong) UIImageView *iconView;
@property (nonatomic,strong) UILabel *albumNameLabel;

@end
