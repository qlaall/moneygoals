//
//  PHAsset+YYBPhoto.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "PHAsset+YYBPhotoViewController.h"

@implementation PHAsset (YYBPhotoViewController)

- (void)thumbnailImageWithTargetSize:(CGSize)targetSize completionBlock:(void (^)(UIImage *))completionBlock
{
    if (CGSizeEqualToSize(targetSize, CGSizeZero)) {
        targetSize = CGSizeMake(self.pixelWidth,self.pixelHeight);
    }
    
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    options.networkAccessAllowed = YES;
    
    [[PHImageManager defaultManager] requestImageForAsset:self targetSize:targetSize contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        
        BOOL finished = ![[info objectForKey:PHImageCancelledKey] boolValue] &&
        ![info objectForKey:PHImageErrorKey] &&
        ![[info objectForKey:PHImageResultIsDegradedKey] boolValue] &&
        result;
        
        if (finished && completionBlock) {
            completionBlock(result);
        }
    }];
}

- (void)queryImageWithCompletionBlock:(void (^)(UIImage *))completionBlock
{
    CGSize targetSize = CGSizeMake(self.pixelWidth, self.pixelHeight);
    
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    options.networkAccessAllowed = YES;
    
    [[PHImageManager defaultManager] requestImageForAsset:self targetSize:targetSize contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        
        BOOL finished = ![[info objectForKey:PHImageCancelledKey] boolValue] &&
        ![info objectForKey:PHImageErrorKey] &&
        ![[info objectForKey:PHImageResultIsDegradedKey] boolValue] &&
        result;
        
        if (finished && completionBlock) {
            completionBlock(result);
        }
    }];
}

@end
