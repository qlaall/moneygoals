//
//  YYBPhotoAssetCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPhotoAssetCell.h"

@implementation YYBPhotoAssetCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    _contentImageView = [UIImageView createAtSuperView:self.contentView iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } configureBlock:^(UIImageView *view) {
        view.backgroundColor = [UIColor colorWithHexValue:0xF6F6F6];
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.clipsToBounds = YES;
    }];
    
    _bannedBackgroundView = [UIView createViewAtSuperView:self.contentView backgroundColor:[UIColor colorWithHexValue:0xffffff alpha:0.6f] constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } configureBlock:^(UIView *view) {
        view.hidden = YES;
    }];
    
    _videoBackgroundLayer = [CAGradientLayer layer];
    _videoBackgroundLayer.startPoint = CGPointMake(0.5f, 0);
    _videoBackgroundLayer.endPoint = CGPointMake(0.5f, 1);
    _videoBackgroundLayer.colors = @[(__bridge id)[UIColor clearColor].CGColor,
                                     (__bridge id)[UIColor colorWithHexValue:0x000000 alpha:0.3f].CGColor];
    
    _videoBackgroundLayer.frame = CGRectMake(0, CGRectGetHeight(frame) - 30, CGRectGetWidth(frame), 30);
    [self.contentView.layer addSublayer:_videoBackgroundLayer];
    
    __weak typeof(self) weakself = self;
    _selectedStatusButton = [UIButton createAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-3);
        make.top.equalTo(self.contentView).offset(3);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    } configureBlock:^(UIButton *button) {
        [button cornerRadius:15 width:2 color:[UIColor whiteColor]];
        
        button.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [button setBackgroundImage:[UIColor clearColor].colorToUIImage forState:0];
        [button setBackgroundImage:[UIColor colorWithHexValue:0x50d890].colorToUIImage forState:UIControlStateSelected];
        [button setTitleColor:[UIColor whiteColor] forState:0];
    } tapedBlock:^(UIButton *sender) {
        if (weakself.delegate && [weakself.delegate respondsToSelector:@selector(didSelectedAssetWithAsset:)]) {
            [weakself.delegate didSelectedAssetWithAsset:weakself.asset];
        }
    }];
    
    _videoImageButton = [UIButton createAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(30);
    } configureBlock:^(UIButton *button) {
        button.userInteractionEnabled = NO;
        button.titleLabel.font = [UIFont systemFontOfSize:12];
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        UIImage *videoImage = [NSBundle imageWithBundleName:@"Icon_PhotoViewController" imageName:@"video"];
        [button setImage:videoImage forState:0];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChangeSelectedAsset) name:@"YYBPhotoViewSelectNotification" object:nil];
    
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _contentImageView.image = [UIImage new];
}

- (void)refreshViewWithAsset:(PHAsset *)asset
{
    _asset = asset;
    _assetImage = nil;
    
    CGFloat size = ([UIScreen mainScreen].bounds.size.width - 4 * 4) / 3;
    
    __weak typeof(self) weakself = self;
    [asset thumbnailImageWithTargetSize:CGSizeMake(size * 3, size * 3) completionBlock:^(UIImage *thumbnailImage) {
        weakself.contentImageView.image = thumbnailImage;
        weakself.assetImage = thumbnailImage;
    }];
    
    BOOL isAppendPhotoAssetEnable = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(isAppendPhotoAssetEnableWithAsset:)]) {
        isAppendPhotoAssetEnable = [self.delegate isAppendPhotoAssetEnableWithAsset:asset];
    }
    
    BOOL isAppendVideoAssetEnable = YES;
    if (asset.mediaType == PHAssetMediaTypeVideo) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(isAppendVideoAssetEnableWithAsset:)]) {
            isAppendVideoAssetEnable = [self.delegate isAppendVideoAssetEnableWithAsset:asset];
        }
    }
    
    BOOL isAppendMultiAssetsEnable = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(isAppendMultiAssetsEnable)]) {
        isAppendMultiAssetsEnable = [self.delegate isAppendMultiAssetsEnable];
    }
    
    if (asset.mediaType == PHAssetMediaTypeVideo) {
        _bannedBackgroundView.hidden = isAppendVideoAssetEnable;
        _selectedStatusButton.hidden = YES;
        _videoImageButton.hidden = NO;
        
        NSInteger minutes = asset.duration / 60;
        NSInteger seconds = (int)asset.duration % 60;
        NSString *durationString = [NSString stringWithFormat:@"%02d:%02d",(int)minutes, (int)seconds];
        [_videoImageButton setTitle:durationString forState:0];
        
    } else {
        _bannedBackgroundView.hidden = isAppendPhotoAssetEnable;
        _videoImageButton.hidden = YES;
        _selectedStatusButton.hidden = !isAppendMultiAssetsEnable;
    }
    
    [self handleChangeSelectedAsset];
}

- (void)handleCheckSelectedStatus
{
    NSInteger index = 0;
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedIndexWithAsset:)]) {
        index = [self.delegate selectedIndexWithAsset:_asset];
    }
    
    if (index == 0) {
        _selectedStatusButton.selected = NO;
        [_selectedStatusButton setTitle:nil forState:0];
    } else {
        _selectedStatusButton.selected = YES;
        [_selectedStatusButton setTitle:@(index).stringValue forState:0];
    }
}

- (void)blockefreshBannedStatus
{
    BOOL isAppendPhotoAssetEnable = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(isAppendPhotoAssetEnableWithAsset:)]) {
        isAppendPhotoAssetEnable = [self.delegate isAppendPhotoAssetEnableWithAsset:_asset];
    }
    
    BOOL isAppendVideoAssetEnable = YES;
    if (_asset.mediaType == PHAssetMediaTypeVideo) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(isAppendVideoAssetEnableWithAsset:)]) {
            isAppendVideoAssetEnable = [self.delegate isAppendVideoAssetEnableWithAsset:_asset];
        }
    }
    
    if (_asset.mediaType == PHAssetMediaTypeVideo) {
        _bannedBackgroundView.hidden = isAppendVideoAssetEnable;
    } else {
        _bannedBackgroundView.hidden = isAppendPhotoAssetEnable;
    }
}

- (void)handleChangeSelectedAsset
{
    [self handleCheckSelectedStatus];
    [self blockefreshBannedStatus];
}

@end
