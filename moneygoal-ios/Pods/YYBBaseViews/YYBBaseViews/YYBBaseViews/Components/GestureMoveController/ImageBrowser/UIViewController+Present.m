//
//  UIViewController+YYBImageBrowserController.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import "UIViewController+Present.h"

@implementation UIViewController (Present)

- (void)showImageBrowserWithImageModels:(NSMutableArray *)imageModels selectedIndex:(NSInteger)selectedIndex
{
    YYBGestureMoveTransition *transition = [[YYBGestureMoveTransition alloc] init];
    transition.imageModel = [imageModels objectAtIndex:selectedIndex];
    
    YYBImageBrowserController *vc = [[YYBImageBrowserController alloc] init];
    vc.initialIndex = selectedIndex;
    vc.dataModels = imageModels;
    vc.placeholderImageModel = [imageModels objectAtIndex:selectedIndex];
    vc.transition = transition;
    vc.transitioningDelegate = transition;
    
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showImageBrowserWithImagePath:(NSString *)imagePath
{
    YYBAdjustedImageModel *dataModel = [[YYBAdjustedImageModel alloc] init];
    dataModel.imageURL = imagePath;
    
    YYBGestureMoveTransition *transition = [[YYBGestureMoveTransition alloc] init];
    transition.imageModel = dataModel;
    
    YYBImageTapedBrowserController *vc = [[YYBImageTapedBrowserController alloc] init];
    
    vc.initialIndex = 0;
    vc.dataModels = [NSMutableArray arrayWithObject:dataModel];
    vc.placeholderImageModel = dataModel;
    vc.transition = transition;
    vc.transitioningDelegate = transition;
    
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
