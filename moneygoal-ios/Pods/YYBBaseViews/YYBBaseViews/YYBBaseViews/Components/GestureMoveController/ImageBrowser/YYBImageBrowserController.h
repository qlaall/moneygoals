//
//  YYBImageBrowserController.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBGestureMoveController.h"
#import "YYBPageControl.h"

NS_ASSUME_NONNULL_BEGIN

@class YYBImageBrowserController;

@protocol YYBImageBrowserControllerDelegate <NSObject>

- (void)imageBrowser:(YYBImageBrowserController *)imageBrowser didChangedScrollIndex:(NSInteger)scrollIndex;

@end

@interface YYBImageBrowserController : YYBGestureMoveController

@property (nonatomic, weak) id<YYBImageBrowserControllerDelegate> imageBrowserDelegate;

@property (nonatomic, strong) NSMutableArray *dataModels;
@property (nonatomic) NSInteger initialIndex;

// 是否显示 <...>
@property (nonatomic) BOOL isPreferDashHidden;

@end

NS_ASSUME_NONNULL_END
