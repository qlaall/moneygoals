//
//  YYBImageBrowserController.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBImageBrowserController.h"
#import "YYBImageBrowserCollectionViewCell.h"

@interface YYBImageBrowserController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) YYBPageControl *pageControlView;

@end

@implementation YYBImageBrowserController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _isPreferDashHidden = YES;
    
    return self;
}

- (void)beforeConfigViewAction
{
    [super beforeConfigViewAction];
    
    _collectionView = [UICollectionView createAtSuperView:self.containerView delegeteTarget:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    } dequeueCellIdentifiers:@[@"YYBImageBrowserCollectionViewCell"] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.itemSize = self.view.frame.size;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        view.pagingEnabled = YES;
        view.backgroundColor = [UIColor blackColor];
        view.showsHorizontalScrollIndicator = NO;
    }];

    if (!_isPreferDashHidden) {
        _pageControlView = [YYBPageControl createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice safeAreaInsetsBottom]-20);
            make.height.mas_equalTo(8);
        } configureBlock:^(YYBPageControl *view) {
            view.currentPageIndicatorColor = [UIColor whiteColor];
            view.othersPageIndicatorColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2f];
            view.sizeForPageIndicator = CGSizeMake(8, 8);
            view.pageItemPadding = 9;
        }];
    }
}

- (void)beginQueryData
{
    [super beginQueryData];
    
    [_collectionView reloadData];
    
    [self performSelector:@selector(scrollProperPosition) withObject:nil afterDelay:0.1f];
    
    if (!_isPreferDashHidden) {
        NSInteger count = self.dataModels.count;
        
        if (count > 0) {
            _pageControlView.numbersOfPages = count;
            _pageControlView.currentPage = _initialIndex;
        } else {
            _pageControlView.hidden = YES;
        }
    }
    
    if (self.dataModels.count > self.initialIndex) {
        self.placeholderImageModel = [self.dataModels objectAtIndex:self.initialIndex];
    }
}

- (void)scrollProperPosition
{
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.initialIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
    _pageControlView.currentPage = index;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    if (self.imageBrowserDelegate && [self.imageBrowserDelegate respondsToSelector:@selector(imageBrowser:didChangedScrollIndex:)]) {
        [self.imageBrowserDelegate imageBrowser:self didChangedScrollIndex:index];
    }
    
    if (self.dataModels.count > index) {
        self.placeholderImageModel = [self.dataModels objectAtIndex:index];
        self.adjustedImageView.dataModel = self.placeholderImageModel;
        self.transition.imageModel = self.placeholderImageModel;
    }
}

- (void)dismissTapBlock
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataModels.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YYBImageBrowserCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YYBImageBrowserCollectionViewCell" forIndexPath:indexPath];
    
    cell.contentIconView.dataModel = [self.dataModels objectAtIndex:indexPath.row];
    return cell;
}

@end
