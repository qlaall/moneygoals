//
//  YYBGestureMoveController.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBGestureMoveController.h"

@interface YYBGestureMoveController ()
@property (nonatomic, strong) UIPanGestureRecognizer *pan;
@property (nonatomic, strong) UIView *gestureHandleView;

@end

@implementation YYBGestureMoveController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isHiddenNavigationReturnButton = YES;
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)beforeConfigViewAction {
    [super beforeConfigViewAction];
    
    _containerView = [UIView new];
    _containerView.backgroundColor = [UIColor blackColor];
    _containerView.frame = self.view.bounds;
    [self.view addSubview:_containerView];
    
    _gestureHandleView = [[UIView alloc] init];
    _gestureHandleView.frame = self.view.bounds;
    _gestureHandleView.backgroundColor = [UIColor blackColor];
    _gestureHandleView.hidden = YES;
    [self.view addSubview:_gestureHandleView];
    
    _adjustedImageView = [YYBAdjustedImageView new];
    _adjustedImageView.hidden = YES;
    [self.view addSubview:_adjustedImageView];
}

- (void)beginQueryData {
    [super beginQueryData];
    
    if (!_invalidPanGesture) {
        _pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        [self.containerView addGestureRecognizer:_pan];
    }
    
    _transition.pan = nil;
    _adjustedImageView.dataModel = _placeholderImageModel;
}

- (void)setPlaceholderImageModel:(YYBAdjustedImageModel *)placeholderImageModel {
    _placeholderImageModel = placeholderImageModel;
    
    _placeholderImageModel.movedEndRect = _placeholderImageModel.endRect;
    _adjustedImageView.dataModel = placeholderImageModel;
    _transition.imageModel = placeholderImageModel;
}

- (void)handlePan:(UIPanGestureRecognizer *)pan {
    CGPoint translation = [pan translationInView:pan.view];
    
    CGFloat scale = 1 - (translation.y / CGRectGetHeight(self.view.frame));
    scale = scale < 0 ? 0 : scale;
    scale = scale > 1 ? 1 : scale;
    
    switch (pan.state) {
        case UIGestureRecognizerStateBegan: {
            _transition.pan = pan;
            _adjustedImageView.hidden = NO;
            
            [self handlePanGestureBegin];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
            break;
        case UIGestureRecognizerStateChanged: {
            [self handlePanGestureChangedWithScale:scale offset:translation];
        }
            break;
        case UIGestureRecognizerStateEnded: {
            [self handlePanGestureEndWithScale:scale];
        }
            break;
        default:
            break;
    }
}

- (void)handlePanGestureBegin {
    _gestureHandleView.hidden = NO;
    _containerView.hidden = YES;
    
    _adjustedImageView.frame = _placeholderImageModel.endRect;
}

- (void)handlePanGestureChangedWithScale:(CGFloat)scale offset:(CGPoint)offset {
    CGFloat x = CGRectGetMidX(self.view.frame);
    CGFloat y = CGRectGetMidY(self.view.frame);
    
    _adjustedImageView.center = CGPointMake(x + offset.x * scale, y + offset.y);
    _adjustedImageView.transform = CGAffineTransformMakeScale(scale, scale);
    
    _gestureHandleView.alpha = scale;
    _placeholderImageModel.movedEndRect = _adjustedImageView.frame;
}

// 当移动距离达到阈值，占位的图需要移动到开始的位置，然后隐藏
- (void)handlePanGestureEndWithScale:(CGFloat)scale {
    if (scale > 0.95f) {
        [UIView animateWithDuration:0.25f animations:^{
            self.adjustedImageView.frame = self.placeholderImageModel.endRect;
            self.adjustedImageView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            self.adjustedImageView.hidden = YES;
            [self handlePanGestureEndAnimation];
        }];
    }
}

- (void)handlePanGestureEndAnimation {
    _gestureHandleView.hidden = YES;
    _containerView.hidden = NO;
}

- (UIColor *)customPageHeaderTintColor {
    return [UIColor blackColor];
}

- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar {
    [super configCustomNavigationBar:navigationBar];
    self.navigationBar.bottomLayerView.backgroundColor = [UIColor clearColor];
    self.navigationBar.alpha = 0;
}

@end
