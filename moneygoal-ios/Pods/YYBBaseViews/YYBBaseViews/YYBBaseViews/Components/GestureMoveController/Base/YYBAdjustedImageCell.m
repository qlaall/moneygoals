//
//  YYBAdjustedImageCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import "YYBAdjustedImageCell.h"

@implementation YYBAdjustedImageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    _iconView = [[YYBAdjustedImageView alloc] init];
    _iconView.aspectMode = YYBImageAdjustedTypeAspectFill;
    _iconView.frame = self.bounds;
    
    [self.contentView addSubview:_iconView];
    
    return self;
}

@end
