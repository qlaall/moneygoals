//
//  YYBGestureMovePercentTransition.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBAdjustedImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBGestureMovePercentTransition : UIPercentDrivenInteractiveTransition

@property (nonatomic,strong) UIPanGestureRecognizer *pan;

@property (nonatomic, strong) YYBAdjustedImageModel *imageModel;

@end

NS_ASSUME_NONNULL_END
