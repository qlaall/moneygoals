//
//  YYBGestureMovePush.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBGestureMovePush.h"

@interface YYBGestureMovePush ()
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) YYBAdjustedImageView *adjustedImageView;

@end

@implementation YYBGestureMovePush

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _adjustedImageView = [[YYBAdjustedImageView alloc] init];
    _adjustedImageView.aspectMode = YYBImageAdjustedTypeNone;
    
    _backgroundView = [[UIView alloc] init];
    _backgroundView.backgroundColor = [UIColor blackColor];
    
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.25f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *contentView = transitionContext.containerView;
    
    UIViewController *targetView = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    targetView.view.hidden = YES;
    [contentView addSubview:targetView.view];
    
    _backgroundView.frame = targetView.view.bounds;
    _backgroundView.alpha = 0;
    [contentView addSubview:_backgroundView];
    
    _adjustedImageView.frame = targetView.view.frame;
    _adjustedImageView.dataModel = self.imageModel;
    _adjustedImageView.imageView.frame = self.imageModel.beginRect;
    [contentView addSubview:_adjustedImageView];

    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        self.backgroundView.alpha = 1;
        self.adjustedImageView.imageView.frame = self.imageModel.endRect;
    } completion:^(BOOL finished) {
        [self.backgroundView removeFromSuperview];
        [self.adjustedImageView removeFromSuperview];
        targetView.view.hidden = NO;
        
        [transitionContext completeTransition:YES];
    }];
}

@end
