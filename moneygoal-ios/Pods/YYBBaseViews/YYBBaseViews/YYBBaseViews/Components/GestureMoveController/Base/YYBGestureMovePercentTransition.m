//
//  YYBGestureMovePercentTransition.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBGestureMovePercentTransition.h"

@interface YYBGestureMovePercentTransition ()
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) YYBAdjustedImageView *adjustedImageView;

@property (nonatomic, weak) id<UIViewControllerContextTransitioning> transitionContext;

@end

@implementation YYBGestureMovePercentTransition

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _adjustedImageView = [[YYBAdjustedImageView alloc] init];
    _adjustedImageView.aspectMode = YYBImageAdjustedTypeNone;
    
    _backgroundView = [[UIView alloc] init];
    _backgroundView.backgroundColor = [UIColor blackColor];
    
    return self;
}

- (void)setPan:(UIPanGestureRecognizer *)pan {
    _pan = pan;
    [_pan addTarget:self action:@selector(recogizerDidUpdate:)];
}

- (CGFloat)scaleOfRecogzier {
    CGPoint translation = [_pan translationInView:_pan.view];
    CGFloat scale = 1 - (translation.y / [UIScreen mainScreen].bounds.size.height);
    scale = scale < 0 ? 0 : scale;
    scale = scale > 1 ? 1 : scale;
    return scale;
}

- (void)recogizerDidUpdate:(UIPanGestureRecognizer *)recognizer {
    CGFloat percent = [self scaleOfRecogzier];
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            
        }
            break;
        case UIGestureRecognizerStateChanged: {
            [self updateInteractiveTransition:percent];
        }
            break;
        case UIGestureRecognizerStateEnded: {
            if (percent > 0.95f) {
                [self cancelInteractiveTransition];
                [self cancelTransition:percent];
            } else {
                [self finishInteractiveTransition];
                [self finishTransition:percent];
            }
        }
            break;
        default: {
            [self cancelInteractiveTransition];
            [self cancelTransition:percent];
        }
            break;
    }
}

- (void)startInteractiveTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    _transitionContext = transitionContext;

    UIView *contentView = _transitionContext.containerView;

    UIViewController *targetView = [_transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    [contentView addSubview:targetView.view];

    _backgroundView.frame = targetView.view.bounds;
    _backgroundView.alpha = 0;
    [contentView addSubview:_backgroundView];

    UIViewController *sourceView = [_transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [contentView addSubview:sourceView.view];
}

- (void)cancelTransition:(CGFloat)percent {
    [self.transitionContext completeTransition:![self.transitionContext transitionWasCancelled]];
}

- (void)finishTransition:(CGFloat)percent {
    UIView *contentView = _transitionContext.containerView;
    
    UIViewController *targetView = [_transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    [contentView addSubview:targetView.view];
    
    _backgroundView.alpha = percent;
    [contentView addSubview:_backgroundView];
    
    _adjustedImageView.dataModel = _imageModel;
    _adjustedImageView.frame = targetView.view.bounds;
    _adjustedImageView.imageView.frame = _imageModel.movedEndRect;
    [contentView addSubview:_adjustedImageView];
    
    [UIView animateWithDuration:0.25f animations:^{
        self.backgroundView.alpha = 0;
        if (!CGRectEqualToRect(CGRectZero, self.imageModel.beginRect)) {
            self.adjustedImageView.imageView.frame = self.imageModel.beginRect;
        }
    } completion:^(BOOL finished) {
        [self.backgroundView removeFromSuperview];
        [self.adjustedImageView removeFromSuperview];
        
        [self.transitionContext completeTransition:![self.transitionContext transitionWasCancelled]];
    }];
}

@end
