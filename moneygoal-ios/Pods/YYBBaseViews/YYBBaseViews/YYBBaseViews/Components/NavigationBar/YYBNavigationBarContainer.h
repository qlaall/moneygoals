//
//  YYBNavigationBarContainer.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBNavigationBarBadgeView.h"

@interface YYBNavigationBarContainer : UIControl

@property (nonatomic, strong) UIView *contentView;

// Navibar与container的间距
@property (nonatomic) UIEdgeInsets contentEdgeInsets;

// ContentView与边距的距离, 会影响ContentView尺寸大小
@property (nonatomic) UIEdgeInsets contentPadding;
// ContentView与边距的偏移, 不会ContentView影响尺寸大小
@property (nonatomic) UIEdgeInsets contentMargin;

// ===================
// ContentView中的子控件与ContentView相对间距
@property (nonatomic) UIEdgeInsets containerPadding;
@property (nonatomic) UIEdgeInsets containerMargin;


@property (nonatomic) CGSize contentSize;

@property (nonatomic, copy) void (^ tapedActionBlock)(YYBNavigationBarContainer *view);

// ======== badgeValue
@property (nonatomic, strong) YYBNavigationBarBadgeView *badgeView;

@end
