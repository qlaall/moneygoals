//
//  YYBNavigationBarImageView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarContainer.h"

@interface YYBNavigationBarImageView : YYBNavigationBarContainer

@property (nonatomic, strong) UIImageView *imageView;

@end
