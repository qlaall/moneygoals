//
//  YYBNavigationBarButton.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarContainer.h"

@interface YYBNavigationBarButton : YYBNavigationBarContainer

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, copy) void (^ barButtonTapedBlock)(YYBNavigationBarContainer *view);

@end
