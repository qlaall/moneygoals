//
//  YYBBaseViews.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBNavigationBarContainer+Create.h"
#import "UIDevice+YYBAdd.h"

@interface YYBNavigationBar : UIView

@property (nonatomic,strong) UIView *shadowView;
@property (nonatomic,strong) UIImageView *contentView;
@property (nonatomic,strong) UIView *containerView;
@property (nonatomic,strong) UIView *bottomLayerView;

@property (nonatomic) CGFloat contentViewTopEdgeInset;

@property (nonatomic,strong) YYBNavigationBarLabel *titleBarLabel;
@property (nonatomic,strong) UIView *titleCustomView;

@property (nonatomic,strong) NSArray *leftBarContainers;
@property (nonatomic,strong) NSArray *rightBarContainers;

// 默认0.5f
@property (nonatomic) CGFloat heightForBottomView;

@end
