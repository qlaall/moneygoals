//
//  YYBNavigationBarContainer+Create.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarContainer.h"
#import "YYBNavigationBarImageView.h"
#import "YYBNavigationBarLabel.h"
#import "YYBNavigationBarButton.h"
#import "YYBNavigationBarControl.h"

@interface YYBNavigationBarContainer (Create)

+ (YYBNavigationBarLabel *)labelWithConfigureBlock:(void (^)(YYBNavigationBarLabel *container, UILabel *view))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *view))tapedActionBlock;

+ (YYBNavigationBarImageView *)imageViewWithConfigureBlock:(void (^)(YYBNavigationBarImageView *container, UIImageView *view))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *container))tapedActionBlock;

+ (YYBNavigationBarButton *)buttonWithConfigureBlock:(void (^)(YYBNavigationBarButton *container, UIButton *view))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *view))tapedActionBlock;

+ (YYBNavigationBarControl *)controlWithConfigureBlock:(void (^)(YYBNavigationBarControl *container, UIImageView *iconView, UILabel *label))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *view))tapedActionBlock;

@end
