//
//  YYBNavigationBarBadgeView.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/5/16.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YYBNavigationBarBadgePosition)
{
    YYBNavigationBarBadgePositionCenter = 0,
    YYBNavigationBarBadgePositionRight = 1,
    YYBNavigationBarBadgePositionLeft = 2,
    YYBNavigationBarBadgePositionCenterTop = 3,
};

NS_ASSUME_NONNULL_BEGIN

@interface YYBNavigationBarBadgeView : UIView

@property (nonatomic) NSInteger badgeValue;
@property (nonatomic) UIEdgeInsets badgeMargin;
@property (nonatomic, strong) NSMutableDictionary *badgeValueAttrs;

@property (nonatomic) UIEdgeInsets badgePadding;
@property (nonatomic) YYBNavigationBarBadgePosition position;

@end

NS_ASSUME_NONNULL_END
