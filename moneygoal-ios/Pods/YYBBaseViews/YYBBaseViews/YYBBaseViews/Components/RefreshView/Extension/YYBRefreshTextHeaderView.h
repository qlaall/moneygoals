//
//  YYBRefreshTextHeaderView.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"
#import <Masonry/Masonry.h>

static const NSString * BTRV_TOP_INITIALIZE = @"下拉刷新";
static const NSString * BTRV_TOP_PULLING = @"松开刷新";
static const NSString * BTRV_TOP_REFRESHING = @"正在刷新";

@interface YYBRefreshTextHeaderView : YYBRefreshBaseHeaderView

- (void)renderViewWithTitle:(NSString *)title status:(YYBRefreshStatus)status;

@end
