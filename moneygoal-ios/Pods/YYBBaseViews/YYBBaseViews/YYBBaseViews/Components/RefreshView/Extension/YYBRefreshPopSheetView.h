//
//  YYBRefreshPopSheetView.h
//  YYBBaseViews
//
//  Created by alchemy on 2019/5/23.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseSheetView.h"
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshPopSheetView : YYBRefreshBaseSheetView

@end

NS_ASSUME_NONNULL_END
