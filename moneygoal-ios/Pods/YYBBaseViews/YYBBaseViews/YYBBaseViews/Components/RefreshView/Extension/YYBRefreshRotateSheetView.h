//
//  YYBRefreshRotateSheetView.h
//  YYBBaseViews
//
//  Created by yyb on 2023/1/9.
//  Copyright © 2023 Univease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseSheetView.h"
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshRotateSheetView : YYBRefreshBaseSheetView

@end

NS_ASSUME_NONNULL_END
