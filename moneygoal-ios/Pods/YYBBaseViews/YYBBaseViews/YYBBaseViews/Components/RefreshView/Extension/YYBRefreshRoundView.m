//
//  YYBRefreshRoundView.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshRoundView.h"

@interface YYBRefreshRoundView ()
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, strong) CAGradientLayer *gradientLayer;

@end

@implementation YYBRefreshRoundView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor clearColor];
    
    _gradientLayer = [CAGradientLayer layer];
    _gradientLayer.colors = @[(id)[UIColor colorWithHexValue:0xF1F3F7].CGColor,
                              (id)[UIColor colorWithHexValue:0xD6E0EF].CGColor];
    _gradientLayer.startPoint = CGPointMake(0, 0);
    _gradientLayer.endPoint = CGPointMake(0, 1);
    _gradientLayer.locations = @[@(0),@(1)];
    
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.lineWidth = 4;
    _shapeLayer.fillColor = [UIColor clearColor].CGColor;
    _shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    _gradientLayer.mask = _shapeLayer;
    
    [self.layer addSublayer:_gradientLayer];
    
    return self;
}

- (void)beginAnimation
{
    CABasicAnimation *anim = [self createAnimation];
    [self.gradientLayer addAnimation:anim forKey:nil];
}

- (void)stopAnimation
{
    [self.gradientLayer removeAllAnimations];
}

- (CABasicAnimation *)createAnimation
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.toValue = @(M_PI * 2);
    animation.duration = 1;
    animation.autoreverses = NO;
    animation.repeatCount = MAXFLOAT;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    return animation;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _shapeLayer.frame = self.bounds;
    _gradientLayer.frame = self.bounds;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);

    CGFloat radius = MIN(width, height) / 2 - 4;
    _shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width / 2, height / 2) radius:radius startAngle:0 endAngle:M_PI * 2 clockwise:NO].CGPath;
}

@end
