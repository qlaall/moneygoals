//
//  YYBRefreshRotateHeaderView.h
//  YYBBaseViews
//
//  Created by yyb on 2023/1/7.
//  Copyright © 2023 Univease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshRotateHeaderView : YYBRefreshBaseHeaderView

@end

NS_ASSUME_NONNULL_END
