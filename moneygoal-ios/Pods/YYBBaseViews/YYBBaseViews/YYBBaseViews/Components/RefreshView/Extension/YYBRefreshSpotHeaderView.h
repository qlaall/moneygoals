//
//  YYBRefreshSpotHeaderView.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/12/23.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"
#import "UIColor+YYBAdd.h"

@interface YYBRefreshSpotHeaderView : YYBRefreshBaseHeaderView

@end
