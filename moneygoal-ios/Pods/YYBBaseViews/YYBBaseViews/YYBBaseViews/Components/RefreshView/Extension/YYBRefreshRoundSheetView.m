//
//  YYBRefreshRoundSheetView.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshRoundSheetView.h"
#import "YYBRefreshRoundView.h"

@interface YYBRefreshRoundSheetView ()
@property (nonatomic, strong) YYBRefreshRoundView *refreshView;

@end

@implementation YYBRefreshRoundSheetView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView
{
    self = [super initWithScrollView:scrollView];
    if (!self) return nil;
    
    _refreshView = [[YYBRefreshRoundView alloc] init];
    [self addSubview:_refreshView];
    [_refreshView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(34, 34));
    }];
    
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    [_refreshView stopAnimation];
}

- (void)statusDidChanged:(YYBRefreshStatus)status
{
    switch (status) {
        case YYBRefreshStatusInitial: {
            [_refreshView stopAnimation];
        }
            break;
        case YYBRefreshStatusPulling: {
            
        }
            break;
        case YYBRefreshStatusRefreshing: {
            [_refreshView beginAnimation];
        }
            break;
            
        default:
            break;
    }
}

@end
