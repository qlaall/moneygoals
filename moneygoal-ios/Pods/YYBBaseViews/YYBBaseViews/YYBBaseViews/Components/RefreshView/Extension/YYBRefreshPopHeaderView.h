//
//  YYBRefreshPopHeaderView.h
//  YYBBaseViews
//
//  Created by Vincin on 2019/5/23.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshPopHeaderView : YYBRefreshBaseHeaderView

@end

NS_ASSUME_NONNULL_END
