//
//  YYBRefreshTextSheetView.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseSheetView.h"

static const NSString * BTRV_BOTTOM_INITIALIZE = @"上拉加载更多";
static const NSString * BTRV_BOTTOM_PULLING = @"松开加载更多";
static const NSString * BTRV_BOTTOM_REFRESHING = @"正在加载";

@interface YYBRefreshTextSheetView : YYBRefreshBaseSheetView

- (void)renderViewWithTitle:(NSString *)title status:(YYBRefreshStatus)status;

@end
