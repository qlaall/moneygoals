//
//  UIScrollView+YYBRefreshHeader.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

#import "YYBRefreshBaseView.h"
#import "YYBRefreshBaseHeaderView.h"

@interface UIScrollView (YYBRefreshHeader)

@property (nonatomic,strong) YYBRefreshBaseHeaderView *headerView;
- (void)removeRefreshHeaderView;

- (void)handleCreateHeaderViewWithBlock:(YYBRefreshBeginRefreshBlock)startRefreshBlock;
- (void)handleCreateHeaderViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock;
- (void)handleCreateHeaderViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock height:(CGFloat)height;

@end
