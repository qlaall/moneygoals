//
//  YYBRefreshBaseHeaderView.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"

@implementation YYBRefreshBaseHeaderView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView
{
    self = [super initWithScrollView:scrollView];
    _autoLoadData = NO;
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentOffset"])
    {
        if (self.status != YYBRefreshStatusRefreshing)
        {
            CGFloat offset = [[change valueForKey:NSKeyValueChangeNewKey] CGPointValue].y;
            CGFloat state1 = [self stateInitToStatePull];
            CGFloat state2 = [self statePullToStateRefresh];
            self.offset = offset;
            
            if (offset >= state1)
            {
                self.status = YYBRefreshStatusInitial;
                self.percent = 0;
            }
            else if ((offset < state1 && self.scrollView.isDragging) && !_autoLoadData)
            {
                self.status = YYBRefreshStatusPulling;
                self.percent = [self percentWithOffsetValue:offset];
            }
            else if (offset < state1 && _autoLoadData)
            {
                self.status = YYBRefreshStatusRefreshing;
                self.percent = 1;
            }
            else if ((offset < state2 && !self.scrollView.isDragging))
            {
                self.status = YYBRefreshStatusRefreshing;
                self.percent = 1;
            }
            
            if (self.isAllowHandleOffset && [self respondsToSelector:@selector(offsetDidChanged:percent:)])
            {
                [self offsetDidChanged:self.offset percent:self.percent];
            }
        }
    }
    else if ([keyPath isEqualToString:@"contentSize"])
    {
        UIEdgeInsets edgeInsets = self.scrollEdgeInsets;
        CGFloat height = CGRectGetHeight(self.frame);
        
        self.frame = CGRectMake(0, - height, CGRectGetWidth(self.scrollView.frame) - edgeInsets.left - edgeInsets.right, height);
    }
}

- (CGFloat)percentWithOffsetValue:(CGFloat)offset
{
    CGFloat rOffset = offset - [self stateInitToStatePull];
    CGFloat pOffset = fabs(rOffset);
    CGFloat percent = pOffset / CGRectGetHeight(self.frame);
    return MIN(percent, 1);
}

- (CGFloat)stateInitToStatePull
{
    return - self.scrollEdgeInsets.top;
}

- (CGFloat)statePullToStateRefresh
{
    return - self.scrollEdgeInsets.top - CGRectGetHeight(self.frame);
}

- (void)renderInitialEdgeInsets
{
    if (self.refreshType == YYBRefreshTypeChangeInset) {
        UIEdgeInsets edgeInsets = self.scrollView.contentInset;
        edgeInsets.top = self.scrollEdgeInsets.top;
        [self.scrollView setContentInset:edgeInsets];
    } else if (self.refreshType == YYBRefreshTypeStatic) {
        self.hidden = YES;
    }
}

- (void)renderRefreshingStateEdgeInsets
{
    if (self.refreshType == YYBRefreshTypeChangeInset) {
        UIEdgeInsets edgeInsets = self.scrollView.contentInset;
        edgeInsets.top += CGRectGetHeight(self.frame);
        [self.scrollView setContentInset:edgeInsets];
    } else if (self.refreshType == YYBRefreshTypeStatic) {
        self.hidden = NO;
    }
}

@end
