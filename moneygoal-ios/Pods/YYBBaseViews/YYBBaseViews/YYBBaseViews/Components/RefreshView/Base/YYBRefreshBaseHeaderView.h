//
//  YYBRefreshBaseHeaderView.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseView.h"

@interface YYBRefreshBaseHeaderView : YYBRefreshBaseView

// 用于在例如聊天页面，不需要拉到底就可以刷新的地方
@property (nonatomic) BOOL autoLoadData;

@end
