//
//  YYBRefreshBaseView.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseView.h"

@interface YYBRefreshBaseView ()
@property (nonatomic, copy) void (^ endRefreshBlock)(void);

@end

@implementation YYBRefreshBaseView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView
{
    self = [super initWithFrame:CGRectZero];
    if (!self) return nil;
    self.backgroundColor = [UIColor clearColor];
    
    _isAllowHandleOffset = YES;
    _scrollView = scrollView;
    _scrollEdgeInsets = scrollView.contentInset;
    
    return self;
}

- (void)startRefreshAnimation
{
    [_scrollView setContentOffset:CGPointMake(0, - _scrollView.contentInset.top - CGRectGetHeight(self.frame)) animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.status = YYBRefreshStatusRefreshing;
    });
}

- (void)endRefreshAnimation
{
    [self endRefreshAnimationWithBlock:nil];
}

- (void)endRefreshAnimationWithBlock:(void (^)(void))endRefreshBlock
{
    if (_status != YYBRefreshStatusInitial) {
        self.status = YYBRefreshStatusInitial;
        self.endRefreshBlock = endRefreshBlock;
        
        [UIView animateWithDuration:0 animations:^{

        } completion:^(BOOL finished) {
            if (self.endRefreshBlock) {
                self.endRefreshBlock();
                self.endRefreshBlock = nil;
            }
        }];
    }
}

- (void)renderInitialEdgeInsets
{
    
}

- (void)renderRefreshingStateEdgeInsets
{
    
}

- (void)statusDidChanged:(YYBRefreshStatus)status
{
    
}

- (void)offsetDidChanged:(CGFloat)offset percent:(CGFloat)percent
{
    
}

- (void)setStatus:(YYBRefreshStatus)status
{
    if (_status == status) return;
    _status = status;
    
    [self statusDidChanged:status];
    
    switch (status)
    {
        case YYBRefreshStatusInitial:
        {
            [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                [self renderInitialEdgeInsets];
            } completion:nil];
        }
            break;
        case YYBRefreshStatusPulling:
        {
            
        }
            break;
        case YYBRefreshStatusRefreshing:
        {
            if (self.endRefreshBlock == nil) {
                [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [self renderRefreshingStateEdgeInsets];
                } completion:^(BOOL finished) {
                    if (self.startRefreshBlock) {
                        self.startRefreshBlock();
                    }
                }];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
