//
//  YYBViewRouter.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/24.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "YYBCategory.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,YYBRouterAppearType) {
    // 正常
    YYBRouterAppearTypePush,
    // 弹出
    YYBRouterAppearTypePresent,
    // 弹出并带有一个导航栏控制器
    YYBRouterAppearTypePresentWithNavigation
};

static NSString * _Nullable const YYBRouterProtocol = @"yyb://";
static NSString * _Nullable const YYBRouterClassSuffix = @"ViewController";

typedef void (^ YYBRouterConfigBlock)(id obj);

@interface YYBViewRouter : NSObject

+ (YYBViewRouter *)shared;
@property (nonatomic, strong) NSDictionary *mappedDictionary;
@property (nonatomic, copy) NSString * (^ mapppedBlock)(NSString *protocol);

+ (void)openOnClass:(Class)className;
+ (void)openOnClass:(Class)className configureBlock:(YYBRouterConfigBlock)configureBlock;
+ (void)openOnClass:(Class)className configureBlock:(YYBRouterConfigBlock)configureBlock isAnimated:(BOOL)isAnimated appearType:(YYBRouterAppearType)appearType navigationBlock:(void(^)(UINavigationController *nav))navigationBlock;

+ (void)openURL:(NSString *)URLString;
+ (void)openURL:(NSString *)URLString configureBlock:(YYBRouterConfigBlock)configureBlock;
+ (void)openURL:(NSString *)URLString configureBlock:(YYBRouterConfigBlock)configureBlock appearType:(YYBRouterAppearType)appearType;

+ (void)openURL:(NSString *)URLString configureBlock:(YYBRouterConfigBlock)configureBlock
     parameters:(nullable NSDictionary *)parameters isAnimated:(BOOL)isAnimated appearType:(YYBRouterAppearType)appearType
navigationBlock:(nullable void(^)(UINavigationController *nav))navigationBlock;

+ (void)openFullScreenPresentPageWithClass:(Class)className configureBlock:(YYBRouterConfigBlock)configureBlock;

+ (void)dismissViewControllers:(NSInteger)pageNumber;
// pop回该控制器
// 如果不存在则不pop
+ (void)dismissToViewController:(NSString *)URLString;
+ (UIViewController *)topViewController;

+ (void)dismissModalViewControllers;
+ (void)dismissModalViewControllersWithAnimation:(BOOL)animation;

+ (void)dismissModalViewControllerUntilClass:(Class)aCalss;

@end

NS_ASSUME_NONNULL_END
