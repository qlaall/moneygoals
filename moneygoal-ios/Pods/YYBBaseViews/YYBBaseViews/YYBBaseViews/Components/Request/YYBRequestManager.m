//
//  YYBRequestManager.m
//  YYBBaseViews
//
//  Created by Aokura on 2018/7/31.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRequestManager.h"
#import "NSDictionary+Sign.h"

@implementation YYBRequestManager

+ (YYBRequestManager *)shared {
    static YYBRequestManager *helper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[YYBRequestManager alloc] init];
    });
    return helper;
}

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.enableLog = YES;
    
    return self;
}

- (AFHTTPSessionManager *)defaultSessionManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 30;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    ((AFJSONResponseSerializer *)manager.responseSerializer).removesKeysWithNullValues = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*"]];
    
    return manager;
}

- (void)handleRequestWithMethod:(NSString *)method
                      URLString:(NSString *)URLString
                         params:(NSDictionary *)params
             configManagerBlock:(YYBRequestConfigBlock)configManagerBlock
             configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock
                   successBlock:(YYBResponseSuccBlock)successBlock
                     errorBlock:(YYBResponseErrorBlock)errorBlock {

    [self handleRequestWithMethod:method URLString:URLString params:params paramsInBody:NO configManagerBlock:configManagerBlock configResultsBlock:configResultsBlock successBlock:successBlock errorBlock:errorBlock];
}

- (void)handleRequestWithMethod:(NSString *)method
                      URLString:(NSString *)URLString
                         params:(id)params
                   paramsInBody:(BOOL)paramsInBody
             configManagerBlock:(YYBRequestConfigBlock)configManagerBlock
             configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock
                   successBlock:(YYBResponseSuccBlock)successBlock
                     errorBlock:(YYBResponseErrorBlock)errorBlock {
    
    NSAssert(URLString, @"请求URL路径不能为空");
    AFHTTPSessionManager *manager = [self defaultSessionManager];
    if (self.configManagerBlock) {
        self.configManagerBlock(manager, URLString);
    }
    if (configManagerBlock) {
        configManagerBlock(manager, URLString);
    }
    
    NSError *error = nil;
    NSMutableURLRequest *request = [[manager requestSerializer] requestWithMethod:method URLString:URLString parameters:paramsInBody ? nil : params error:&error];
    if (paramsInBody && params) {
        [request setHTTPMethod:@"POST"];
        [request setValue: @"application/json; encoding=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue: @"application/json" forHTTPHeaderField:@"Accept"];
        
        NSError *writeError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&writeError];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [request setHTTPBody: [jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if (error && errorBlock) {
        errorBlock(error,params);
    } else {
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
            
        } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
            
        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (self.enableLog) {
                NSLog(@"\n请求路径 == %@",URLString);
                NSLog(@"\n请求参数 == %@",params);
                NSLog(@"\n结果 = %@",responseObject);
                NSLog(@"\n错误 = %@",error);
            }
            [self handleResponseWithPath:URLString data:responseObject parameters:params error:error successBlock:successBlock errorBlock:errorBlock configResultsBlock:configResultsBlock];
        }];
        [dataTask resume];
    }
}

- (void)handleFormDataWithURLString:(NSString *)URLString params:(NSDictionary *)params images:(NSArray *)images
               appendImageDataBlock:(void (^)(id<AFMultipartFormData>))appendImageDataBlock
                       successBlock:(YYBResponseSuccBlock)successBlock
                         errorBlock:(YYBResponseErrorBlock)errorBlock
                 configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock {
    
    NSAssert(URLString, @"请求URL路径不能为空");
    AFHTTPSessionManager *manager = [self defaultSessionManager];
    if (self.configManagerBlock) {
        self.configManagerBlock(manager, URLString);
    }
    
    NSError *error = nil;
    
    NSDictionary *parameters = [self handleMultipartParameters:params];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (appendImageDataBlock) {
            appendImageDataBlock(formData);
        } else {
            NSString *defaultServerFilePath = @"Uploads";
            for (UIImage *image in images) {
                if ([image isKindOfClass:[UIImage class]]) {
                    NSData *data = [YYBRequestManager compressImage:image toMaxFileSize:1024];
                    [formData appendPartWithFileData:data name:defaultServerFilePath fileName:@".png" mimeType:@"image/png"];
                }
            }
        }
    } error:&error];
    
    if (error && errorBlock) {
        errorBlock(error,params);
    } else {
        NSURLSessionUploadTask *dataTask = [manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (self.enableLog) {
                NSLog(@"\n请求路径 == %@",URLString);
                NSLog(@"\n请求参数 == %@",params);
                NSLog(@"\nResponseObject == %@",responseObject);
                NSLog(@"\nerror == %@",error);
            }
            [self handleResponseWithPath:URLString data:responseObject parameters:params error:error successBlock:successBlock errorBlock:errorBlock configResultsBlock:configResultsBlock];
        }];
        [dataTask resume];
    }
}

- (void)handleResponseWithPath:(NSString *)path
                          data:(id)data
                    parameters:(NSDictionary *)parameters
                         error:(NSError *)error
                  successBlock:(YYBResponseSuccBlock)successBlock
                    errorBlock:(YYBResponseErrorBlock)errorBlock
            configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock {
    BOOL shouldHandle = YES;
    if (configResultsBlock) {
        shouldHandle = configResultsBlock(data, error, parameters, path, successBlock, errorBlock);
    }
    if (shouldHandle && self.configResultsBlock) {
        self.configResultsBlock(data, error, parameters, path, successBlock, errorBlock);
    }
}

+ (NSData *)compressImage:(UIImage *)image toMaxFileSize:(NSInteger)maxFileSize {
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
    while ([imageData length] > maxFileSize && compression > maxCompression) {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(image, compression);
    }
    
    return imageData;
}

- (NSDictionary *)handleMultipartParameters:(NSDictionary *)parameters {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    for (NSString *key in parameters.allKeys) {
        id value = [parameters objectForKey:key];
        if ([value isKindOfClass:[NSDictionary class]]) {
            [dict setObject:[value handleConvertJSONStringWithParameters:value] forKey:key];
        } else {
            [dict setObject:value forKey:key];
        }
    }
    return dict;
}

@end
