//
//  NSDictionary+Sign.h
//  YYBBaseViews
//
//  Created by Aokura on 2018/8/1.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Sign)

- (NSString *)handleConvertJSONStringWithParameters:(NSDictionary *)parameters;

@end
