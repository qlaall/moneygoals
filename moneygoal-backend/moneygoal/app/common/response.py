
from fastapi.responses import JSONResponse, Response
from fastapi import status
from typing import Union


def resp_200(*, data: Union[list, dict, str] = None) -> Response:
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            'code': 200,
            'message': 'success',
            'data': data
        }
    )


def resp_500(*, data: str = None, message: str = '服务器错误') -> Response:
    return resp_error(code=500, message=message, data=data)


def resp_401(*, data: str = None, message: str = '参数校验错误') -> Response:
    return resp_error(code=401, message=message, data=data)


def resp_403(*, data: str = None, message: str = '缺少参数') -> Response:
    return resp_error(code=403, message=message, data=data)


def resp_error(*, code: int = 200, data: str = None, message: str = '') -> Response:
    return JSONResponse(
        status_code=status.HTTP_401_UNAUTHORIZED,
        content={
            'code': code,
            'message': message,
            'data': data
        }
    )
