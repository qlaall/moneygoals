import oss2

AccessKeyId = ''
AccessKeySecret = ''
EndPoint = ''
BucketName = ''
RegionID = 'cn-hangzhou'

auth = oss2.Auth(AccessKeyId, AccessKeySecret)
bucket = oss2.Bucket(auth, EndPoint, BucketName)

def upload_wx_avatar(filePath, ossFilePath):
    bucket.put_object_from_file(ossFilePath, filePath)