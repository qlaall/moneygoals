import base64
from Crypto.Cipher import AES
from app.common.config import Config

def get_sign_result(params: dict) -> bool:
    return unpack_sign(params=params) is not None

# 验签
def unpack_sign(params):
    sign = params.pop('sign', None)

    keys = list(params.keys())
    keys.sort(reverse=True)
    key_values = []
    for key in keys:
        value = params.get(key, '')
        key_with_value = key + "=" + str(value)
        key_values.append(key_with_value)
    
    sign_text ='&'.join(key_values) + '_' + 'MoneyGoal'
    sign_base64 = base64.b64encode(sign_text.encode("utf-8")).decode().strip()
    sign_gen = encrypt(sign_base64)

    if sign == sign_gen:
        return None
    else:
        return ""

def pkcs7padding(text):
    """
    明文使用PKCS7填充
    最终调用AES加密方法时，传入的是一个byte数组，要求是16的整数倍，因此需要对明文进行处理
    :param text: 待加密内容(明文)
    :return:
    """
    bs = AES.block_size  # 16
    length = len(text)
    bytes_length = len(bytes(text, encoding='utf-8'))
    # tips：utf-8编码时，英文占1个byte，而中文占3个byte
    padding_size = length if(bytes_length == length) else bytes_length
    padding = bs - padding_size % bs
    # tips：chr(padding)看与其它语言的约定，有的会使用'\0'
    padding_text = chr(padding) * padding
    return text + padding_text

def encrypt(content):
    """
    AES加密
    key,iv使用同一个
    模式cbc
    填充pkcs7
    :param key: 密钥
    :param content: 加密内容
    :return:
    """
    key_bytes = bytes(Config.sign_key, encoding='utf-8')
    cipher = AES.new(key_bytes, AES.MODE_ECB)
    # 处理明文
    content_padding = pkcs7padding(content)
    # 加密
    encrypt_bytes = cipher.encrypt(bytes(content_padding, encoding='utf-8'))
    # 重新编码
    result = str(base64.b64encode(encrypt_bytes), encoding='utf-8')
    return result