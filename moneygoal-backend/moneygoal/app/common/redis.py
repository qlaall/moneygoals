import redis as redis2
from app.common.config import Config
from app.common.utils import generate_token

# r = redis.Redis(host=Config.redis_port, password=Config.redis_password)
pool = redis2.ConnectionPool(host=Config.db_host, port=Config.redis_port, password=Config.redis_password, db=10, decode_responses=True)
redis = redis2.Redis(connection_pool=pool)

pool_captcha = redis2.ConnectionPool(host=Config.db_host, port=Config.redis_port, password=Config.redis_password, db=11, decode_responses=True)
redis_captcha = redis2.Redis(connection_pool=pool_captcha)

def refresh_tokens(info_id):
    accessToken = generate_token()
    refreshToken = generate_token()

    redis.set(info_id + 'AccessToken', accessToken, ex= 3600 * 2)
    redis.set(info_id + 'RefreshToken', refreshToken, ex= 3600 * 365)

    return { 'accessToken': accessToken, 'refreshToken': refreshToken }

